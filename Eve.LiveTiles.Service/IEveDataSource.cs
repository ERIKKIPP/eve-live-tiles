﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Service
{
    public interface IEveDataSource
    {
      
        /// <summary>
        /// save to disk
        /// </summary>
        /// <param name="accounts"></param>
        /// <returns></returns>
        Task SaveAccountsAsync(IList<EveAccount> accounts);

        /// <summary>
        /// load from disk
        /// </summary>
        /// <returns></returns>
        Task<IList<EveAccount>> LoadAccountsAsync();

        /// <summary>
        /// get character data from api
        /// </summary>
        /// <param name="account"></param>
        /// <param name="forceUpdate"></param>
        /// <returns></returns>
        Task GetCharacterDataAsync(EveAccount account, bool forceUpdate);

    }
}
