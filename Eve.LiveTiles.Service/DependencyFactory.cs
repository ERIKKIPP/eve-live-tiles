﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Service
{
    public static class DependencyFactory
    {
        private static IUnityContainer s_container = new UnityContainer();
             
        /// <summary>
        /// Resolves the type parameter T to an instance of the appropriate type.
        /// </summary>
        /// <typeparam name="T">Type of object to return</typeparam>
        public static T Resolve<T>()
        {
            T ret = default(T);

            if (s_container.IsRegistered(typeof(T)))
            {
                ret = s_container.Resolve<T>();
            }

            return ret;
        }

        /// <summary>
        /// register as a new instance with each resolve
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void RegisterType(Type from, Type to)
        {
            s_container.RegisterType(from, to);
        }

        // <summary>
        /// register as a singleton
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void RegisterTypeAsSingleton(Type from, Type to)
        {
            s_container.RegisterType(from, to, new ContainerControlledLifetimeManager());
        }
    }
}
