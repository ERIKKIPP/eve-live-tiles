
using Eve.LiveTiles.Data;
namespace Eve.LiveTiles.Service
{
    public sealed class LocatorService
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public LocatorService()
        {
            DependencyFactory.RegisterTypeAsSingleton(typeof(IEveDataService), typeof(EveDataService));
            DependencyFactory.RegisterTypeAsSingleton(typeof(IEveDataSource), typeof(EveDataSource));
        }

    }
}