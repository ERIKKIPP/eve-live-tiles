﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.Specialized;
using Windows.Storage;
using System.Globalization;
using Windows.ApplicationModel.Resources;
using System.Threading.Tasks;
using Windows.Networking.BackgroundTransfer;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.Service;
using Windows.UI.Notifications;
using Eve.LiveTiles.DataModel;
using System.Threading;
using Eve.LiveTiles.Common;
using Eve.LiveTiles.Extensions;

namespace Eve.LiveTiles.Service
{
    /// <summary>
    /// datasource is Eve API and roaming storage
    /// </summary>
    public sealed class EveDataSource : IEveDataSource
    {
        #region Private Members
        private const string FILENAME = "Accounts.xml";
        #endregion

        #region CTOR
        public EveDataSource()
        {
        }
        #endregion

        #region IEveDataSource


        /// <summary>
        /// downloads CharacterSheet, CharacterInfo, SkillInTraining and SkillQueue
        /// </summary>
        public async Task GetCharacterDataAsync(EveAccount account, bool forceUpdate)
        {
            IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();

            foreach (var pilot in account.Characters)
            {
                Response response = await eveData.GetCharacterSheet(new ApiKey { KeyId = account.KeyId, VerificationCode = account.VerificationCode }, pilot.CharacterID, forceUpdate);                
                if (!response.IsError)
                {
                    pilot.CharacterSheet = response as CharacterSheetResponse;
                }

                response = await eveData.GetCharacterInfo(new ApiKey { KeyId = account.KeyId, VerificationCode = account.VerificationCode }, pilot.CharacterID, forceUpdate);
                if (!response.IsError)
                {
                    pilot.CharacterInfo = response as CharacterInfoResponse;
                }

                response = await eveData.GetCharacterSkillInTraining(new ApiKey { KeyId = account.KeyId, VerificationCode = account.VerificationCode }, pilot.CharacterID, forceUpdate);
                if (!response.IsError)
                {
                    pilot.SkillInTraining = response as SkillInTrainingResponse;
                }

                response = await eveData.GetCharacterSkillQueue(new ApiKey { KeyId = account.KeyId, VerificationCode = account.VerificationCode }, pilot.CharacterID, forceUpdate);
                if (!response.IsError)
                {
                    pilot.SkillQueue = response as SkillQueueResponse;
                }

                response = await eveData.GetAccountStatus(new ApiKey { KeyId = account.KeyId, VerificationCode = account.VerificationCode }, pilot.CharacterID, forceUpdate);
                if (!response.IsError)
                {
                    pilot.AccountStatus = response as AccountStatusResponse;
                }
               
                await GetImageAsync(pilot, forceUpdate);
            }
        }

        private async Task GetImageAsync(EveCharacter pilot, bool forceUpdate)
        {
            IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();
            IStorageFile file = null;

            //get the character portrait
            if (forceUpdate || string.IsNullOrEmpty(pilot.CharacterImagePath))
            {
                Uri uri = eveData.GetPortrait(MiscMethodType.CharacterImage, pilot.CharacterID);
                file = await uri.DownloadFile(string.Format(CultureInfo.InvariantCulture, "{0}.jpg", pilot.CharacterName));
                pilot.CharacterImagePath = file.Path;
            }

            //get the corporate portrait
            if (forceUpdate || string.IsNullOrEmpty(pilot.CorpImagePath))
            {
                Uri uri = eveData.GetPortrait(MiscMethodType.CorporationImage, pilot.CharacterSheet.CorporationID);
                file = await uri.DownloadFile(string.Format(CultureInfo.InvariantCulture, "{0}.png", pilot.CharacterSheet.CorporationName));
                pilot.CorpImagePath = file.Path;
            }
        }

        /// <summary>
        /// load persisted accounts if they exist
        /// </summary>
        /// <returns></returns>
        public async Task<IList<EveAccount>> LoadAccountsAsync()
        {
            //load accounts
            Storage<IList<EveAccount>> database = new Storage<IList<EveAccount>>();

            return await database.OpenAsync(FILENAME, StorageLocation.Local);
        }

        /// <summary>
        /// save the accounts to disk
        /// </summary>
        /// <returns></returns>
        public async Task SaveAccountsAsync(IList<EveAccount> accounts)
        {
            Storage<IList<EveAccount>> database = new Storage<IList<EveAccount>>();

            await database.SaveAsync(accounts, FILENAME, StorageLocation.Local);

        }


        #endregion

    }
}
