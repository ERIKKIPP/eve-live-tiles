﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;

namespace Eve.LiveTiles.Service
{
    /// <summary>
    /// 
    /// </summary>
    public static class EveDataRepository
    {
        #region Private Members

        private static IList<CrtCategory> s_crtCategories;
        private static IList<CrtCertificate> s_crtCertificates;
        private static IList<CrtClass> s_crtClasses;
        private static IList<InvFlag> s_invFlags;
        private static IList<InvType> s_invTypes;
        private static IList<StaStation> s_staStation;
        private static IList<InvCategory> s_invCategories;
        private static IList<InvMarketGroup> s_invMarketGroups;
        private static IList<InvGroup> s_invGroups;
        private static IList<DgmAttributeCategory> s_dgmAttCategories;
        private static IList<DgmAttributeType> s_dgmAttributeType;
        private static IList<DgmTypeAttribute> s_dgmTypeAttributes;
        private static IList<EveUnit> s_eveUnits;
        private static IList<CrtRecommendation> s_crtRecommendations;
        private static IList<CrtRelationship> s_relations;
        private static IList<SkillGroup> s_skillTree;
        private static IList<DgmEffect> s_dgmEffect;
        private static IList<DgmTypeEffect> s_dgmtypeEffect;
        private static IList<RamActivities> s_ramActivities;
        #endregion

        #region Methods
        public async static Task<IList<RamActivities>> GetAllRamActivities()
        {
            if (s_ramActivities == null)
            {
                Storage<IList<RamActivities>> db = new Storage<IList<RamActivities>>();
                s_ramActivities = await db.OpenAsync("ms-appx:///Xml/RamActivities.xml");
            }

            return s_ramActivities;
        }

        public async static Task<IList<DgmTypeEffect>> GetAllDgmTypeEffects()
        {
            if (s_dgmtypeEffect == null)
            {
                Storage<IList<DgmTypeEffect>> db = new Storage<IList<DgmTypeEffect>>();
                s_dgmtypeEffect = await db.OpenAsync("ms-appx:///Xml/DgmTypeEffect.xml");
            }

            return s_dgmtypeEffect;
        }

        public async static Task<IList<DgmEffect>> GetAllDgmEffects()
        {
            if (s_dgmEffect == null)
            {
                Storage<IList<DgmEffect>> db = new Storage<IList<DgmEffect>>();
                s_dgmEffect = await db.OpenAsync("ms-appx:///Xml/DgmEffect.xml");
            }

            return s_dgmEffect;
        }

        public async static Task<IList<CrtCategory>> GetAllCrtCategories()
        {
            if (s_crtCategories == null)
            {
                Storage<IList<CrtCategory>> db = new Storage<IList<CrtCategory>>();
                s_crtCategories = await db.OpenAsync("ms-appx:///Xml/CrtCategory.xml");
            }

            return s_crtCategories;
        }

        public async static Task<IList<CrtCertificate>> GetAllCrtCertificates()
        {
            if (s_crtCertificates == null)
            {
                Storage<IList<CrtCertificate>> db = new Storage<IList<CrtCertificate>>();
                s_crtCertificates = await db.OpenAsync("ms-appx:///Xml/CrtCertificate.xml");
            }

            return s_crtCertificates;
        }

        public async static Task<IList<CrtClass>> GetAllCrtClasses()
        {
            if (s_crtClasses == null)
            {
                Storage<IList<CrtClass>> db = new Storage<IList<CrtClass>>();
                s_crtClasses = await db.OpenAsync("ms-appx:///Xml/CrtClass.xml");
            }

            return s_crtClasses;
        }

        public async static Task<IList<InvFlag>> GetAllInvFlags()
        {
            if (s_invFlags == null)
            {
                Storage<IList<InvFlag>> db = new Storage<IList<InvFlag>>();
                s_invFlags = await db.OpenAsync("ms-appx:///Xml/InvFlags.xml");
            }

            return s_invFlags;
        }

        public async static Task<IList<InvType>> GetAllInvTypes()
        {
            if (s_invTypes == null)
            {
                Storage<IList<InvType>> db = new Storage<IList<InvType>>();
                s_invTypes = await db.OpenAsync("ms-appx:///Xml/InvType.xml");
            }

            return s_invTypes;
        }

        public async static Task<IList<StaStation>> GetAllStaStations()
        {
            if (s_staStation == null)
            {
                Storage<IList<StaStation>> db = new Storage<IList<StaStation>>();
                s_staStation = await db.OpenAsync("ms-appx:///Xml/StaStation.xml");
            }

            return s_staStation;
        }

        public async static Task<IList<InvCategory>> GetAllInvCategories()
        {
            if (s_invCategories == null)
            {
                Storage<IList<InvCategory>> db = new Storage<IList<InvCategory>>();
                s_invCategories = await db.OpenAsync("ms-appx:///Xml/InvCategory.xml");
            }

            return s_invCategories;
        }

        public async static Task<IList<InvMarketGroup>> GetAllInvMarketGroups()
        {
            if (s_invMarketGroups == null)
            {
                Storage<IList<InvMarketGroup>> db = new Storage<IList<InvMarketGroup>>();
                s_invMarketGroups = await db.OpenAsync("ms-appx:///Xml/InvMarketGroup.xml");
            }

            return s_invMarketGroups;
        }

        public async static Task<IList<InvGroup>> GetAllInvGroups()
        {
            if (s_invGroups == null)
            {
                Storage<IList<InvGroup>> db = new Storage<IList<InvGroup>>();
                s_invGroups = await db.OpenAsync("ms-appx:///Xml/InvGroup.xml");
            }

            return s_invGroups;
        }

        public async static Task<IList<DgmAttributeCategory>> GetAllDgmAttributeCategories()
        {
            if (s_dgmAttCategories == null)
            {
                Storage<IList<DgmAttributeCategory>> db = new Storage<IList<DgmAttributeCategory>>();
                s_dgmAttCategories = await db.OpenAsync("ms-appx:///Xml/DgmAttributeCategory.xml");
            }

            return s_dgmAttCategories;
        }

        public async static Task<IList<DgmAttributeType>> GetAllDgmAttributeTypes()
        {
            if (s_dgmAttributeType == null)
            {
                Storage<IList<DgmAttributeType>> db = new Storage<IList<DgmAttributeType>>();
                s_dgmAttributeType = await db.OpenAsync("ms-appx:///Xml/DgmAttributeType.xml");

                foreach (var attribute in s_dgmAttributeType.Where(s => s.CategoryID == null))
                {
                    attribute.CategoryID = (attribute.Published.GetValueOrDefault() ? (byte)AttributeCategoryType.Miscellaneous : (byte)AttributeCategoryType.Empty);
                }

                var item = s_dgmAttributeType.Where(s => s.AttributeID == (int)AttributeIdType.MaxSubSystems).FirstOrDefault();
                if (item != null)
                {
                    item.CategoryID = (byte)AttributeCategoryType.Fitting;
                }

                item = s_dgmAttributeType.Where(s => s.AttributeID == (int)AttributeIdType.TurretHardPointModifier).FirstOrDefault();
                if (item != null)
                {
                    item.CategoryID = (byte)AttributeCategoryType.Structure;
                }

                item = s_dgmAttributeType.Where(s => s.AttributeID == (int)AttributeIdType.LauncherHardPointModifier).FirstOrDefault();
                if (item != null)
                {
                    item.CategoryID = (byte)AttributeCategoryType.Structure;
                }
            }

            return s_dgmAttributeType;
        }

        public async static Task<IList<DgmTypeAttribute>> GetAllDgmTypeAttributes()
        {
            if (s_dgmTypeAttributes == null)
            {
                Storage<IList<DgmTypeAttribute>> db = new Storage<IList<DgmTypeAttribute>>();
                s_dgmTypeAttributes = await db.OpenAsync("ms-appx:///Xml/DgmTypeAttribute.xml");
            }

            return s_dgmTypeAttributes;
        }

        public async static Task<IList<EveUnit>> GetAllEveUnits()
        {
            if (s_eveUnits == null)
            {
                Storage<IList<EveUnit>> db = new Storage<IList<EveUnit>>();
                s_eveUnits = await db.OpenAsync("ms-appx:///Xml/EveUnit.xml");
            }

            return s_eveUnits;
        }

        public async static Task<IList<CrtRecommendation>> GetAllCrtRecommendations()
        {
            if (s_crtRecommendations == null)
            {
                Storage<IList<CrtRecommendation>> db = new Storage<IList<CrtRecommendation>>();
                s_crtRecommendations = await db.OpenAsync("ms-appx:///Xml/CrtRecommendation.xml");
            }

            return s_crtRecommendations;
        }

        public async static Task<IList<CrtRelationship>> GetAllCrtRelationships()
        {
            if (s_relations == null)
            {
                Storage<IList<CrtRelationship>> db = new Storage<IList<CrtRelationship>>();
                s_relations = await db.OpenAsync("ms-appx:///Xml/CrtRelationship.xml");
            }

            return s_relations;
        }

        /// <summary>
        /// to update
        /// get xml https://api.eveonline.com/eve/SkillTree.xml.aspx
        /// place in EveDataExtractor folder
        /// change ns to Eve.LiveTiles.Data
        /// </summary>
        /// <returns></returns>
        public async static Task<IList<SkillGroup>> GetSkillTree()
        {
            if (s_skillTree == null)
            {
                Storage<IList<SkillGroup>> db = new Storage<IList<SkillGroup>>();
                s_skillTree = await db.OpenAsync("ms-appx:///Xml/SkillTree.xml");
            }

            return s_skillTree;
        }

        /// <summary>
        /// commonly used so should be loaded then accessed via this property so we don't need to do async
        /// </summary>
        public static IList<SkillGroup> SkillTree { get { return s_skillTree; } }
        #endregion

    }
}

