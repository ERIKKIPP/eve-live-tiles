﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class EveAccount
    {
        public EveAccount()
        {
            Characters = new List<EveCharacter>();
        }
        
        [DataMember]
        public string KeyId { get; set; }

        [DataMember]
        public string VerificationCode { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public IList<EveCharacter> Characters { get; set; }

        [DataMember]
        public int AccessMask { get; set; }

        [DataMember]
        public KeyType Type { get; set; }

        [DataMember]
        public DateTime? ExpirationDate { get; set; }

        public bool HasAccess(CharacterMethodType flag)
        {
            return ((AccessMask & (int)flag) == (int)flag);
        }

    }
}
