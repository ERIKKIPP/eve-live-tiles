﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    public enum DgmEffectIdType
    {

        LoPower = 11,

        HiPower = 12,

        MedPower = 13,

        Online = 16,

        LauncherFitted = 40,

        TurretFitted = 42,

        RigSlot = 2663,

        SubSystem = 3772
    }
}
