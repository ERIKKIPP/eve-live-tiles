﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    public enum AttributeCategoryType
    {
        None,
        Fitting,//	Fitting capabilities of a ship
        Shield,//	Shield attributes of ships
        Armor,//	Armor attributes of ships
        Structure,//	Structure attributes of ships
        Capacitor,//	Capacitor attributes for ships
        Targeting,//	Targeting Attributes for ships
        Miscellaneous,//	Misc. attributes
        RequiredSkills,//	Skill requirements
        Empty,//Attributes already checked and not going into a category
        Drones,//All you need to know about drones
        AI,//Attribs for the AI configuration
    }
}
