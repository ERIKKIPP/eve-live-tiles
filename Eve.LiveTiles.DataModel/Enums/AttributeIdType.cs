﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    public enum AttributeIdType
    {
        CapacitorNeed = 6,

        Hitpoints = 9,

        PowerOutput = 11,

        LowSlots = 12,

        MedSlots = 13,

        HiSlots = 14,

        PowerLoad = 15,

        Power = 30,

        MaxVelocity = 37,

        CpuOutput = 48,

        Cpu = 50,

        RechargeRate = 55,

        Agility = 70,

        MaxTargetRange = 76,

        //StructureBonus = 82,

        LauncherSlotsLeft = 101,

        TurretSlotsLeft = 102,

        KineticDamageResonance = 109,

        ThermalDamageResonance = 110,

        ExplosiveDamageResonance = 111,

        EmDamageResonance = 113,

        AgilityBonus = 151,

        PrimaryAttribute = 180,

        SecondaryAttribute = 181,

        RequiredSkill1 = 182,

        RequiredSkill2 = 183,

        RequiredSkill3 = 184,

        MaxLockedTargets = 192,

        CpuMultiplier = 202,

        ScanRadarStrength = 208,

        ScanLadarStrength = 209,

        ScanMagnetometricStrength = 210,

        ScanGravimetricStrength = 211,

        ShieldCapacity = 263,

        ArmorHP = 265,

        ArmorEmDamageResonance = 267,

        ArmorExplosiveDamageResonance = 268,

        ArmorKineticDamageResonance = 269,

        ArmorThermalDamageResonance = 270,

        ShieldEmDamageResonance = 271,

        ShieldExplosiveDamageResonance = 272,

        ShieldKineticDamageResonance = 273,

        ShieldThermalDamageResonance = 274,

        SkillTimeConstant = 275,

        RequiredSkill1Level = 277,

        RequiredSkill2Level = 278,

        RequiredSkill3Level = 279,

        SkillLevel = 280,

        DroneCapacity = 283,

        CpuNeedBonus = 310,

        MaxTargetBonus = 311,

        PowerEngineeringOutputBonus = 313,

        CapRechargeBonus = 314,

        VelocityBonus = 315,

        PowerNeedBonus = 323,

        HullHpBonus = 327,

        ArmorHpBonus = 335,

        ShieldCapacityBonus = 337,

        Rechargeratebonus = 338,

        MaxActiveDroneBonus = 353,

        CpuOutputBonus2 = 424,

        DroneRangeBonus = 459,

        ShieldRechargeRate = 479,

        CapacitorCapacity = 482,

        SignatureRadius = 552,

        ScanResolution = 564,

        ScanResolutionBonus = 566,

        WarpSpeedMultiplier = 600,

        CovertOpsAndReconOpsCloakModuleDelay = 1034,

        CapacitorCapacityBonus = 1079,

        CpuPenaltyPercent = 1082,

        UpgradeCapacity = 1132,

        RigSlots = 1137,

        DroneBandwidth = 1271,

        BaseWarpSpeed = 1281,

        RequiredSkill4 = 1285,

        RequiredSkill4Level = 1286,

        RequiredSkill5Level = 1287,

        RequiredSkill6Level = 1288,

        RequiredSkill5 = 1289,

        RequiredSkill6 = 1290,

        MaxSubSystems = 1367,

        TurretHardPointModifier = 1368,

        LauncherHardPointModifier = 1369
    }
}
