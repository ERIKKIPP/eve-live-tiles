﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    public enum MarketGroupType
    {
        None = 0,
        Blueprints = 2,
        Ships = 4,
        ShipEquipment = 9,
        AmmunitionCharges = 11,
        TradeGoods = 19,
        ImplantsBoosters = 24,
        Skills = 150,
        Drones = 157,
        ManufactureResearch = 475,
        StarbaseSovereigntyStructures = 477,
        ShipModifications = 955,
        PlanetaryInfrastructure = 1320,
        Apparel = 1396,
        SpecialEditionAssets = 1659,
        InfantryGear = 350001,
    }
}
