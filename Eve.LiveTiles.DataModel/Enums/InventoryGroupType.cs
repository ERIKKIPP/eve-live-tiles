﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    public enum InventoryGroupType
    {
        Industry = 268,
        Mechanics = 269,
        Science = 270,
        Engineering = 271,
        Electronics = 272,
        Drones = 273,
        Trade = 274,
        Navigation = 275,
        Social = 278,
        Gunnery = 255,
        MissileLauncherOperation = 256,
        SpaceshipCommand = 257,
        Leadership = 258,
        CorporationManagement = 266,
        OrbitalBombardment = 1161,
        PlanetManagement = 1044,
        Subsystems = 989,
    }
}
