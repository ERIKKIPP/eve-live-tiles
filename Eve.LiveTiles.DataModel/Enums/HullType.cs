﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    public enum HullType
    {
        Capsule = 25,
        Frigate = 40,
        Destroyer = 85,
        Cruiser = 135,
        Battlecruiser = 275,
        Battleship = 420,
        Dreadnought = 1710,
        Carrier = 2950,
        Mothership = 12000,
        Titan = 16000
    }
}
