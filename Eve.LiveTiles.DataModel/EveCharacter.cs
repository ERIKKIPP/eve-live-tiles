﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Eve.LiveTiles.DataModel
{
    [Windows.Foundation.Metadata.WebHostHidden]
    [DataContract]
    public sealed class EveCharacter : ObservableBase
    {
        #region Private Members
        private string _characterID;
        private string _characterName;
        private CharacterSheetResponse _characterSheet;
        private CharacterInfoResponse _characterInfo;
        private SkillInTrainingResponse _skillInTraining;
        private SkillQueueResponse _skillQueue;
        private AccountStatusResponse _accountStatus;
        private string _characterImagePath;
        private string _corpImagePath;
        #endregion

        #region CTOR
        public EveCharacter()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// stored in CharacterSheet but will never change so it's ok to keep here for easier access
        /// </summary>
        [DataMember]
        public string CharacterID
        {
            get { return _characterID; }
            set { SetProperty(ref _characterID, value); }
        }

        /// <summary>
        /// stored in CharacterSheet but will never change so it's ok to keep here for easier access
        /// </summary>
        [DataMember]
        public string CharacterName
        {
            get { return _characterName; }
            set { SetProperty(ref _characterName, value); }
        }

        [DataMember]
        public CharacterSheetResponse CharacterSheet
        {
            get { return _characterSheet; }
            set { SetProperty(ref _characterSheet, value); }
        }

        [DataMember]
        public CharacterInfoResponse CharacterInfo
        {
            get { return _characterInfo; }
            set { SetProperty(ref _characterInfo, value); }
        }

        [DataMember]
        public SkillInTrainingResponse SkillInTraining
        {
            get { return _skillInTraining; }
            set { SetProperty(ref _skillInTraining, value); }
        }

        [DataMember]
        public SkillQueueResponse SkillQueue
        {
            get { return _skillQueue; }
            set { SetProperty(ref _skillQueue, value); }
        }

        [DataMember]
        public AccountStatusResponse AccountStatus
        {
            get { return _accountStatus; }
            set { SetProperty(ref _accountStatus, value); }
        }

        [DataMember]
        public string CharacterImagePath
        {
            get { return _characterImagePath; }
            set { SetProperty(ref _characterImagePath, value); RaisePropertyChanged(() => CharacterImageUri); }
        }

        [DataMember]
        public string CorpImagePath
        {
            get { return _corpImagePath; }
            set { SetProperty(ref _corpImagePath, value); RaisePropertyChanged(() => CorpImageUri); }
        }

        /// <summary>
        /// when using path ms-appdata the image is locked so always save the full path to the file
        /// the file is accessible because it's still within the apps folders
        /// </summary>
        [IgnoreDataMember]
        public Uri CharacterImageUri
        {
            get
            {
                if (!string.IsNullOrEmpty(_characterImagePath))
                {
                    return new Uri(string.Format(CultureInfo.InvariantCulture, "{0}?Cache=0", _characterImagePath));
                }

                return null;
            }
        }

        [IgnoreDataMember]
        public Uri CorpImageUri
        {
            get
            {
                if (!string.IsNullOrEmpty(_corpImagePath))
                {
                    return new Uri(string.Format(CultureInfo.InvariantCulture, "{0}?Cache=0", _corpImagePath));
                }

                return null;
            }
        }

        #endregion

        public float GetEffectiveAttribute(AttributeType att)
        {
            if (_characterSheet != null && _characterSheet.Attributes != null && _characterSheet.AttributeEnhancers != null)
            {
                CharacterAttribute charatt = _characterSheet.Attributes.Where(s => s.Attribute == att).FirstOrDefault();
                AttributeEnhancer enhancer = _characterSheet.AttributeEnhancers.Where(s => s.Enhancer == att).FirstOrDefault();

                if (charatt != null)
                {
                    if (enhancer != null)
                    {
                        return charatt.Value + enhancer.AugmentatorValue;
                    }

                    return charatt.Value;
                }
            }

            return 0;
        }
    }
}
