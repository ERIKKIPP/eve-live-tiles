﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class EveUnit
    {
        public EveUnit()
        {

        }

        [DataMember]
        public byte UnitID { get; set; }

        [DataMember]
        public string UnitName { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
