using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{   
    [DataContract]
    public partial class CrtClass
    {
        [DataMember]
        public int ClassID { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ClassName { get; set; }
    }
}
