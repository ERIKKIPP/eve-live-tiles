using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class DgmTypeAttribute
    {
        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public short AttributeID { get; set; }

        [DataMember]
        public int? ValueInt { get; set; }

        [DataMember]
        public double? ValueFloat { get; set; }
    }
}
