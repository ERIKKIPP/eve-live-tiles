using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class CrtRelationship
    {
        [DataMember]
        public int RelationshipID { get; set; }

        [DataMember]
        public int? ParentID { get; set; }

        [DataMember]
        public int? ParentTypeID { get; set; }

        [DataMember]
        public byte? ParentLevel { get; set; }

        [DataMember]
        public int? ChildID { get; set; }
    }
}
