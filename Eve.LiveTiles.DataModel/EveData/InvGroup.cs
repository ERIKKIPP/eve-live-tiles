﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{

    [DataContract]
    public sealed class InvGroup
    {
        [DataMember]
        public int GroupID { get; set; }

        [DataMember]
        public int? CategoryID { get; set; }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public bool? UseBasePrice { get; set; }

        [DataMember]
        public bool? AllowManufacture { get; set; }

        [DataMember]
        public bool? AllowRecycler { get; set; }

        [DataMember]
        public bool? Anchored { get; set; }

        [DataMember]
        public bool? Anchorable { get; set; }

        [DataMember]
        public bool? FittableNonSingleton { get; set; }

        [DataMember]
        public bool? Published { get; set; }
    }
}
