using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class DgmAttributeType
    {
        [DataMember]
        public short AttributeID { get; set; }

        [DataMember]
        public string AttributeName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public double? DefaultValue { get; set; }

        [DataMember]
        public bool? Published { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public byte? UnitID { get; set; }

        [DataMember]
        public bool? Stackable { get; set; }

        [DataMember]
        public bool? HighIsGood { get; set; }

        [DataMember]
        public byte? CategoryID { get; set; }
    }
}
