using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class CrtRecommendation
    {
        [DataMember]
        public int RecommendationID { get; set; }

        [DataMember]
        public int? ShipTypeID { get; set; }

        [DataMember]
        public int? CertificateID { get; set; }

        [DataMember]
        public byte RecommendationLevel { get; set; }
    }
}
