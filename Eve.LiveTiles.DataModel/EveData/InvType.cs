using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public partial class InvType
    {
        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public int? GroupID { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public double? Mass { get; set; }

        [DataMember]
        public double? Volume { get; set; }

        [DataMember]
        public double? Capacity { get; set; }

        [DataMember]
        public int? PortionSize { get; set; }

        [DataMember]
        public byte? RaceID { get; set; }

        [DataMember]
        public decimal? BasePrice { get; set; }

        [DataMember]
        public bool? Published { get; set; }

        [DataMember]
        public int? MarketGroupID { get; set; }

        [DataMember]
        public double? ChanceOfDuplicating { get; set; }

    }
}
