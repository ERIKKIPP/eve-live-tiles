﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class StaStation
    {
        [DataMember]
        public int StationID { get; set; }

        [DataMember]
        public short? Security { get; set; }

        [DataMember]
        public double? DockingCostPerVolume { get; set; }

        [DataMember]
        public double? MaxShipVolumeDockable { get; set; }

        [DataMember]
        public int? OfficeRentalCost { get; set; }

        [DataMember]
        public byte? OperationID { get; set; }

        [DataMember]
        public int? StationTypeID { get; set; }

        [DataMember]
        public int? CorporationID { get; set; }

        [DataMember]
        public int? SolarSystemID { get; set; }

        [DataMember]
        public int? ConstellationID { get; set; }

        [DataMember]
        public int? RegionID { get; set; }

        [DataMember]
        public string StationName { get; set; }

        [DataMember]
        public double? X { get; set; }

        [DataMember]
        public double? Y { get; set; }

        [DataMember]
        public double? Z { get; set; }

        [DataMember]
        public double? ReprocessingEfficiency { get; set; }

        [DataMember]
        public double? ReprocessingStationsTake { get; set; }

        [DataMember]
        public byte? ReprocessingHangarFlag { get; set; }
    }
}
