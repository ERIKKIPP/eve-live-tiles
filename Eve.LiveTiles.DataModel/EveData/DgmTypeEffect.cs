using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class DgmTypeEffect
    {
        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public short EffectID { get; set; }

        [DataMember]
        public bool? IsDefault { get; set; }
    }
}
