using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class DgmEffect
    {
        [DataMember]
        public short EffectID { get; set; }

        [DataMember]
        public string EffectName { get; set; }

        [DataMember]
        public short? EffectCategory { get; set; }

        [DataMember]
        public int? PreExpression { get; set; }

        [DataMember]
        public int? PostExpression { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Guid { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public bool? IsOffensive { get; set; }

        [DataMember]
        public bool? IsAssistance { get; set; }

        [DataMember]
        public short? DurationAttributeID { get; set; }

        [DataMember]
        public short? TrackingSpeedAttributeID { get; set; }

        [DataMember]
        public short? DischargeAttributeID { get; set; }

        [DataMember]
        public short? RangeAttributeID { get; set; }

        [DataMember]
        public short? FalloffAttributeID { get; set; }

        [DataMember]
        public bool? DisallowAutoRepeat { get; set; }

        [DataMember]
        public bool? Published { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public bool? IsWarpSafe { get; set; }

        [DataMember]
        public bool? RangeChance { get; set; }

        [DataMember]
        public bool? ElectronicChance { get; set; }

        [DataMember]
        public bool? PropulsionChance { get; set; }

        [DataMember]
        public byte? Distribution { get; set; }

        [DataMember]
        public string SfxName { get; set; }

        [DataMember]
        public short? NpcUsageChanceAttributeID { get; set; }

        [DataMember]
        public short? NpcActivationChanceAttributeID { get; set; }

        [DataMember]
        public short? FittingUsageChanceAttributeID { get; set; }
    }
}
