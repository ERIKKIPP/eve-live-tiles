using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{

    [DataContract]
    public sealed class InvMarketGroup
    {
        [DataMember]
        public int MarketGroupID { get; set; }

        [DataMember]
        public int? ParentGroupID { get; set; }

        [DataMember]
        public string MarketGroupName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public bool? HasTypes { get; set; }
    }
}
