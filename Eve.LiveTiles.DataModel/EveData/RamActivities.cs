﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class RamActivities
    {
        public RamActivities()
        {

        }

        [DataMember]
        public byte ActivityID { get; set; }

        [DataMember]
        public string ActivityName { get; set; }

        [DataMember]
        public string IconNo { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool? Published { get; set; }
    }

}
