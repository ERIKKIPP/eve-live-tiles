using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Eve.LiveTiles.DataModel
{

    [DataContract]
    public partial class CrtCertificate
    {
        [DataMember]
        public int CertificateID { get; set; }

        [DataMember]
        public byte? CategoryID { get; set; }

        [DataMember]
        public int? ClassID { get; set; }

        [DataMember]
        public byte? Grade { get; set; }

        [DataMember]
        public int? CorpID { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
