﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class InvFlag
    {
        public InvFlag()
        {

        }

        [DataMember]
        public short FlagID { get; set; }

        [DataMember]
        public string FlagName { get; set; }

        [DataMember]
        public string FlagText { get; set; }

        [DataMember]
        public int? OrderID { get; set; }
    }
}
