﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class SkillBonus
    {
        public SkillBonus()
        {

        }

        [DataMember]
        public string BonusType { get; set; }

        [DataMember]
        public int BonusValue { get; set; }
    }
}
