﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.DataModel
{
    public sealed class SwitchOnType<T1>
    {
        private bool _break;

        public SwitchOnType()
        {

        }

        public SwitchOnType<T1> Case<T2>(Action action)
        {
            if (!_break)
            {
                if (typeof(T1) == typeof(T2))
                {
                    action();
                    _break = true;
                }
            }

            return this as SwitchOnType<T1>;
        }

        public void Default<T2>(Action action)
        {
            if (!_break)
            {
                if (typeof(T1) == typeof(T2))
                {
                    action();
                    _break = true;
                }
            }
        }
    }

    [DataContract]
    public abstract class Response
    {
        public Response()
        {

        }

        public Response(XDocument doc)
        {
            if (doc == null)
            {
                IsError = true;
                return;
            }

            if (doc.Element("eveapi").Element("error") != null)
            {
                IsError = true;
                ErrorCode =  GetValue<string>(doc.Element("eveapi").Element("error").Attribute("code"));
            }

            Version = GetValue<string>(doc.Root.Attribute("version"));

            DateTime dt = GetValue<DateTime>(doc.Element("eveapi").Element("currentTime"));
            DateTime convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            CurrentTime = convertedDate.ToLocalTime();

            dt = GetValue<DateTime>(doc.Element("eveapi").Element("cachedUntil"));
            convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            CachedUntil = convertedDate.ToLocalTime();
        }

        protected T GetValue<T>(XElement ele)
        {
            T value = default(T);

            if (ele != null)
            {
                new SwitchOnType<T>()
                  .Case<int>(() =>
                  {
                      int x = 0;
                      if (int.TryParse(ele.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<decimal>(() =>
                  {
                      decimal x = 0;
                      if (decimal.TryParse(ele.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<string>(() =>
                  {
                      value = (T)Convert.ChangeType(ele.Value, typeof(T));
                  })
                  .Case<double>(() =>
                  {
                      double x = 0;
                      if (double.TryParse(ele.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<long>(() =>
                   {
                       long x = 0;
                       if (long.TryParse(ele.Value, out x))
                       {
                           value = (T)Convert.ChangeType(x, typeof(T));
                       }
                   })
                   .Case<bool>(() =>
                    {
                        bool x = false;
                        if (bool.TryParse(ele.Value, out x))
                        {
                            value = (T)Convert.ChangeType(x, typeof(T));
                        }
                    })
                  .Default<DateTime>(() =>
                  {
                      DateTime x = DateTime.MinValue;
                      if (DateTime.TryParse(ele.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  });
            }

            return value;
        }

        protected T GetValue<T>(XAttribute att)
        {
            T value = default(T);

            if (att != null)
            {
                new SwitchOnType<T>()
                  .Case<int>(() =>
                  {
                      int x = 0;
                      if (int.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<decimal>(() =>
                  {
                      decimal x = 0;
                      if (decimal.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<string>(() =>
                  {
                      value = (T)Convert.ChangeType(att.Value, typeof(T));
                  })
                  .Case<double>(() =>
                  {
                      double x = 0;
                      if (double.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<long>(() =>
                  {
                      long x = 0;
                      if (long.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<bool>(() =>
                   {
                       bool x = false;
                       if (bool.TryParse(att.Value, out x))
                       {
                           value = (T)Convert.ChangeType(x, typeof(T));
                       }
                   })
                  .Default<DateTime>(() =>
                  {
                      DateTime x = DateTime.MinValue;
                      if (DateTime.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  });
            }

            return value;
        }

        protected abstract void LoadXml(XDocument doc);

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public DateTime CurrentTime { get; set; }

        [DataMember]
        public DateTime CachedUntil { get; set; }

        [IgnoreDataMember]
        public bool IsError { get; set; }

        [DataMember]
        public string ErrorCode { get; set; }
    }
}
