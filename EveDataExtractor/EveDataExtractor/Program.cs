﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.DataModel
{
    class Program
    {
        static void Main(string[] args)
        {




            DataContractSerializer serializer = null;
            IList<SkillGroup> skillgroup = null;

            using (Stream fileStream = new FileStream("SkillTree.xml", FileMode.OpenOrCreate))
            {
                XDocument doc = XDocument.Load(fileStream);
                SkillTreeResponse response = new SkillTreeResponse(doc);
                skillgroup = response.SkillGroups;
            }

            serializer = new DataContractSerializer(typeof(IList<SkillGroup>));

            using (MemoryStream data = new MemoryStream())
            {
                serializer.WriteObject(data, skillgroup);
                data.Seek(0, SeekOrigin.Begin);
                using (Stream fileStream = new FileStream("SkillTreenew.xml", FileMode.OpenOrCreate))
                {
                    data.CopyToAsync(fileStream);
                    fileStream.FlushAsync();
                }
            }


            using (ebs_DATADUMPEntities1 context = new ebs_DATADUMPEntities1())
            {
                IList<RamActivities> ram = new List<RamActivities>();

                serializer = new DataContractSerializer(typeof(IList<RamActivities>));

                foreach (var item in context.ramActivities)
                {
                    ram.Add(new RamActivities
                    {
                        ActivityID = item.activityID,
                        ActivityName = item.activityName,
                        Description = item.description,
                        IconNo = item.description,
                        Published = item.published
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, ram);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("RamActivities.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }

            }




            using (ebs_DATADUMPEntities1 context = new ebs_DATADUMPEntities1())
            {



                IList<DgmTypeEffect> dgmtypeeffect = new List<DgmTypeEffect>();

                serializer = new DataContractSerializer(typeof(IList<DgmTypeEffect>));

                foreach (var item in context.dgmTypeEffects)
                {
                    dgmtypeeffect.Add(new DgmTypeEffect
                    {
                        IsDefault = item.isDefault,
                        TypeID = item.typeID,
                        EffectID = item.effectID
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, dgmtypeeffect);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("DgmTypeEffect.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }





































                IList<DgmEffect> dgmeffect = new List<DgmEffect>();

                serializer = new DataContractSerializer(typeof(IList<DgmEffect>));

                foreach (var item in context.dgmEffects)
                {
                    dgmeffect.Add(new DgmEffect
                    {
                        Description = item.description,
                        DisallowAutoRepeat = item.disallowAutoRepeat,
                        DischargeAttributeID = item.dischargeAttributeID,
                        DisplayName = item.displayName,
                        Distribution = item.distribution,
                        DurationAttributeID = item.durationAttributeID,
                        EffectCategory = item.effectCategory,
                        EffectID = item.effectID,
                        EffectName = item.effectName,
                        ElectronicChance = item.electronicChance,
                        FalloffAttributeID = item.falloffAttributeID,
                        FittingUsageChanceAttributeID = item.fittingUsageChanceAttributeID,
                        Guid = item.guid,
                        IconID = item.iconID,
                        IsAssistance = item.isAssistance,
                        IsOffensive = item.isOffensive,
                        IsWarpSafe = item.isWarpSafe,
                        NpcActivationChanceAttributeID = item.npcActivationChanceAttributeID,
                        NpcUsageChanceAttributeID = item.npcUsageChanceAttributeID,
                        PostExpression = item.postExpression,
                        PreExpression = item.preExpression,
                        PropulsionChance = item.propulsionChance,
                        Published = item.published,
                        RangeAttributeID = item.rangeAttributeID,
                        RangeChance = item.rangeChance,
                        SfxName = item.sfxName,
                        TrackingSpeedAttributeID = item.trackingSpeedAttributeID
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, dgmeffect);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("DgmEffect.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }






























                IList<CrtRelationship> relate = new List<CrtRelationship>();

                serializer = new DataContractSerializer(typeof(IList<CrtRelationship>));

                foreach (var item in context.crtRelationships)
                {
                    relate.Add(new CrtRelationship
                    {
                        ChildID = item.childID,
                        ParentID = item.parentID,
                        ParentLevel = item.parentLevel,
                        ParentTypeID = item.parentTypeID,
                        RelationshipID = item.relationshipID
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, relate);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("CrtRelationship.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }































                IList<CrtRecommendation> recommend = new List<CrtRecommendation>();

                serializer = new DataContractSerializer(typeof(IList<CrtRecommendation>));

                foreach (var item in context.crtRecommendations)
                {
                    recommend.Add(new CrtRecommendation
                    {
                        CertificateID = item.certificateID,
                        RecommendationID = item.recommendationID,
                        RecommendationLevel = item.recommendationLevel,
                        ShipTypeID = item.shipTypeID
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, recommend);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("CrtRecommendation.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }





































                IList<EveUnit> units = new List<EveUnit>();

                serializer = new DataContractSerializer(typeof(IList<EveUnit>));

                foreach (var item in context.eveUnits)
                {
                    units.Add(new EveUnit
                    {
                        Description = item.description,
                        DisplayName = item.displayName,
                        UnitID = item.unitID,
                        UnitName = item.unitName
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, units);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("EveUnit.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }
























                IList<DgmAttributeCategory> dgmattcat = new List<DgmAttributeCategory>();

                serializer = new DataContractSerializer(typeof(IList<DgmAttributeCategory>));

                foreach (var item in context.dgmAttributeCategories)
                {
                    dgmattcat.Add(new DgmAttributeCategory
                    {

                        CategoryID = item.categoryID,
                        CategoryDescription = item.categoryDescription,
                        CategoryName = item.categoryName
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, dgmattcat);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("DgmAttributeCategory.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }




















                IList<DgmAttributeType> dgmatttype = new List<DgmAttributeType>();

                serializer = new DataContractSerializer(typeof(IList<DgmAttributeType>));

                foreach (var item in context.dgmAttributeTypes)
                {
                    dgmatttype.Add(new DgmAttributeType
                    {
                        AttributeID = item.attributeID,
                        AttributeName = item.attributeName,
                        CategoryID = item.categoryID,
                        DefaultValue = item.defaultValue,
                        Description = item.description,
                        DisplayName = string.IsNullOrEmpty(item.displayName) ? item.attributeName : item.displayName,
                        HighIsGood = item.highIsGood,
                        IconID = item.iconID,
                        Published = item.published,
                        Stackable = item.stackable,
                        UnitID = item.unitID
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, dgmatttype);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("DgmAttributeType.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }





























                IList<DgmTypeAttribute> dmgtypeatt = new List<DgmTypeAttribute>();

                serializer = new DataContractSerializer(typeof(IList<DgmTypeAttribute>));

                foreach (var item in context.dgmTypeAttributes)
                {
                    dmgtypeatt.Add(new DgmTypeAttribute
                    {
                        AttributeID = item.attributeID,
                        TypeID = item.typeID,
                        ValueFloat = item.valueFloat,
                        ValueInt = item.valueInt
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, dmgtypeatt);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("DgmTypeAttribute.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }




















                IList<InvGroup> invgrp = new List<InvGroup>();

                serializer = new DataContractSerializer(typeof(IList<InvGroup>));

                foreach (var item in context.invGroups)
                {
                    invgrp.Add(new InvGroup
                    {
                        Description = item.description,
                        AllowManufacture = item.allowManufacture,
                        AllowRecycler = item.allowRecycler,
                        Anchorable = item.anchorable,
                        Anchored = item.anchored,
                        CategoryID = item.categoryID,
                        FittableNonSingleton = item.fittableNonSingleton,
                        GroupID = item.groupID,
                        GroupName = item.groupName,
                        Published = item.published,
                        UseBasePrice = item.useBasePrice,
                        IconID = item.iconID,

                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, invgrp);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("InvGroup.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }














                IList<InvMarketGroup> invmktgrp = new List<InvMarketGroup>();

                serializer = new DataContractSerializer(typeof(IList<InvMarketGroup>));

                foreach (var item in context.invMarketGroups)
                {
                    invmktgrp.Add(new InvMarketGroup
                    {
                        Description = item.description,
                        HasTypes = item.hasTypes,
                        IconID = item.iconID,
                        MarketGroupID = item.marketGroupID,
                        MarketGroupName = item.marketGroupName,
                        ParentGroupID = item.parentGroupID
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, invmktgrp);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("InvMarketGroup.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }












                IList<InvCategory> invcat = new List<InvCategory>();

                serializer = new DataContractSerializer(typeof(IList<InvCategory>));

                foreach (var item in context.invCategories)
                {
                    invcat.Add(new InvCategory
                    {
                        CategoryID = item.categoryID,
                        CategoryName = item.categoryName,
                        Description = item.description,
                        IconID = item.iconID,
                        Published = item.published
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, invcat);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("InvCategory.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }










                IList<StaStation> stationlist = new List<StaStation>();

                serializer = new DataContractSerializer(typeof(IList<StaStation>));

                foreach (var item in context.staStations)
                {
                    stationlist.Add(new StaStation
                    {
                        ConstellationID = item.constellationID,
                        CorporationID = item.corporationID,
                        DockingCostPerVolume = item.dockingCostPerVolume,
                        MaxShipVolumeDockable = item.maxShipVolumeDockable,
                        OfficeRentalCost = item.officeRentalCost,
                        OperationID = item.operationID,
                        RegionID = item.regionID,
                        ReprocessingEfficiency = item.reprocessingEfficiency,
                        ReprocessingHangarFlag = item.reprocessingHangarFlag,
                        ReprocessingStationsTake = item.reprocessingStationsTake,
                        Security = item.security,
                        SolarSystemID = item.solarSystemID,
                        StationID = item.stationID,
                        StationName = item.stationName,
                        StationTypeID = item.stationTypeID,
                        X = item.x,
                        Y = item.y,
                        Z = item.z
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, stationlist);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("StaStation.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }






                IList<InvType> list = new List<InvType>();

                serializer = new DataContractSerializer(typeof(IList<InvType>));

                foreach (var item in context.invTypes)
                {
                    list.Add(new InvType
                    {
                        BasePrice = item.basePrice,
                        Capacity = item.capacity,
                        ChanceOfDuplicating = item.chanceOfDuplicating,
                        Description = item.description,
                        GroupID = item.groupID,
                        MarketGroupID = item.marketGroupID,
                        Mass = item.mass,
                        PortionSize = item.portionSize,
                        Published = item.published,
                        RaceID = item.raceID,
                        TypeID = item.typeID,
                        TypeName = item.typeName,
                        Volume = item.volume
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, list);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("InvType.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }






                IList<InvFlag> list1 = new List<InvFlag>();

                serializer = new DataContractSerializer(typeof(IList<InvFlag>));

                foreach (var item in context.invFlags)
                {
                    list1.Add(new InvFlag
                    {
                        FlagID = item.flagID,
                        FlagName = item.flagName,
                        FlagText = item.flagText,
                        OrderID = item.orderID
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, list1);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("InvFlags.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }








                IList<CrtClass> list2 = new List<CrtClass>();

                serializer = new DataContractSerializer(typeof(IList<CrtClass>));

                foreach (var item in context.crtClasses)
                {
                    list2.Add(new CrtClass
                    {
                        ClassID = item.classID,
                        ClassName = item.className,
                        Description = item.description
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, list2);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("CrtClass.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }













                IList<CrtCertificate> list3 = new List<CrtCertificate>();

                serializer = new DataContractSerializer(typeof(IList<CrtCertificate>));

                foreach (var item in context.crtCertificates)
                {
                    list3.Add(new CrtCertificate
                    {
                        CategoryID = item.categoryID,
                        ClassID = item.classID,
                        CertificateID = item.certificateID,
                        CorpID = item.corpID,
                        Grade = item.grade,
                        IconID = item.iconID,
                        Description = item.description
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, list3);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("CrtCertificate.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }












                IList<CrtCategory> list4 = new List<CrtCategory>();

                serializer = new DataContractSerializer(typeof(IList<CrtCategory>));

                foreach (var item in context.crtCategories)
                {
                    list4.Add(new CrtCategory
                    {
                        CategoryID = item.categoryID,
                        CategoryName = item.categoryName,
                        Description = item.description
                    });
                }

                using (MemoryStream data = new MemoryStream())
                {
                    serializer.WriteObject(data, list4);
                    data.Seek(0, SeekOrigin.Begin);
                    using (Stream fileStream = new FileStream("CrtCategory.xml", FileMode.OpenOrCreate))
                    {
                        data.CopyToAsync(fileStream);
                        fileStream.FlushAsync();
                    }
                }
            }
        }
    }

    [DataContract]
    public sealed class RamActivities
    {
        public RamActivities()
        {

        }

        [DataMember]
        public byte ActivityID { get; set; }

        [DataMember]
        public string ActivityName { get; set; }

        [DataMember]
        public string IconNo { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool? Published { get; set; }
    }

    [DataContract]
    public sealed class EveUnit
    {
        public EveUnit()
        {

        }

        [DataMember]
        public byte UnitID { get; set; }

        [DataMember]
        public string UnitName { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public sealed class DgmTypeAttribute
    {
        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public short AttributeID { get; set; }

        [DataMember]
        public int? ValueInt { get; set; }

        [DataMember]
        public double? ValueFloat { get; set; }
    }

    [DataContract]
    public sealed class DgmAttributeType
    {
        [DataMember]
        public short AttributeID { get; set; }

        [DataMember]
        public string AttributeName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public double? DefaultValue { get; set; }

        [DataMember]
        public bool? Published { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public byte? UnitID { get; set; }

        [DataMember]
        public bool? Stackable { get; set; }

        [DataMember]
        public bool? HighIsGood { get; set; }

        [DataMember]
        public byte? CategoryID { get; set; }
    }

    [DataContract]
    public sealed class DgmAttributeCategory
    {
        [DataMember]
        public byte CategoryID { get; set; }

        [DataMember]
        public string CategoryName { get; set; }

        [DataMember]
        public string CategoryDescription { get; set; }
    }

    [DataContract]
    public sealed class InvGroup
    {
        [DataMember]
        public int GroupID { get; set; }

        [DataMember]
        public int? CategoryID { get; set; }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public bool? UseBasePrice { get; set; }

        [DataMember]
        public bool? AllowManufacture { get; set; }

        [DataMember]
        public bool? AllowRecycler { get; set; }

        [DataMember]
        public bool? Anchored { get; set; }

        [DataMember]
        public bool? Anchorable { get; set; }

        [DataMember]
        public bool? FittableNonSingleton { get; set; }

        [DataMember]
        public bool? Published { get; set; }
    }

    [DataContract]
    public sealed class InvMarketGroup
    {
        [DataMember]
        public int MarketGroupID { get; set; }

        [DataMember]
        public int? ParentGroupID { get; set; }

        [DataMember]
        public string MarketGroupName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public bool? HasTypes { get; set; }
    }

    [DataContract]
    public sealed class InvCategory
    {
        [DataMember]
        public int CategoryID { get; set; }

        [DataMember]
        public string CategoryName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public bool? Published { get; set; }
    }

    [DataContract]
    public sealed class StaStation
    {
        [DataMember]
        public int StationID { get; set; }

        [DataMember]
        public short? Security { get; set; }

        [DataMember]
        public double? DockingCostPerVolume { get; set; }

        [DataMember]
        public double? MaxShipVolumeDockable { get; set; }

        [DataMember]
        public int? OfficeRentalCost { get; set; }

        [DataMember]
        public byte? OperationID { get; set; }

        [DataMember]
        public int? StationTypeID { get; set; }

        [DataMember]
        public int? CorporationID { get; set; }

        [DataMember]
        public int? SolarSystemID { get; set; }

        [DataMember]
        public int? ConstellationID { get; set; }

        [DataMember]
        public int? RegionID { get; set; }

        [DataMember]
        public string StationName { get; set; }

        [DataMember]
        public double? X { get; set; }

        [DataMember]
        public double? Y { get; set; }

        [DataMember]
        public double? Z { get; set; }

        [DataMember]
        public double? ReprocessingEfficiency { get; set; }

        [DataMember]
        public double? ReprocessingStationsTake { get; set; }

        [DataMember]
        public byte? ReprocessingHangarFlag { get; set; }
    }

    [DataContract]
    public partial class InvType
    {
        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public int? GroupID { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public double? Mass { get; set; }

        [DataMember]
        public double? Volume { get; set; }

        [DataMember]
        public double? Capacity { get; set; }

        [DataMember]
        public int? PortionSize { get; set; }

        [DataMember]
        public byte? RaceID { get; set; }

        [DataMember]
        public decimal? BasePrice { get; set; }

        [DataMember]
        public bool? Published { get; set; }

        [DataMember]
        public int? MarketGroupID { get; set; }

        [DataMember]
        public double? ChanceOfDuplicating { get; set; }
    }

    [DataContract]
    public sealed class InvFlag
    {
        public InvFlag()
        {

        }

        [DataMember]
        public short FlagID { get; set; }

        [DataMember]
        public string FlagName { get; set; }

        [DataMember]
        public string FlagText { get; set; }

        [DataMember]
        public int? OrderID { get; set; }
    }

    [DataContract]
    public partial class CrtCategory
    {
        [DataMember]
        public byte CategoryID { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string CategoryName { get; set; }
    }

    [DataContract]
    public partial class CrtCertificate
    {
        [DataMember]
        public int CertificateID { get; set; }

        [DataMember]
        public byte? CategoryID { get; set; }

        [DataMember]
        public int? ClassID { get; set; }

        [DataMember]
        public byte? Grade { get; set; }

        [DataMember]
        public int? CorpID { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public partial class CrtClass
    {
        [DataMember]
        public int ClassID { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ClassName { get; set; }
    }

    [DataContract]
    public sealed class CrtRecommendation
    {
        [DataMember]
        public int RecommendationID { get; set; }

        [DataMember]
        public int? ShipTypeID { get; set; }

        [DataMember]
        public int? CertificateID { get; set; }

        [DataMember]
        public byte RecommendationLevel { get; set; }
    }

    [DataContract]
    public sealed class CrtRelationship
    {
        [DataMember]
        public int RelationshipID { get; set; }

        [DataMember]
        public int? ParentID { get; set; }

        [DataMember]
        public int? ParentTypeID { get; set; }

        [DataMember]
        public byte? ParentLevel { get; set; }

        [DataMember]
        public int? ChildID { get; set; }
    }

    [DataContract]
    public sealed class DgmEffect
    {
        [DataMember]
        public short EffectID { get; set; }

        [DataMember]
        public string EffectName { get; set; }

        [DataMember]
        public short? EffectCategory { get; set; }

        [DataMember]
        public int? PreExpression { get; set; }

        [DataMember]
        public int? PostExpression { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Guid { get; set; }

        [DataMember]
        public int? IconID { get; set; }

        [DataMember]
        public bool? IsOffensive { get; set; }

        [DataMember]
        public bool? IsAssistance { get; set; }

        [DataMember]
        public short? DurationAttributeID { get; set; }

        [DataMember]
        public short? TrackingSpeedAttributeID { get; set; }

        [DataMember]
        public short? DischargeAttributeID { get; set; }

        [DataMember]
        public short? RangeAttributeID { get; set; }

        [DataMember]
        public short? FalloffAttributeID { get; set; }

        [DataMember]
        public bool? DisallowAutoRepeat { get; set; }

        [DataMember]
        public bool? Published { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public bool? IsWarpSafe { get; set; }

        [DataMember]
        public bool? RangeChance { get; set; }

        [DataMember]
        public bool? ElectronicChance { get; set; }

        [DataMember]
        public bool? PropulsionChance { get; set; }

        [DataMember]
        public byte? Distribution { get; set; }

        [DataMember]
        public string SfxName { get; set; }

        [DataMember]
        public short? NpcUsageChanceAttributeID { get; set; }

        [DataMember]
        public short? NpcActivationChanceAttributeID { get; set; }

        [DataMember]
        public short? FittingUsageChanceAttributeID { get; set; }
    }

    [DataContract]
    public sealed class DgmTypeEffect
    {
        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public short EffectID { get; set; }

        [DataMember]
        public bool? IsDefault { get; set; }
    }

}
