﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.DataModel
{
    [DataContract]
    public sealed class SkillGroup
    {
        public SkillGroup()
        {
            Skills = new List<Skill>();
        }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public int GroupID { get; set; }

        [DataMember]
        public IList<Skill> Skills { get; set; }
    }
}
