﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.DataModel
{
    public sealed class SkillTreeResponse : Response
    {
        public SkillTreeResponse()
        {

        }

        public SkillTreeResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(System.Xml.Linq.XDocument doc)
        {
            int x = 0;
            SkillGroups = new List<SkillGroup>();
            Dictionary<int, SkillGroup> dict = new Dictionary<int, SkillGroup>();
            IEnumerable<XElement> groups = (from c in doc.Descendants("result").Elements("rowset").Elements("row")
                                            select c);

            foreach (var grp in groups)
            {
                SkillGroup group = null;
                int groupId = GetValue<int>(grp.Attribute("groupID"));

                //if exists use current
                if (dict.ContainsKey(groupId))
                {
                    group = dict[groupId];
                }
                else
                {
                    group = new SkillGroup();
                    group.GroupID = groupId;
                    dict[group.GroupID] = group;
                }

                group.GroupName = GetValue<string>(grp.Attribute("groupName"));

                IEnumerable<XElement> skills = (from c in grp.Elements("rowset").Elements("row")
                                                select c);

                foreach (var skl in skills)
                {
                    Skill skill = new Skill();
                    group.Skills.Add(skill);

                    skill.GroupID = GetValue<int>(skl.Attribute("groupID"));
                    skill.TypeID = GetValue<int>(skl.Attribute("typeID"));

                    x = GetValue<int>(skl.Attribute("published"));
                    skill.Published = x == 1 ? true : false;

                    skill.TypeName = GetValue<string>(skl.Attribute("typeName"));
                    skill.Description = GetValue<string>(skl.Element("description"));
                    skill.Rank = GetValue<int>(skl.Element("rank"));

                    var rows = (from c in skl.Elements("rowset")
                                where string.Equals(c.Attribute("name").Value, "requiredSkills")
                                select c);

                    foreach (var item in rows.Elements())
                    {
                        RequiredSkill reqskill = new RequiredSkill();
                        skill.RequiredSkills.Add(reqskill);

                        reqskill.TypeID = GetValue<int>(item.Attribute("typeID"));
                        reqskill.SkillLevel = GetValue<int>(item.Attribute("skillLevel"));
                    }

                    string primary = GetValue<string>(skl.Element("requiredAttributes").Element("primaryAttribute"));

                    if (!string.IsNullOrEmpty(primary))
                    {
                        skill.PrimaryAttribute = (AttributeType)Enum.Parse(typeof(AttributeType), primary, true);
                    }

                    string secondary = GetValue<string>(skl.Element("requiredAttributes").Element("secondaryAttribute"));

                    if (!string.IsNullOrEmpty(secondary))
                    {
                        skill.SecondaryAttribute = (AttributeType)Enum.Parse(typeof(AttributeType), secondary, true);
                    }

                    rows = (from c in skl.Elements("rowset")
                            where string.Equals(c.Attribute("name").Value, "skillBonusCollection")
                            select c);

                    foreach (var item in rows.Elements())
                    {
                        SkillBonus bonus = new SkillBonus();

                        bonus.BonusType = GetValue<string>(item.Attribute("bonusType"));
                        bonus.BonusValue = GetValue<int>(item.Attribute("bonusValue"));

                        skill.SkillBonusCollection.Add(bonus);
                    }
                }

            }

            (SkillGroups as List<SkillGroup>).AddRange(dict.Values);
        }

        public IList<SkillGroup> SkillGroups { get; set; }
    }
}
