﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Eve.LiveTiles.Extensions;
using System.Globalization;
using Eve.LiveTiles.Data;

namespace Eve.LiveTiles
{
    public sealed class AccountInfoFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            AccountStatusResponse account = value as AccountStatusResponse;

            if (account != null)
            {
                TimeSpan ts = account.PaidUntil.Subtract(DateTime.Now);

                return string.Format(CultureInfo.CurrentUICulture, "{0} ({1})", account.PaidUntil.ToLocalTime(), ts.TimeSpanText());
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
