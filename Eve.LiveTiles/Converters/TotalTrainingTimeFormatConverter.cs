﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eve.LiveTiles.Extensions;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// SkillPlanningQueuePage
    /// </summary>
    public sealed class TotalTrainingTimeFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            IEnumerable<SkillQueued> list = value as IEnumerable<SkillQueued>;
           
            if (list != null)
            {
                TimeSpan ts = TimeSpan.Zero;
                EveCharacter character = DependencyFactory.Resolve<SkillPlannerViewModel>().Character;

                foreach (var item in list)
                {
                    ts = ts.Add(item.GetTrainingTimeSpan(character));
                }

                return ts.TimeSpanText();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
