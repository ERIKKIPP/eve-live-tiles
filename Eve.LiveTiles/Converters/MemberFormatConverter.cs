﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Eve.LiveTiles.Extensions;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;

namespace Eve.LiveTiles
{
    public sealed class MemberFormatConverter : IValueConverter
    {
        private static string s_yearsmonthsdays;
        private static string s_monthsdays;
        private static string s_days;
        private static string s_day;

        static MemberFormatConverter()
        {
            s_yearsmonthsdays = ResourceHelper.Instance.GetValue("MemberYears");
            s_monthsdays = ResourceHelper.Instance.GetValue("MemberMonths");
            s_days = ResourceHelper.Instance.GetValue("MemberDays");
            s_day = ResourceHelper.Instance.GetValue("MemberDay");
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            DateTime corpDate = (DateTime)value;

            TimeSpan ts = DateTime.Now.Subtract(corpDate);

            int years = ts.YearsTotal();
            int months = ts.MonthsTotal();
            int days = ts.DaysTotal();

            if (years > 0)
            {
                return string.Format(CultureInfo.CurrentUICulture, s_yearsmonthsdays, years, months, days);
            }
            else if (months > 0)
            {
                return string.Format(CultureInfo.CurrentUICulture, s_monthsdays, months, days);
            }
            else if (days > 0)
            {
                return string.Format(CultureInfo.CurrentUICulture, s_days, days);
            }

            return s_day;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }


}
