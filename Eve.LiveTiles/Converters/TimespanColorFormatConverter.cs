﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// used for the neural remap
    /// </summary>
    public sealed class TimespanColorFormatConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {

            if (value != null && value.GetType() == typeof(TimeSpan))
            {
                TimeSpan ts = (TimeSpan)value;

                if (ts.TotalMilliseconds < 0)
                {
                    return "Red";
                }
            }

            return "Green";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
