﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// CharacterSheetControl
    /// </summary>
    public sealed class CloneColorFormatConverter : IValueConverter
    {
        public CloneColorFormatConverter()
        {

        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            EveCharacter character = value as EveCharacter;

            if (character != null && character.CharacterSheet != null && character.CharacterInfo != null)
            {
                if (character.CharacterSheet.CloneSkillPoints < character.CharacterInfo.SkillPoints)
                {
                    return "Red";
                }
            }

            return "#99FFFFFF";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
