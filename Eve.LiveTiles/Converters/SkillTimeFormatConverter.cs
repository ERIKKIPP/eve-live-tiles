﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Eve.LiveTiles.Extensions;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class SkillTimeFormatConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueue queue = value as SkillQueue;

            if (queue != null)
            {
                TimeSpan ts = TimeSpan.MinValue;

                if (queue.QueuePosition == 0)
                {
                    ts = queue.EndTime.Subtract(DateTime.Now);
                }
                else
                {
                    ts = queue.EndTime.Subtract(queue.StartTime);
                }

                return ts.TimeSpanText();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }


    }
}
