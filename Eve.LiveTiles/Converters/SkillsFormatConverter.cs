﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class SkillsFormatConverter : IValueConverter
    {
        private static string s_skills;

        static SkillsFormatConverter()
        {
            s_skills = ResourceHelper.Instance.GetValue("Skills");
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                return string.Format(CultureInfo.CurrentUICulture, "({0} {1})", (value as IList<CharacterSkill>).Count, s_skills);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
