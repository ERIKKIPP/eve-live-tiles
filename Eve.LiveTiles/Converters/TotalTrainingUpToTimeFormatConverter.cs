﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Eve.LiveTiles.Extensions;

namespace Eve.LiveTiles
{
    public sealed class TotalTrainingUpToTimeFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueued queueSkill = value as SkillQueued;

            if (queueSkill != null)
            {
                TimeSpan ts = TimeSpan.Zero;
                SkillPlannerViewModel model = DependencyFactory.Resolve<SkillPlannerViewModel>();

                foreach (var item in model.SelectedSkillPlan)
                {
                    ts = ts.Add(item.GetTrainingTimeSpan(model.Character));

                    if (queueSkill == item)
                    {
                        break;
                    }
                }

                return DateTime.Now.Add(ts);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
