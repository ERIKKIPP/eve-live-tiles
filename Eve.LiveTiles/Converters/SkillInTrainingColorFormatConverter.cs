﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// CharacterTrainingControl
    /// </summary>
    public sealed class SkillInTrainingColorFormatConverter : IValueConverter
    {
        public SkillInTrainingColorFormatConverter()
        {

        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueueResponse skill = value as SkillQueueResponse;

            if (skill != null && skill.SkillsInQueue != null)
            {
                TimeSpan total = TimeSpan.Zero;

                foreach (var item in skill.SkillsInQueue)
                {
                    TimeSpan ts = TimeSpan.Zero;

                    if (item.QueuePosition == 0)
                    {
                        ts = item.EndTime.Subtract(DateTime.Now);
                    }
                    else
                    {
                        ts = item.EndTime.Subtract(item.StartTime);
                    }

                    total += ts;
                }

                if (total.Days == 0 && total.Hours < 24)
                {
                    return "Red";
                }
            }

            return "#99FFFFFF";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
