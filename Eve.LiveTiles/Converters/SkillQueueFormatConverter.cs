﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class SkillQueueFormatConverter : IValueConverter
    {
        private static string s_skillsinqueue;

        static SkillQueueFormatConverter()
        {
            s_skillsinqueue = ResourceHelper.Instance.GetValue("SkillsInQueue");
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueueResponse skillQueue = value as SkillQueueResponse;

            if (skillQueue != null && skillQueue.SkillsInQueue != null)
            {
                foreach (var item in skillQueue.SkillsInQueue.OrderBy(s => s.QueuePosition))
                {
                    var query = from grp in EveDataRepository.SkillTree
                                where grp.Skills.Any(c => c.TypeID == item.TypeID)
                                select (grp.Skills.Where(s => s.TypeID == item.TypeID)).FirstOrDefault();

                    //sb.AppendFormat(CultureInfo.CurrentUICulture, "({0} {1})", item.EndTime);
                }

                return string.Format(CultureInfo.CurrentUICulture, "({0} {1})", skillQueue.SkillsInQueue.Count, s_skillsinqueue);
                //return sb.ToString();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
