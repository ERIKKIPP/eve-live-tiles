﻿using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class SkillDescriptionFormatConverter : IValueConverter
    {

      

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int typeId = (int)value;
                     
            //get skill name
            var query = from grp in EveDataRepository.SkillTree
                        where grp.Skills.Any(c => c.TypeID == typeId)
                        select (grp.Skills.Where(s => s.TypeID == typeId)).FirstOrDefault();
   
            if (query != null)
            {
                return query.FirstOrDefault().Description;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
