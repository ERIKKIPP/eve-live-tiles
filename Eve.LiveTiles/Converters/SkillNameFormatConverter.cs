﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class SkillNameFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int typeId = (int)value;
            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();
           
            //get skill name
            var query = from grp in EveDataRepository.SkillTree
                        where grp.Skills.Any(c => c.TypeID == typeId)
                        select (grp.Skills.Where(s => s.TypeID == typeId)).FirstOrDefault();
            string skillName = string.Empty;

            if (query != null)
            {
                return string.Format(CultureInfo.InvariantCulture, "{0} ({1}x)", query.FirstOrDefault().TypeName, query.FirstOrDefault().Rank);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
