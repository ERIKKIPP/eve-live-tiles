﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// SkillPlanningSkillsPage
    /// </summary>
    public sealed class SkillLevelFormatConverter : IValueConverter
    {
        private static string s_format;
        private static string s_text;

        static SkillLevelFormatConverter()
        {
            s_format = ResourceHelper.Instance.GetValue("SkillLevel");
            s_text = ResourceHelper.Instance.GetValue("NotKnown");
        }

        public SkillLevelFormatConverter()
        {

        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int level = (int)value;

            //switch (level)
            //{
            //    case 0:
            //        return (char)0x24EA;
            //    case 1:
            //        return (char)0x2460;
            //    case 2:
            //        return (char)0x2461;
            //    case 3:
            //        return (char)0x2462;
            //    case 4:
            //        return (char)0x2463;
            //    case 5:
            //        return (char)0x2464;
            //    default:
            //        return (char)0x25A8;
            //}

            if (level == -1)
            {
                return s_text;
            }

            return string.Format(CultureInfo.CurrentUICulture, s_format, level);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
