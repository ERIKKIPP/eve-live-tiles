﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// accepts a SkillQueued and returns an image with the current level as gray and level to train to as blue
    /// </summary>
    public sealed class SkillLevelQueueImageFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueued queued = value as SkillQueued;

            if (value != null)
            {
                EveCharacter character = DependencyFactory.Resolve<SkillPlannerViewModel>().Character;
                int currentLevel = queued.GetCurrentLevel(character);
                return string.Format(CultureInfo.InvariantCulture, "ms-appx:///Assets/level_{0}{1}.png", currentLevel == -1 ? 0 : currentLevel, queued.ToLevel);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
