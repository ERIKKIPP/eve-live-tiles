﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Eve.LiveTiles.Extensions;
using Eve.LiveTiles.Data;
using Windows.ApplicationModel.Resources.Core;
using System.Globalization;

namespace Eve.LiveTiles
{
    /// <summary>
    /// SkillPlanningSkillsPage
    /// </summary>
    public sealed class AllTrainingTimeFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueued queuedSkill = value as SkillQueued;

            if (queuedSkill != null)
            {
                EveCharacter character = DependencyFactory.Resolve<SkillPlannerViewModel>().Character;
                CharacterSkill charSkill = character.CharacterSheet.Skills.Where(s => s.TypeID == queuedSkill.Skill.TypeID).FirstOrDefault();
                int level = charSkill != null ? charSkill.Level : 0;
                StringBuilder sb = new StringBuilder();

                for (int i = 1; i <= 5; i++)
                {
                    if (i > level)
                    {
                        sb.AppendLine(string.Format(CultureInfo.CurrentUICulture, ResourceHelper.Instance.GetValue("LevelTrained"), i, queuedSkill.GetRemainingTrainingTimeSpan(character, i).TimeSpanText()));
                    }
                    else
                    {
                        sb.AppendLine(string.Format(CultureInfo.CurrentUICulture, ResourceHelper.Instance.GetValue("Trained"), i));
                    }
                }

                return sb.ToString();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
