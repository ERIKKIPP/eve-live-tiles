﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    internal sealed class FittingNullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                ShipFittingModel model = value as ShipFittingModel;
                SlotType slot = (SlotType)Enum.Parse(typeof(SlotType), parameter as string);

                switch (slot)
                {
                    case SlotType.HighSlots:
                        return model.HighSlots != null ? Visibility.Visible : Visibility.Collapsed;
                    case SlotType.MediumSlots:
                        return model.MediumSlots != null ? Visibility.Visible : Visibility.Collapsed;
                    case SlotType.LowSlots:
                        return model.LowSlots != null ? Visibility.Visible : Visibility.Collapsed;
                    case SlotType.RigSlots:
                        return model.RigSlots != null ? Visibility.Visible : Visibility.Collapsed;
                    case SlotType.SubSlots:
                        return model.SubSlots != null ? Visibility.Visible : Visibility.Collapsed;
                    case SlotType.Panel:
                        return model.Panel != null ? Visibility.Visible : Visibility.Collapsed;
                    default:
                        break;
                }
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value == Visibility.Visible;
        }
    }
}
