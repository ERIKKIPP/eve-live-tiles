﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using Eve.LiveTiles.Extensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class SkillInTrainingFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillInTrainingResponse skill = value as SkillInTrainingResponse;
          
            if (skill != null && skill.SkillInTraining)
            {
                StringBuilder sb = new StringBuilder();

                //get skill name
                var query = from grp in EveDataRepository.SkillTree
                            where grp.Skills.Any(c => c.TypeID == skill.TrainingTypeID)
                            select (grp.Skills.Where(s => s.TypeID == skill.TrainingTypeID)).FirstOrDefault();
                string skillName = string.Empty;

                if (query != null)
                {
                    skillName = query.FirstOrDefault().TypeName;
                }
                
                TimeSpan ts = skill.TrainingEndTime.Subtract(DateTime.Now);

                sb.AppendLine(string.Format(CultureInfo.CurrentUICulture, "{0} ({1})", skillName, skill.TrainingToLevel));
                sb.AppendLine(ts.TimeSpanText());
                sb.Append(string.Format(CultureInfo.CurrentUICulture, "{0}", skill.TrainingEndTime)); 

                return sb.ToString();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
