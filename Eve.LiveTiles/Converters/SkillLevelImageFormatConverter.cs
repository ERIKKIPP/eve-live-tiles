﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// accepts an int level and return an image with gray squares that represent each level
    /// </summary>
    public sealed class SkillLevelImageFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int level = (int)value;

            if (level != -1)
            {
                return string.Format(CultureInfo.InvariantCulture, "ms-appx:///Assets/level_{0}.png", level);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
