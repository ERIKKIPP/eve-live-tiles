﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    internal sealed class BooleanColorFormatConverter : IValueConverter
    {
        public BooleanColorFormatConverter()
        {

        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(bool)value)
            {
                return "Red";
            }

            return "#99FFFFFF";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
