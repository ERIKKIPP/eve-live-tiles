﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class TrainingTimeRequiredSkillFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            RequiredSkillModel reqSkill = value as RequiredSkillModel;
            SkillQueued queueSkill = new SkillQueued();
            EveCharacter character = DependencyFactory.Resolve<SkillPlannerViewModel>().Character;
         
            //get skill name
            var query = (from grp in EveDataRepository.SkillTree
                         where grp.Skills.Any(c => c.TypeID == reqSkill.TypeID)
                         select (grp.Skills.Where(s => s.TypeID == reqSkill.TypeID)).FirstOrDefault()).FirstOrDefault();

            if (query != null)
            {
                queueSkill.Skill = query;
                queueSkill.ToLevel = reqSkill.SkillLevel;

                string time = queueSkill.GetTrainingTime(character);

                return !string.IsNullOrEmpty(time) ? time : ResourceHelper.Instance.GetValue("AlreadyTrained");
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
