﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    internal sealed class InQueueColorFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillModel model = value as SkillModel;

            if (model != null && model.InQueue)
            {
                return "#FF1297FB";
            }

            return "White";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
