﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    public sealed class AttributeFormatConverter : IValueConverter
    {
        private static string s_format;

        static AttributeFormatConverter()
        {
            s_format = ResourceHelper.Instance.GetValue("AttributesFormat");
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            CharacterSheetResponse sheet = value as CharacterSheetResponse;

            if (sheet != null && sheet.Attributes != null && sheet.Attributes.Count > 0)
            {
                int x = 0;

                if (int.TryParse(parameter as string, out x))
                {
                    CharacterAttribute attr = sheet.Attributes[x];
                    if (sheet.AttributeEnhancers != null)
                    {
                        AttributeEnhancer enh = sheet.AttributeEnhancers.Where(s => s.Enhancer == attr.Attribute).FirstOrDefault();

                        if (enh != null)
                        {
                            return string.Format(CultureInfo.CurrentUICulture, s_format, attr.Value + enh.AugmentatorValue, attr.Value, enh.AugmentatorValue);
                        }
                        else
                        {
                            return string.Format(CultureInfo.CurrentUICulture, "{0}", attr.Value);
                        }
                    }
                }

            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
