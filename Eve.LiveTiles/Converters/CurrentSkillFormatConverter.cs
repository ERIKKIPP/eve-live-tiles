﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// SkillPlanningQueuePage
    /// </summary>
    public sealed class CurrentSkillFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueued queueSkill = value as SkillQueued;
            EveCharacter character = DependencyFactory.Resolve<SkillPlannerViewModel>().Character;

            if (character.CharacterSheet != null && character.CharacterSheet.Skills != null)
            {
                CharacterSkill charSkill = character.CharacterSheet.Skills.Where(s => s.TypeID == queueSkill.Skill.TypeID).FirstOrDefault();

                if (charSkill != null)
                {
                    return string.Format(CultureInfo.CurrentUICulture, parameter as string, charSkill.Level);
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
