﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    /// <summary>
    /// SkillPlanningQueuePage
    /// </summary>
    public sealed class PercentCompleteFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            SkillQueued queueSkill = value as SkillQueued;
            EveCharacter character = DependencyFactory.Resolve<SkillPlannerViewModel>().Character;

            return string.Format(CultureInfo.InvariantCulture, parameter as string, queueSkill.GetPercentComplete(character));
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
