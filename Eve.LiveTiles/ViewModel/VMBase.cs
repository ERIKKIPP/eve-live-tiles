﻿using Eve.LiveTiles.DataModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal abstract class VMBase : ViewModelBase
    {
        private static ObservableCollection<EveAccount> _accounts = new ObservableCollection<EveAccount>();
        private static ObservableCollection<EveCharacter> _characters;

        public VMBase()
        {
            IsBusyDataLoader = new DataLoader(true);
        }

        /// <summary>
        /// all changes must come from the MainViewModel because the main page is bound to it
        /// </summary>
        public ObservableCollection<EveAccount> Accounts { get { return _accounts; } set { _accounts = value; RaisePropertyChanged(() => Accounts); } }

        public ObservableCollection<EveCharacter> Characters { get { return _characters; } set { _characters = value; RaisePropertyChanged(() => Characters); } }

        public DataLoader IsBusyDataLoader { get; private set; }
        
    }
}
