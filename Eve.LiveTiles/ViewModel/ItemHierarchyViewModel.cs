﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class ItemHierarchyViewModel : ItemHierarchyViewModelBase
    {

        public ItemHierarchyViewModel()
            : base()
        {

        }

        #region Methods
        protected override async void Initialize(int? _parentId)
        {
            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<InvMarketGroup> mktGrp = await EveDataRepository.GetAllInvMarketGroups();

            var query = from itemcat in invTypes
                        where itemcat.Published.GetValueOrDefault()
                        select itemcat;

            ItemsSource = CreateHierarchy(mktGrp, query, _parentId).Where(s => ExpressionsUtilty.ItemCategoriesByMarketGroup(s.MarketGroupID));
        }

        public override void Initialize()
        {
            Initialize(null);
        }

        protected async override void FilteredData()
        {
            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<InvMarketGroup> mktGrp = await EveDataRepository.GetAllInvMarketGroups();
            IList<InvGroup> invgroups = await EveDataRepository.GetAllInvGroups();
            IList<InvCategory> invcats = await EveDataRepository.GetAllInvCategories();

            ItemsSource = from inv in invTypes
                          join invgroup in invgroups on inv.GroupID equals invgroup.GroupID
                          join invcat in invcats on invgroup.CategoryID equals invcat.CategoryID
                          join mkt in mktGrp on inv.MarketGroupID equals mkt.MarketGroupID
                          where inv.Published.GetValueOrDefault() && ExpressionsUtilty.ItemCategoriesByMarketGroup(GetToplevelParentId(mktGrp, inv.MarketGroupID))
                          && FilterFunction(inv.TypeName)
                          orderby inv.TypeName
                          select new InvCategoryModel
                          {
                              Description = inv.Description,
                              MarketGroupID = mkt.MarketGroupID,
                              MarketGroupName = inv.TypeName,
                              ParentGroupID = mkt.ParentGroupID,
                              Item = inv,
                              IsFilter = true
                          };
        }

        private static int? GetToplevelParentId(IList<InvMarketGroup> mktGrp, int? marketGroupID)
        {
            var query = (from mkt in mktGrp
                         where mkt.MarketGroupID == marketGroupID
                         select mkt).FirstOrDefault();

            if (query != null && query.ParentGroupID != null)
            {
                return GetToplevelParentId(mktGrp, query.ParentGroupID);
            }

            return query.MarketGroupID;
        }

        #endregion
    }
}
