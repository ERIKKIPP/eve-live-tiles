﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Eve.LiveTiles
{
    internal abstract class PlannerBaseViewModel : VMBase
    {
        #region Private Members
        private EveCharacter _selectedCharacter;
        #endregion

        #region CTOR

        public PlannerBaseViewModel()
        {
            GoHomeCommand = new RelayCommand(() =>
            {
                NavigationHelper.NavigateHome();
            });
        }
        #endregion

        #region Methods
        /// <summary>
        /// gets required skills for the parentSkill and populates skillList
        /// </summary>
        /// <param name="parentSkill"></param>
        /// <param name="skillList"></param>
        protected static void GetRequiredSkillsFor(Skill parentSkill, IList<RequiredSkillModel> skillList)
        {
            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();

            foreach (var reqSkill in parentSkill.RequiredSkills)
            {
                var skill = (from grp in EveDataRepository.SkillTree
                             from skl in grp.Skills
                             where skl.TypeID == reqSkill.TypeID
                             select skl).FirstOrDefault();

                skillList.Add(new RequiredSkillModel
                {
                    SkillLevel = reqSkill.SkillLevel,
                    TypeName = skill.TypeName,
                    TypeID = skill.TypeID,
                    InventoryGroup = (InventoryGroupType)skill.GroupID
                });

                GetRequiredSkillsFor(skill, skillList);
            }
        }

        /// <summary>
        /// called when loading SkillPlanningShipsPage.xaml and FitterPage.xaml
        /// needed only once per life of this instance
        /// </summary>
        /// <returns></returns>
        public void Initialize()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            Messenger.Default.Register<MessageType>(this, (s) =>
            {
                switch (s)
                {
                    case MessageType.GoBack:
                        SelectedInventory = null;
                        RaisePropertyChanged(() => IsVisibleInventory);

                        GoBackClicked();
                        break;
                    default:
                        break;
                }
            });

            Messenger.Default.Register<InvCategoryModel>(this, (e) =>
            {
                LastChildClickedEvent(e);
            });

            //always call
            Reset();

            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        protected abstract void Reset();

        /// <summary>
        /// for inherited classes in case they need extra initialization
        /// </summary>
        protected virtual void Init() { }

        protected virtual void GoBackClicked() { }

        protected virtual void LastChildClicked() { }

        public IAsyncOperation<InventoryModel> CreateInventoryModel(InvType inv)
        {
            return CreateInventoryModelAsync(inv).AsAsyncOperation();
        }

        private async Task<InventoryModel> CreateInventoryModelAsync(InvType inv)
        {
            IList<EveUnit> units = await EveDataRepository.GetAllEveUnits();
            IList<DgmTypeAttribute> dgmTypes = await EveDataRepository.GetAllDgmTypeAttributes();
            IList<DgmAttributeType> dgmtatts = await EveDataRepository.GetAllDgmAttributeTypes();
            IList<DgmAttributeCategory> dgmcats = await EveDataRepository.GetAllDgmAttributeCategories();
            IList<CrtRecommendation> recommend = await EveDataRepository.GetAllCrtRecommendations();
            IList<CrtCertificate> certs = await EveDataRepository.GetAllCrtCertificates();
            IList<CrtClass> classes = await EveDataRepository.GetAllCrtClasses();

            return await Task.Run<InventoryModel>(() =>
                {

                    var query = from att1 in dgmTypes
                                join att2 in dgmtatts on att1.AttributeID equals att2.AttributeID
                                join cats in dgmcats on att2.CategoryID equals cats.CategoryID
                                join unit in units on att2.UnitID equals unit.UnitID into unitGroup
                                from unt in unitGroup.DefaultIfEmpty()
                                where att1.TypeID == inv.TypeID && att2.Published.GetValueOrDefault()
                                select new TypeAttributesModel
                                {
                                    TypeAttribute = att1,
                                    AttributeType = att2,
                                    AttributeCategory = cats,
                                    Unit = unt
                                };

                    var reqSkills = from req in query
                                    join typeAtt in dgmTypes on new { req.TypeAttribute.AttributeID, req.TypeAttribute.TypeID } equals new { typeAtt.AttributeID, typeAtt.TypeID }
                                    from skills in EveDataRepository.SkillTree
                                    from skill in skills.Skills
                                    where req.AttributeCategory.CategoryID == (int)AttributeCategoryType.RequiredSkills && (skill.TypeID == typeAtt.ValueInt || skill.TypeID == typeAtt.ValueFloat)
                                    select new
                                    {
                                        Skill = skill,
                                        TypeAttribute = typeAtt
                                    };

                    var reqCerts = from rec in recommend
                                   join crt in certs on rec.CertificateID equals crt.CertificateID
                                   join cls in classes on crt.ClassID equals cls.ClassID
                                   where rec.ShipTypeID == inv.TypeID
                                   select new CertificateModel
                                   {
                                       Description = crt.Description,
                                       Name = cls.ClassName,
                                       Grade = (CertGradeType)crt.Grade,
                                       CertificateID = crt.CertificateID
                                   };

                    IList<RequiredSkillModel> skillList = new List<RequiredSkillModel>();
                    foreach (var item in reqSkills)
                    {
                        AttributeIdType id = AttributeIdType.RequiredSkill1Level;
                        switch ((AttributeIdType)item.TypeAttribute.AttributeID)
                        {
                            case AttributeIdType.RequiredSkill1:
                                id = AttributeIdType.RequiredSkill1Level;
                                break;
                            case AttributeIdType.RequiredSkill2:
                                id = AttributeIdType.RequiredSkill2Level;
                                break;
                            case AttributeIdType.RequiredSkill3:
                                id = AttributeIdType.RequiredSkill3Level;
                                break;
                            case AttributeIdType.RequiredSkill4:
                                id = AttributeIdType.RequiredSkill4Level;
                                break;
                            case AttributeIdType.RequiredSkill5:
                                id = AttributeIdType.RequiredSkill5Level;
                                break;
                            case AttributeIdType.RequiredSkill6:
                                id = AttributeIdType.RequiredSkill6Level;
                                break;
                            default:
                                break;
                        }

                        int? level = (from q in dgmTypes
                                      where q.AttributeID == (int)id && q.TypeID == item.TypeAttribute.TypeID
                                      select q.ValueInt).FirstOrDefault();

                        GetRequiredSkillsFor(item.Skill, skillList);//add the required skills of the primary etc skills
                        skillList.Add(new RequiredSkillModel//add the required skills
                        {
                            SkillLevel = level.GetValueOrDefault(),
                            TypeName = item.Skill.TypeName,
                            TypeID = item.Skill.TypeID,
                            InventoryGroup = (InventoryGroupType)item.Skill.GroupID
                        });
                    }

                    return new InventoryModel
                    {
                        InventoryItem = inv,
                        TypeAttributes = query.Where(v => v.AttributeCategory.CategoryID != (int)AttributeCategoryType.RequiredSkills && v.AttributeCategory.CategoryID != (int)AttributeCategoryType.Empty)
                        .OrderBy(s => s.AttributeType.AttributeID).GroupBy(x => x.AttributeCategory.CategoryName).OrderBy(s => s.Key),
                        RequiredSkills = skillList.GroupBy(s => s.TypeID).Select(q => q.First()),//distinct
                        RequiredCerts = reqCerts
                    };
                });

        }
        #endregion

        #region Properties
        /// <summary>
        /// the character that is selected
        /// </summary>
        public EveCharacter Character { get { return _selectedCharacter; } set { _selectedCharacter = value; RaisePropertyChanged(() => Character); } }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand GoHomeCommand { get; private set; }

        /// <summary>
        /// selected inventory item
        /// </summary>
        public InventoryModel SelectedInventory { get; protected set; }

        /// <summary>
        /// hides or displays the inventory section
        /// </summary>
        public bool IsVisibleInventory { get { return SelectedInventory != null; } }
        #endregion

        #region Events

        protected async void LastChildClickedEvent(InvCategoryModel e)
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            SelectedInventory = await CreateInventoryModel(e.Item);

            RaisePropertyChanged(() => SelectedInventory);
            RaisePropertyChanged(() => IsVisibleInventory);

            LastChildClicked();

            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        #endregion
    }
}
