﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eve.LiveTiles.Extensions;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;

namespace Eve.LiveTiles
{
    internal abstract class ItemHierarchyViewModelBase : ViewModelBase
    {
        #region Private Members

        private IEnumerable<InvCategoryModel> _itemsSource;
        private List<string> _crumbs;
        private string _itemFilter;
        #endregion

        #region CTOR
        public ItemHierarchyViewModelBase()
        {
            _crumbs = new List<string>();

            Messenger.Default.Register<MessageType>(this, (s) =>
            {
                switch (s)
                {
                    case MessageType.ClearBreadCrumb:
                        _crumbs.Clear();
                        BuildBreadCrumb();
                        break;
                    default:
                        break;
                }
            });

            FilterFunction = (typeName) => string.IsNullOrEmpty(_itemFilter) ? true : typeName.StartsWith(_itemFilter, StringComparison.CurrentCultureIgnoreCase);
        }
        #endregion

        #region Methods
        public abstract void Initialize();

        /// <summary>
        /// used with no filter
        /// </summary>
        /// <param name="_parentId"></param>
        protected async virtual void Initialize(int? _parentId)
        {
            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<InvMarketGroup> mktGrp = await EveDataRepository.GetAllInvMarketGroups();

            var query = from itemcat in invTypes
                        where itemcat.Published.GetValueOrDefault()
                        select itemcat;

            ItemsSource = CreateHierarchy(mktGrp, query, _parentId);
        }

        protected abstract void FilteredData();

        protected static IEnumerable<InvCategoryModel> CreateHierarchy(IEnumerable<InvMarketGroup> marketGroups, IEnumerable<InvType> invTypes, int? parentId)
        {
            var children = from mkt in marketGroups
                           join inventory in invTypes on mkt.MarketGroupID equals inventory.MarketGroupID into groups
                           where mkt.ParentGroupID == parentId
                           orderby mkt.MarketGroupName
                           select new InvCategoryModel
                           {
                               Description = mkt.Description,
                               MarketGroupID = mkt.MarketGroupID,
                               ParentGroupID = mkt.ParentGroupID,
                               MarketGroupName = mkt.MarketGroupName,
                               Inventory = groups,
                               HasTypes = mkt.HasTypes
                           };

            foreach (var item in children)
            {
                yield return new InvCategoryModel()
                {
                    Children = CreateHierarchy(marketGroups, invTypes, item.MarketGroupID),
                    MarketGroupID = item.MarketGroupID,
                    ParentGroupID = item.ParentGroupID,
                    Description = item.Description,
                    MarketGroupName = item.MarketGroupName,
                    Inventory = item.Inventory,
                    HasTypes = item.HasTypes
                };
            }
        }

        private void BuildBreadCrumb()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < _crumbs.Count; i++)
            {
                sb.Append(string.Format(CultureInfo.InvariantCulture, "{0}", _crumbs[i]));

                if (_crumbs.Count > i + 1)
                {
                    sb.Append("->");
                }
            }

            BreadCrumbText = sb.ToString();

            RaisePropertyChanged(() => BreadCrumbText);
        }

        public void BreadCrumbDown(InvCategoryModel model)
        {
            if (!model.IsFilter)
            {
                _crumbs.Add(model.MarketGroupName);
                BuildBreadCrumb();
            }
            else
            {
                _crumbs.Add(null);// add empty string when filtering so BreadCrumbUp works
            }

        }

        public void BreadCrumbUp()
        {
            if (_crumbs.Count > 0)
            {
                _crumbs.RemoveAt(_crumbs.Count - 1);//remove last
            }

            BuildBreadCrumb();
        }
        #endregion

        #region Properties

        public IEnumerable<InvCategoryModel> ItemsSource
        {
            get { return _itemsSource; }
            set { _itemsSource = value; RaisePropertyChanged(() => ItemsSource); }
        }

        public string ItemFilter
        {
            get { return _itemFilter; }
            set
            {
                _itemFilter = value;

                if (string.IsNullOrEmpty(_itemFilter))
                {
                    Initialize();//back to regular menu
                }
                else
                {
                    FilteredData();//just display items
                }
            }
        }

        public string BreadCrumbText { get; private set; }

        protected Func<string, bool> FilterFunction { get; set; }
        #endregion
    }
}
