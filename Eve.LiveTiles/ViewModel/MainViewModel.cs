using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using Eve.LiveTiles.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;
using System;
using Windows.ApplicationModel.Background;
using Windows.Storage;
using GalaSoft.MvvmLight.Messaging;
using Windows.ApplicationModel.Resources;
using Windows.UI.Popups;
using Eve.LiveTiles.Data;
using Windows.ApplicationModel.Resources.Core;
using System.Globalization;
using Windows.Storage.Search;
using Windows.UI.Xaml;
using Windows.System.Threading;

namespace Eve.LiveTiles
{
    internal sealed class MainViewModel : VMBase
    {
        #region Private Members
        private bool _noAccountsMsg;
        private bool _status;
        private int _playerCount;
        private const string BACKUP_FOLDER = "Eve LiveTiles Backup";
        #endregion

        #region CTOR

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            NavigateAccountsCommand = new RelayCommand(() =>
            {
                NavigationHelper.NavigateToView<ManageAccountsPage>();
            });

            BackupCommand = new RelayCommand(() => BackupAction());
            UpdateCommand = new RelayCommand(() => UpdateAction());
            InitialLoadCommand = new RelayCommand(() => InitialLoadAction());

            GetServerStatusCommand = new RelayCommand(() => GetServerStatusAction());

            Messenger.Default.Register<MessageType>(this, (s) =>
            {
                switch (s)
                {
                    case MessageType.Update:
                        DisplayNoAccountMsg = !(Accounts != null && Accounts.Count > 0);
                        break;
                    default:
                        break;
                }

            });
        }
        #endregion

        #region Methods
        private async void UpdateAction()
        {
            await IsBusyDataLoader.LoadAsync(() => UpdateOperation());
        }

        private async void BackupAction()
        {
            string result = await IsBusyDataLoader.LoadAsync(() => BackupOperation());

            // Create the message dialog and set its content; it will get a default "Close" button since there aren't any other buttons being added
            var messageDialog = new MessageDialog(string.Format(CultureInfo.CurrentUICulture, ResourceHelper.Instance.GetValue("FileSaved"), result));

            // Show the message dialog and wait
            await messageDialog.ShowAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        private async void InitialLoadAction()
        {
            await IsBusyDataLoader.LoadAsync(() => InitialLoadOperation(), () =>
            {
                Messenger.Default.Send<MessageType>(MessageType.FinishedInitalLoad);
            });
        }

        private async Task<string> BackupOperation()
        {
            var queryOptions = new QueryOptions(CommonFileQuery.DefaultQuery, new[] { ".xml" });
            queryOptions.FolderDepth = FolderDepth.Shallow;
            queryOptions.ApplicationSearchFilter = "SkillPlans OR Accounts";

            var query = ApplicationData.Current.LocalFolder.CreateFileQueryWithOptions(queryOptions);
            IReadOnlyList<StorageFile> files = await query.GetFilesAsync();

            StorageFolder storageFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(BACKUP_FOLDER, CreationCollisionOption.OpenIfExists);

            foreach (var item in files)
            {
                await item.CopyAsync(storageFolder, string.Format(CultureInfo.InvariantCulture, "{0}{1}", item.DisplayName, item.FileType), NameCollisionOption.ReplaceExisting);
            }

            return storageFolder.Path;
        }

        private async void GetServerStatusAction()
        {
            IEveDataService eve = DependencyFactory.Resolve<IEveDataService>();
            ServerStatusResponse response = await eve.GetServerStatus(false) as ServerStatusResponse;

            Status = response.ServerOpen;
            PlayerCount = response.OnlinePlayers;
        }

        private async Task InitialLoadOperation()
        {
            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();

            //load skilltree
            await EveDataRepository.GetSkillTree();//will cache it

            //load accounts from storage
            IList<EveAccount> list = await eve.LoadAccountsAsync();
            if (list != null)
            {
                foreach (var item in list)
                {
                    Accounts.Add(item);
                }
            }

            if (Accounts != null)
            {
                Characters = new ObservableCollection<EveCharacter>(Accounts.SelectMany(s => s.Characters));
            }

            DisplayNoAccountMsg = !(Accounts != null && Accounts.Count > 0);
        }

        private async Task UpdateOperation()
        {
            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();

            if (Accounts != null)
            {
                foreach (var item in Accounts)
                {
                    await eve.GetCharacterDataAsync(item, true);
                }

                await eve.SaveAccountsAsync(Accounts);//save newly downloaded data
            }
        }
        #endregion

        #region Properties
        public RelayCommand BackupCommand { get; private set; }

        public RelayCommand NavigateAccountsCommand { get; private set; }

        public RelayCommand UpdateCommand { get; private set; }

        public RelayCommand InitialLoadCommand { get; private set; }

        public RelayCommand GetServerStatusCommand { get; private set; }

        public bool DisplayNoAccountMsg { get { return _noAccountsMsg; } set { _noAccountsMsg = value; RaisePropertyChanged(() => DisplayNoAccountMsg); } }

        public bool Status { get { return _status; } set { _status = value; RaisePropertyChanged(() => Status); RaisePropertyChanged(() => StatusText); } }

        public string StatusText { get { return _status ? ResourceHelper.Instance.GetValue("Online") : ResourceHelper.Instance.GetValue("Offline"); } }

        public int PlayerCount { get { return _playerCount; } set { _playerCount = value; RaisePropertyChanged(() => PlayerCount); } }

        public string ServerTime { get { return string.Format(CultureInfo.CurrentUICulture, "{0:HH:mm:ss}", DateTime.UtcNow); } }
        #endregion


    }
}