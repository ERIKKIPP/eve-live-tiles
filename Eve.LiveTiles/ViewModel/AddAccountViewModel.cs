﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Eve.LiveTiles.Extensions;

namespace Eve.LiveTiles
{
    internal sealed class AddAccountViewModel : VMBase
    {
        private EveAccount _accountInfo;
        private ObservableCollection<EveCharacter> _characters;
        private string _keyType;
        private string _accessMask;

        public AddAccountViewModel()
            : base()
        {
            _characters = new ObservableCollection<EveCharacter>();
            AddAccountCommand = new RelayCommand<ListView>((p) => AddAccountAction(p));
            DummyCommand = new RelayCommand<EveCharacter>((p) => { }, (p) => CanExecuteAction(p));
            GetCharactersCommand = new RelayCommand(() => GetCharactersAction());
        }

        private async void AddAccountAction(ListView lv)
        {
            await IsBusyDataLoader.LoadAsync(() => AddAccountOperation(lv));
        }

        private async Task AddAccountOperation(ListView lv)
        {
            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();

            //remove unwanted characters
            _accountInfo.Characters.Clear();
            //TODO delete image

            foreach (EveCharacter character in lv.SelectedItems)
            {
                _accountInfo.Characters.Add(character);
            }

            //unselect the item
            lv.SelectedIndex = -1;
            DummyCommand.RaiseCanExecuteChanged();

            Accounts.Add(_accountInfo);

            //reset
            Characters = new ObservableCollection<EveCharacter>(Accounts.SelectMany(a => a.Characters));

            //send message to listeners (removes the no accounts msg)
            Messenger.Default.Send<MessageType>(MessageType.Update);

            // Create the message dialog and set its content; it will get a default "Close" button since there aren't any other buttons being added
            var messageDialog = new MessageDialog(ResourceHelper.Instance.GetValue("AddAccountSuccess"));

            // Show the message dialog and wait
            await messageDialog.ShowAsync();

            //clear the current form
            _characters.Clear();
            KeyId = null;
            VerificationCode = null;
            KeyType = null;
            AccessMask = null;
            RaisePropertyChanged(() => KeyId);
            RaisePropertyChanged(() => VerificationCode);

            //get data
            await eve.GetCharacterDataAsync(_accountInfo, true);

            //save to disk
            await eve.SaveAccountsAsync(Accounts);
        }

        private async void GetCharactersAction()
        {
            await IsBusyDataLoader.LoadAsync(() => GetCharactersOperation());
        }

        private async Task GetCharactersOperation()
        {
            ErrorCode = null;
            RaisePropertyChanged(() => ErrorCode);

            IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();
            ApiKey apiinfo = new ApiKey() { KeyId = KeyId, VerificationCode = VerificationCode };
            _characters.Clear();

            try
            {
                KeyInfoResponse response = await eveData.GetKeyInfo(apiinfo, true) as KeyInfoResponse;

                if (response != null)
                {
                    if (!response.IsError)
                    {
                        KeyType = Enum.GetName(typeof(KeyType), response.Type);
                        AccessMask = response.AccessMask.ToString();

                        _accountInfo = new EveAccount()
                        {
                            AccessMask = response.AccessMask,
                            ExpirationDate = response.ExpirationDate,
                            KeyId = apiinfo.KeyId,
                            VerificationCode = apiinfo.VerificationCode,
                            Type = response.Type,
                            Version = response.Version
                        };

                        _accountInfo.Characters =
                            (from character in response.Characters
                             select new EveCharacter
                             {
                                 CharacterName = character.CharacterName,
                                 CharacterID = character.CharacterID
                             }).ToList();

                        foreach (var character in _accountInfo.Characters)
                        {
                            Uri uri = eveData.GetPortrait(MiscMethodType.CharacterImage, character.CharacterID);
                            character.CharacterImagePath = uri.AbsoluteUri;
                            AddAccountCharacters.Add(character);
                        }

                        RaisePropertyChanged(() => AddAccountCharacters);
                    }
                    else
                    {
                        ErrorCode = string.Format(CultureInfo.InvariantCulture, ResourceHelper.Instance.GetValue("APIError"), response.ErrorCode);
                        RaisePropertyChanged(() => ErrorCode);
                    }
                }
            }
            catch (Exception ex)//timeout is really the only issue
            {
                ErrorCode = string.Format(CultureInfo.InvariantCulture, ResourceHelper.Instance.GetValue("APIError"), ex.Message);
                RaisePropertyChanged(() => ErrorCode);
            }

            GetCharactersCommand.RaiseCanExecuteChanged();
        }

        private bool CanExecuteAction(EveCharacter account)
        {
            return account != null;
        }

        public RelayCommand<ListView> AddAccountCommand { get; private set; }

        /// <summary>
        /// since SelectedItems can't be bound to or passed as a CommandParameter
        /// I created a Button set to Collapsed bound to this command and SelectedItem
        /// It will be enabled or disabled based on the listview
        /// the Add button is then bound to this dummy button's IsEnabled property
        /// </summary>
        public RelayCommand<EveCharacter> DummyCommand { get; private set; }

        public RelayCommand GetCharactersCommand { get; private set; }

        public string KeyId { get; set; }

        public string VerificationCode { get; set; }

        public string ErrorCode { get; private set; }

        public string KeyType
        {
            get { return _keyType; }
            set { _keyType = value; RaisePropertyChanged(() => KeyType); }
        }

        public string AccessMask
        {
            get { return _accessMask; }
            set { _accessMask = value; RaisePropertyChanged(() => AccessMask); }
        }

        public ObservableCollection<EveCharacter> AddAccountCharacters
        {
            get { return _characters; }
            set { _characters = value; RaisePropertyChanged(() => AddAccountCharacters); }
        }

    }
}
