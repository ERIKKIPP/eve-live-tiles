﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Controls;
using Eve.LiveTiles.Extensions;

namespace Eve.LiveTiles
{
    internal sealed class CharacterDetailViewModel : VMBase
    {
        #region Private Members
        private EveCharacter _selectedCharacter;
        private Dictionary<string, SortData> _sortCache;
        private EveAccount _account;
        private string _skillFilter;
        private string _grpFilter;
        private string _selectedGroupFilter;
        private RamActivities _selectedRamActivity;
        private IEnumerable<IndustryJobModel> _industryJobs;
        private string _selectedIndustryState;
        #endregion

        #region CTOR
        public CharacterDetailViewModel()
        {
            _sortCache = new Dictionary<string, SortData>();

            NavigateSkillPlanningCommand = new RelayCommand(() =>
            {
                NavigationHelper.NavigateToView<SkillPlanningQueuePage>(Character);
            });

            NavigateFitterCommand = new RelayCommand(() =>
            {
                NavigationHelper.NavigateToView<FitterPage>(Character);
            });

            SortAssetsCommand = new RelayCommand<string>((column) => SortAssetsAction(column));
        }
        #endregion

        #region Methods
        private void SortAssetsAction(string column)
        {
            SortData sd = _sortCache["Assets"];

            if (string.Equals(column, sd.SortColumn))//switch sort type
            {
                switch (sd.SortType)
                {
                    case SortType.None:
                        break;
                    case SortType.Ascending:
                        sd.SortType = SortType.Descending;
                        break;
                    case SortType.Descending:
                        sd.SortType = SortType.Ascending;
                        break;
                    default:
                        break;
                }
            }

            sd.SortColumn = column;

            switch (column)
            {
                case "TypeName":
                    switch (sd.SortType)
                    {
                        case SortType.None:
                            break;
                        case SortType.Ascending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderBy(s => s.TypeName));
                            break;
                        case SortType.Descending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderByDescending(s => s.TypeName));
                            break;
                        default:
                            break;
                    }
                    break;
                case "Quantity":
                    switch (sd.SortType)
                    {
                        case SortType.None:
                            break;
                        case SortType.Ascending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderBy(s => s.Quantity));
                            break;
                        case SortType.Descending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderByDescending(s => s.Quantity));
                            break;
                        default:
                            break;
                    }
                    break;
                case "Container":
                    switch (sd.SortType)
                    {
                        case SortType.None:
                            break;
                        case SortType.Ascending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderBy(s => s.Container));
                            break;
                        case SortType.Descending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderByDescending(s => s.Container));
                            break;
                        default:
                            break;
                    }
                    break;
                case "Location":
                    switch (sd.SortType)
                    {
                        case SortType.None:
                            break;
                        case SortType.Ascending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderBy(s => s.Location));
                            break;
                        case SortType.Descending:
                            Assets = new ObservableCollection<AssetItemModel>(Assets.OrderByDescending(s => s.Location));
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            RaisePropertyChanged(() => Assets);
        }

        public async Task IntializeAssets()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            IList<EveAccount> accounts = DependencyFactory.Resolve<MainViewModel>().Accounts;
            IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();
            IList<InvType> types = await EveDataRepository.GetAllInvTypes();
            IList<InvFlag> flags = await EveDataRepository.GetAllInvFlags();
            IList<StaStation> stations = await EveDataRepository.GetAllStaStations();

            if (_account != null)
            {
                CharacterAssetResponse response = await eveData.GetCharacterAssetList(new ApiKey { KeyId = _account.KeyId, VerificationCode = _account.VerificationCode }, _selectedCharacter.CharacterID, false) as CharacterAssetResponse;
                ConquerableStationListResponse stationResponse = await eveData.GetConquerableStationList(false) as ConquerableStationListResponse;

                List<AssetItemModel> assets = new List<AssetItemModel>();

                if (response.AssetList != null)
                {
                    foreach (var item in response.AssetList)
                    {
                        AssetItemModel asset = new AssetItemModel(item, types, flags, stations, stationResponse.ConquerableStationList);
                        assets.Add(asset);

                        if (item.Contents != null)
                        {
                            assets.AddRange(item.Contents.Select(content => new AssetItemModel(content, types, flags, stations, stationResponse.ConquerableStationList)
                            {
                                Location = asset.Location,
                                Container = asset.TypeName,
                            }));
                        }
                    }

                    Assets = new ObservableCollection<AssetItemModel>(assets.OrderBy(s => s.TypeName));
                    _sortCache["Assets"] = new SortData() { SortType = SortType.Ascending, SortColumn = "TypeName" };
                    RaisePropertyChanged(() => Assets);
                }
            }

            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        public async Task IntializeStandings()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();
            IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();

            if (_account != null)
            {
                StandingsResponse response = await eveData.GetCharacterNPCStandings(new ApiKey { KeyId = _account.KeyId, VerificationCode = _account.VerificationCode }, _selectedCharacter.CharacterID, false) as StandingsResponse;
                IList<StandingsGroupModel> list = new List<StandingsGroupModel>();

                foreach (var item in response.StandingsGroups)
                {
                    list.Add(new StandingsGroupModel
                    {
                        Name = item.Name,
                        StandingsList = item.StandingsList
                    });
                }

                Standings = new ObservableCollection<StandingsGroupModel>(list);
                RaisePropertyChanged(() => Standings);
            }

            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        public async Task IntializeMarketOrders()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            if (_account != null)
            {
                IList<InvType> types = await EveDataRepository.GetAllInvTypes();
                IList<StaStation> stations = await EveDataRepository.GetAllStaStations();
                IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();
                IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();

                CharacterMarketOrdersResponse response = await eveData.GetCharacterMarketOrders(new ApiKey { KeyId = _account.KeyId, VerificationCode = _account.VerificationCode }, _selectedCharacter.CharacterID, false) as CharacterMarketOrdersResponse;

                if (!response.IsError)
                {
                    ConquerableStationListResponse stationResponse = await eveData.GetConquerableStationList(false) as ConquerableStationListResponse;

                    var query = (from mktorder in response.MarketOrders
                                 join station in stations on mktorder.StationID equals station.StationID into stationList
                                 from statn in stationList.DefaultIfEmpty()
                                 join cstation in stationResponse.ConquerableStationList on mktorder.StationID equals cstation.StationID into cstationList
                                 from cstatn in cstationList.DefaultIfEmpty()
                                 join typ in types on mktorder.TypeID equals typ.TypeID
                                 orderby mktorder.OrderState
                                 select new MarketOrderModel
                                 {
                                     Station = statn != null ? statn.StationName : cstatn != null ? cstatn.StationName : null,
                                     Issued = mktorder.Issued,
                                     Expiration = GetExpirationString(mktorder),
                                     OrderStateText = GetOrderStateText(mktorder),
                                     Price = mktorder.Price,
                                     Range = mktorder.Range,
                                     VolumeText = string.Format(CultureInfo.CurrentUICulture, "{0} / {1}", mktorder.VolRemaining, mktorder.VolEntered),
                                     TypeName = typ.TypeName

                                 }).GroupBy(s => s.OrderStateText);

                    MarketOrders = new ObservableCollection<IGrouping<string, MarketOrderModel>>(query);
                    RaisePropertyChanged(() => MarketOrders);
                }
            }

            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        public async Task IntializeIndustryJobs()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            if (_account != null)
            {
                IList<RamActivities> activities = await EveDataRepository.GetAllRamActivities();
                IList<InvType> types = await EveDataRepository.GetAllInvTypes();
                IList<StaStation> stations = await EveDataRepository.GetAllStaStations();
                IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();
                IList<RamActivities> ramList = await EveDataRepository.GetAllRamActivities();

                CharacterIndustryJobsResponse response = await eveData.GetCharacterIndustryJobs(new ApiKey { KeyId = _account.KeyId, VerificationCode = _account.VerificationCode }, _selectedCharacter.CharacterID, false) as CharacterIndustryJobsResponse;

                if (!response.IsError)
                {
                    ConquerableStationListResponse stationResponse = await eveData.GetConquerableStationList(false) as ConquerableStationListResponse;

                    _industryJobs = from job in response.IndustryJobs
                                    join station in stations on job.InstalledItemLocationID equals station.StationID into stationList
                                    from statn in stationList.DefaultIfEmpty()
                                    join cstation in stationResponse.ConquerableStationList on job.InstalledItemLocationID equals cstation.StationID into cstationList
                                    from cstatn in cstationList.DefaultIfEmpty()
                                    join typ in types on job.InstalledItemTypeID equals typ.TypeID
                                    join activity in activities on job.ActivityID equals activity.ActivityID
                                    select new IndustryJobModel
                                    {
                                        CompletedStatus = job.CompletedStatus,
                                        Status = GetCompletedStatusText(job),
                                        Completed = job.Completed,
                                        ActivityID = job.ActivityID,
                                        Station = statn != null ? statn.StationName : cstatn != null ? cstatn.StationName : null,
                                        TypeName = typ.TypeName,
                                        Activity = activity.ActivityName,
                                        InstallDate = job.BeginProductionTime,
                                        EndDate = job.EndProductionTime
                                    };

                    RamActivities = new ObservableCollection<RamActivities>(ramList.Where(s => s.Published.GetValueOrDefault()));
                    RaisePropertyChanged(() => RamActivities);
                    SelectedRamActivity = RamActivities[0];
                }
            }

            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        private static string GetCompletedStatusText(IndustryJob job)
        {
            /// <summary>
            /// 0 = failed
            ///1 = delivered
            ///2 = aborted
            ///3 = GM aborted
            ///4 = inflight unanchored
            ///5 = destroyed
            ///If it's not ready yet, completed = 0 and completedStatus is irrelevant/uninitialized/probably 0
            ///If complete = 1 and status = 0, then it failed
            ///

            if (job.CompletedStatus == IndustryCompletedStatusType.Failed && !job.Completed && job.BeginProductionTime.CompareTo(DateTime.UtcNow) < 0
                && DateTime.UtcNow.CompareTo(job.EndProductionTime) < 0)//start date needs to be earlier than now
            {
                return ResourceHelper.Instance.GetValue("InProgress");
            }
            else if (job.CompletedStatus == IndustryCompletedStatusType.Failed && !job.Completed && job.BeginProductionTime.CompareTo(DateTime.UtcNow) > 0)//start date needs to be later than now
            {
                return ResourceHelper.Instance.GetValue("PendingState");
            }
            else if (job.CompletedStatus == IndustryCompletedStatusType.Failed && !job.Completed && DateTime.UtcNow.CompareTo(job.EndProductionTime) > 0)
            {
                return ResourceHelper.Instance.GetValue("ReadyState");
            }

            return ResourceHelper.Instance.GetValue(string.Format(CultureInfo.InvariantCulture, "{0}",
                Enum.GetName(typeof(IndustryCompletedStatusType), job.CompletedStatus)));

        }

        private static string GetOrderStateText(MarketOrder order)
        {
            /// (if Bid = 0 and OrderState = 0) = current sell orders
            /// (if Bid = 0 and OrderState = 2) = recently sold
            /// (if Bid = 1 and OrderState = 0) = current buy orders
            /// (if Bid = 1 and OrderState = 2) = recently bought
            /// 

            if (order.Bid)
            {
                switch (order.OrderState)
                {
                    case OrderStateType.Open:
                        return ResourceHelper.Instance.GetValue("Buying");
                    case OrderStateType.Expired:
                        return ResourceHelper.Instance.GetValue("RecentlyPurchased");
                    default:
                        break;
                }
            }
            else
            {
                switch (order.OrderState)
                {
                    case OrderStateType.Open:
                        return ResourceHelper.Instance.GetValue("Selling");
                    case OrderStateType.Expired:
                        return ResourceHelper.Instance.GetValue("RecentlySold");
                    default:
                        break;
                }
            }

            return null;
        }

        private static string GetExpirationString(MarketOrder order)
        {
            TimeSpan ts = order.Issued.Subtract(DateTime.Now);

            return ts.Add(new TimeSpan(order.Duration, 0, 0, 0)).TimeSpanText();
        }

        public async Task IntializeCerts()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            IList<CrtCertificate> certs = await EveDataRepository.GetAllCrtCertificates();
            IList<CrtClass> classes = await EveDataRepository.GetAllCrtClasses();
            IList<CrtCategory> grps = await EveDataRepository.GetAllCrtCategories();


            IEnumerable<IGrouping<string, CertificateModel>> certsList = (from userCert in _selectedCharacter.CharacterSheet.Certificates
                                                                          join crtCert in certs on userCert.CertificateID equals crtCert.CertificateID
                                                                          join crtClass in classes on crtCert.ClassID equals crtClass.ClassID
                                                                          join crtCat in grps on crtCert.CategoryID equals crtCat.CategoryID
                                                                          orderby crtClass.ClassName
                                                                          select new CertificateModel
                                                                           {
                                                                               Description = crtCert.Description,
                                                                               Name = crtClass.ClassName,
                                                                               Grade = (CertGradeType)crtCert.Grade,
                                                                               GroupName = crtCat.CategoryName

                                                                           }).OrderBy(s => s.GroupName).GroupBy(x => x.GroupName);

            Certificates = new ObservableCollection<IGrouping<string, CertificateModel>>(certsList);

            RaisePropertyChanged(() => Certificates);
            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        public void IntializeSkills()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            SkillFilter = null;
            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }
        #endregion

        #region Properties
        public string SkillFilter
        {
            get { return _skillFilter; }
            set { _skillFilter = value; RaisePropertyChanged(() => Skills); }
        }

        /// <summary>
        /// item source for the group filter
        /// GroupFilter
        /// </summary>
        public IEnumerable<string> SkillGroups
        {
            get
            {
                List<string> list = new List<string>();
                list.Add(" ");

                list.AddRange(from skillGrp in EveDataRepository.SkillTree
                              where skillGrp.Skills.Any(s => s.Published)
                              orderby skillGrp.GroupName
                              select skillGrp.GroupName);

                return list;

            }
        }

        /// <summary>
        /// when an item is selected in the group filter
        /// </summary>
        public string SelectedGroupFilter
        {
            get { return _grpFilter; }
            set
            {
                _grpFilter = value;
                _selectedGroupFilter = null;

                if (!string.IsNullOrWhiteSpace(_grpFilter))
                {
                    _selectedGroupFilter = _grpFilter;
                }

                RaisePropertyChanged(() => Skills);
            }
        }

        /// <summary>
        /// item source for the industry state filter
        /// </summary>
        public IEnumerable<string> IndustryStates
        {
            get
            {
                List<string> list = new List<string>();
                _selectedIndustryState = ResourceHelper.Instance.GetValue("AnyActiveState");
                list.Add(_selectedIndustryState);
                list.Add(ResourceHelper.Instance.GetValue("PendingState"));
                list.Add(ResourceHelper.Instance.GetValue("InProgressState"));
                list.Add(ResourceHelper.Instance.GetValue("ReadyState"));
                list.Add(ResourceHelper.Instance.GetValue("DeliveredState"));

                return list;
            }
        }

        public string SelectedIndustryState
        {
            get { return _selectedIndustryState; }
            set
            {
                _selectedIndustryState = value;
                RaisePropertyChanged(() => IndustryJobs);
            }
        }

        public RamActivities SelectedRamActivity
        {
            get { return _selectedRamActivity; }
            set
            {
                _selectedRamActivity = value;
                RaisePropertyChanged(() => IndustryJobs);
                RaisePropertyChanged(() => SelectedRamActivity);
            }
        }

        public RelayCommand NavigateSkillPlanningCommand { get; private set; }

        public RelayCommand NavigateFitterCommand { get; private set; }

        public RelayCommand<string> SortAssetsCommand { get; private set; }

        public EveCharacter Character
        {
            get { return _selectedCharacter; }
            set
            {
                _selectedCharacter = value;
                RaisePropertyChanged(() => Character);

                _account = Accounts.Where(s => s.Characters.Any(x => x == _selectedCharacter)).FirstOrDefault();

                HasCharacterSheetAccess = _account.HasAccess(CharacterMethodType.CharacterSheet);

                HasStandingsAccess = _account.HasAccess(CharacterMethodType.Standings);

                HasAssetListAccess = _account.HasAccess(CharacterMethodType.AssetList);

                HasMktOrdersAccess = _account.HasAccess(CharacterMethodType.MarketOrders);

                HasIndustryJobsAccess = _account.HasAccess(CharacterMethodType.IndustryJobs);
            }
        }

        public ObservableCollection<AssetItemModel> Assets { get; private set; }

        public ObservableCollection<StandingsGroupModel> Standings { get; private set; }

        public ObservableCollection<IGrouping<string, CertificateModel>> Certificates { get; private set; }

        public ObservableCollection<IGrouping<string, SkillModel>> Skills
        {
            get
            {
                Func<string, bool> filter = typeName => string.IsNullOrEmpty(_skillFilter) ? true : typeName.StartsWith(_skillFilter, StringComparison.CurrentCultureIgnoreCase);
                Func<string, bool> grpfilter = grpName => string.IsNullOrEmpty(_selectedGroupFilter) ? true : grpName.Equals(_selectedGroupFilter, StringComparison.CurrentCultureIgnoreCase);

                IEnumerable<IGrouping<string, SkillModel>> skillList = (from skillGrp in EveDataRepository.SkillTree
                                                                        from skill in skillGrp.Skills
                                                                        join userSkill in _selectedCharacter.CharacterSheet.Skills on skill.TypeID equals userSkill.TypeID
                                                                        where filter(skill.TypeName) && grpfilter(skillGrp.GroupName)
                                                                        select new SkillModel
                                                                        {
                                                                            GroupName = skillGrp.GroupName,
                                                                            Description = skill.Description,
                                                                            TypeName = skill.TypeName,
                                                                            Level = userSkill.Level,
                                                                            TypeID = skill.TypeID,
                                                                            PrimaryAttribute = skill.PrimaryAttribute,
                                                                            SecondaryAttribute = skill.SecondaryAttribute

                                                                        }).OrderBy(s => s.TypeName).GroupBy(x => x.GroupName).OrderBy(s => s.Key);

                return new ObservableCollection<IGrouping<string, SkillModel>>(skillList);
            }
        }

        #region Tokens
        /// <summary>
        /// for binding
        /// </summary>
        public bool HasCharacterSheetAccess { get; private set; }

        /// <summary>
        /// for binding
        /// </summary>
        public bool HasStandingsAccess { get; private set; }

        /// <summary>
        /// for binding
        /// </summary>
        public bool HasAssetListAccess { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool HasMktOrdersAccess { get; private set; }

        public bool HasIndustryJobsAccess { get; private set; }

        public bool HasAccess(CharacterMethodType flags)
        {
            return _account.HasAccess(flags);
        }
        #endregion

        public ObservableCollection<IGrouping<string, MarketOrderModel>> MarketOrders { get; private set; }

        public ObservableCollection<IndustryJobModel> IndustryJobs
        {
            get
            {
                if (_industryJobs != null && _selectedRamActivity != null)
                {
                    Func<int, bool> ramActivityfilter = ramactivity => _selectedRamActivity.ActivityID == 0 ? true : ramactivity == _selectedRamActivity.ActivityID;

                    Func<IndustryJobModel, bool> jobStatefilter = (job) =>
                    {
                        if (string.Equals(_selectedIndustryState, ResourceHelper.Instance.GetValue("AnyActiveState")))
                        {
                            return true;
                        }
                        else if (string.Equals(_selectedIndustryState, ResourceHelper.Instance.GetValue("PendingState")) &&
                            job.CompletedStatus == IndustryCompletedStatusType.Failed && !job.Completed && job.InstallDate.CompareTo(DateTime.UtcNow) > 0)
                        {
                            return true;
                        }
                        else if (string.Equals(_selectedIndustryState, ResourceHelper.Instance.GetValue("InProgressState")) &&
                            job.CompletedStatus == IndustryCompletedStatusType.Failed && !job.Completed && job.InstallDate.CompareTo(DateTime.UtcNow) < 0 
                            && DateTime.UtcNow.CompareTo(job.EndDate) < 0)
                        {
                            return true;
                        }
                        else if (string.Equals(_selectedIndustryState, ResourceHelper.Instance.GetValue("ReadyState")) &&
                            !job.Completed && job.CompletedStatus == IndustryCompletedStatusType.Failed && DateTime.UtcNow.CompareTo(job.EndDate) > 0)
                        {
                            return true;
                        }
                        else if (string.Equals(_selectedIndustryState, ResourceHelper.Instance.GetValue("DeliveredState")) &&
                            job.Completed && job.CompletedStatus == IndustryCompletedStatusType.Delivered)
                        {
                            return true;
                        }

                        return false;

                    };

                    var query = from job in _industryJobs
                                where ramActivityfilter(job.ActivityID) && jobStatefilter(job)
                                select job;

                    return new ObservableCollection<IndustryJobModel>(query);
                }

                return null;
            }
        }

        public ObservableCollection<RamActivities> RamActivities { get; private set; }
        #endregion

    }
}
