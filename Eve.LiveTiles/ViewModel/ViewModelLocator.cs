using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace Eve.LiveTiles
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    internal class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            DependencyFactory.RegisterTypeAsSingleton(typeof(MainViewModel), typeof(MainViewModel));
            DependencyFactory.RegisterType(typeof(AddAccountViewModel), typeof(AddAccountViewModel));
            DependencyFactory.RegisterType(typeof(RemoveAccountViewModel), typeof(RemoveAccountViewModel));
            DependencyFactory.RegisterTypeAsSingleton(typeof(SkillPlannerViewModel), typeof(SkillPlannerViewModel));
            DependencyFactory.RegisterType(typeof(CharacterDetailViewModel), typeof(CharacterDetailViewModel));
            DependencyFactory.RegisterTypeAsSingleton(typeof(ItemHierarchyViewModel), typeof(ItemHierarchyViewModel));
            DependencyFactory.RegisterTypeAsSingleton(typeof(FitterViewModel), typeof(FitterViewModel));
            DependencyFactory.RegisterTypeAsSingleton(typeof(FitterItemHierarchyViewModel), typeof(FitterItemHierarchyViewModel));
            DependencyFactory.RegisterTypeAsSingleton(typeof(ShipHierarchyViewModel), typeof(ShipHierarchyViewModel));
        }

        public ItemHierarchyViewModel ItemHierarchy { get { return DependencyFactory.Resolve<ItemHierarchyViewModel>(); } }

        public FitterItemHierarchyViewModel FitterItemHierarchy { get { return DependencyFactory.Resolve<FitterItemHierarchyViewModel>(); } }

        public MainViewModel Main { get { return DependencyFactory.Resolve<MainViewModel>(); } }

        public CharacterDetailViewModel CharacterDetail { get { return DependencyFactory.Resolve<CharacterDetailViewModel>(); } }

        public SkillPlannerViewModel SkillPlanner { get { return DependencyFactory.Resolve<SkillPlannerViewModel>(); } }

        public AddAccountViewModel AddAccount { get { return DependencyFactory.Resolve<AddAccountViewModel>(); } }

        public RemoveAccountViewModel RemoveAccount { get { return DependencyFactory.Resolve<RemoveAccountViewModel>(); } }

        public FitterViewModel Fitter { get { return DependencyFactory.Resolve<FitterViewModel>(); } }

        public ShipHierarchyViewModel ShipHierarchy { get { return DependencyFactory.Resolve<ShipHierarchyViewModel>(); } }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}