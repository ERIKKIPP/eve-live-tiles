﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class FitterViewModel : PlannerBaseViewModel
    {

        #region Private Members

        private string _slotItemNameFilter;
        private static IEnumerable<IGrouping<int, SkillAffect>> _skillAffects;
        #endregion

        #region CTOR
        public FitterViewModel()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// saves the fittings
        /// </summary>
        /// <returns></returns>
        public void Save()
        {

        }

        /// <summary>
        /// loads the fittings
        /// </summary>
        /// <returns></returns>
        public void Load()
        {

        }

        #region Fitting
        private void HandleHighSlots(IEnumerable<TypeAttributesModel> query)
        {
            var highslots = (from item in query
                             where item.AttributeType.AttributeID == (int)AttributeIdType.HiSlots
                             select item.TypeAttribute).FirstOrDefault();

            if (highslots != null)
            {
                int count = highslots.ValueInt.HasValue ? highslots.ValueInt.Value : (int)highslots.ValueFloat.GetValueOrDefault();

                SelectedFitting.HighSlots = new List<IVariableGridViewItem>(count);
                for (int i = 0; i < count; i++)
                {
                    HighSlotControl ctrl = new HighSlotControl(null);
                    SelectedFitting.HighSlots.Add(ctrl);
                    //set index
                    (ctrl as ISlotControl).Index = i;
                }
            }
        }

        private void HandleMedSlots(IEnumerable<TypeAttributesModel> query)
        {
            var medslots = (from item in query
                            where item.AttributeType.AttributeID == (int)AttributeIdType.MedSlots
                            select item.TypeAttribute).FirstOrDefault();

            if (medslots != null)
            {
                int count = medslots.ValueInt.HasValue ? medslots.ValueInt.Value : (int)medslots.ValueFloat.GetValueOrDefault();

                SelectedFitting.MediumSlots = new List<IVariableGridViewItem>(count);
                for (int i = 0; i < count; i++)
                {
                    MidSlotControl ctrl = new MidSlotControl(null);
                    SelectedFitting.MediumSlots.Add(ctrl);
                    //set index
                    (ctrl as ISlotControl).Index = i;
                }
            }
        }

        private void HandleLowSlots(IEnumerable<TypeAttributesModel> query)
        {
            var lowslots = (from item in query
                            where item.AttributeType.AttributeID == (int)AttributeIdType.LowSlots
                            select item.TypeAttribute).FirstOrDefault();

            if (lowslots != null)
            {
                int count = lowslots.ValueInt.HasValue ? lowslots.ValueInt.Value : (int)lowslots.ValueFloat.GetValueOrDefault();

                SelectedFitting.LowSlots = new List<IVariableGridViewItem>(count);
                for (int i = 0; i < count; i++)
                {
                    LowSlotControl ctrl = new LowSlotControl(null);
                    SelectedFitting.LowSlots.Add(ctrl);
                    //set index
                    (ctrl as ISlotControl).Index = i;
                }
            }
        }

        private void HandleRigSlots(IEnumerable<TypeAttributesModel> query)
        {
            var rigslots = (from item in query
                            where item.AttributeType.AttributeID == (int)AttributeIdType.RigSlots
                            select item.TypeAttribute).FirstOrDefault();

            if (rigslots != null)
            {
                int count = rigslots.ValueInt.HasValue ? rigslots.ValueInt.Value : (int)rigslots.ValueFloat.GetValueOrDefault();

                SelectedFitting.RigSlots = new List<IVariableGridViewItem>(count);
                for (int i = 0; i < count; i++)
                {
                    RigSlotControl ctrl = new RigSlotControl(null);
                    SelectedFitting.RigSlots.Add(ctrl);
                    //set index
                    (ctrl as ISlotControl).Index = i;
                }
            }
        }

        private void HandleSubSlots(IEnumerable<TypeAttributesModel> query)
        {
            var subslots = (from item in query
                            where item.AttributeType.AttributeID == (int)AttributeIdType.MaxSubSystems
                            select item.TypeAttribute).FirstOrDefault();

            if (subslots != null)
            {
                int count = (int)subslots.ValueFloat.GetValueOrDefault();

                SelectedFitting.SubSlots = new List<IVariableGridViewItem>(count);
                for (int i = 0; i < count; i++)
                {
                    SubSlotControl ctrl = new SubSlotControl(null);
                    SelectedFitting.SubSlots.Add(ctrl);
                    //set index
                    (ctrl as ISlotControl).Index = i;
                }
            }
        }

        private void HandlePanel()
        {
            //List<SlotBaseModel> modules = new List<SlotBaseModel>();

            //modules.AddRange(HighSlots);
            //modules.AddRange(MediumSlots);
            //modules.AddRange(LowSlots);

            SelectedFitting.Panel = new List<IVariableGridViewItem>();
            SelectedFitting.Panel.Add(new ResourcesControl(SelectedFitting, _skillAffects));
            SelectedFitting.Panel.Add(new CapacitorControl(SelectedFitting, _skillAffects));
            SelectedFitting.Panel.Add(new OffenseControl(SelectedFitting, _skillAffects));
            SelectedFitting.Panel.Add(new TargetingControl(SelectedFitting, _skillAffects));
            SelectedFitting.Panel.Add(new NavigationControl(SelectedFitting, _skillAffects));
            SelectedFitting.Panel.Add(new DefenseControl(SelectedFitting, _skillAffects));
        }

        private void HandleFittings()
        {

            var fitquery = SelectedFitting.TypeAttributes.Where(s => s.AttributeCategory.CategoryID == (int)AttributeCategoryType.Fitting);

            HandleHighSlots(fitquery);
            HandleMedSlots(fitquery);
            HandleLowSlots(fitquery);
            HandleRigSlots(fitquery);
            HandleSubSlots(fitquery);
            HandlePanel();

        }
        #endregion

        protected async override void Reset()
        {
            //reset
            SelectedFitting = null;

            RaisePropertyChanged(() => SelectedFitting);

            IList<EveUnit> units = await EveDataRepository.GetAllEveUnits();
            IList<DgmTypeAttribute> dgmTypes = await EveDataRepository.GetAllDgmTypeAttributes();
            IList<DgmAttributeType> dgmtatts = await EveDataRepository.GetAllDgmAttributeTypes();
            IList<DgmAttributeCategory> dgmcats = await EveDataRepository.GetAllDgmAttributeCategories();
            IList<InvType> types = await EveDataRepository.GetAllInvTypes();
            IList<InvGroup> invgroups = await EveDataRepository.GetAllInvGroups();
            IList<InvCategory> invcats = await EveDataRepository.GetAllInvCategories();

            var allAffects = from att1 in dgmTypes
                             join att2 in dgmtatts on att1.AttributeID equals att2.AttributeID
                             join inv in types on att1.TypeID equals inv.TypeID
                             join cats in dgmcats on att2.CategoryID equals cats.CategoryID
                             join invgroup in invgroups on inv.GroupID equals invgroup.GroupID
                             join invcat in invcats on invgroup.CategoryID equals invcat.CategoryID
                             join unit in units on att2.UnitID equals unit.UnitID into unitGroup
                             from unt in unitGroup.DefaultIfEmpty()
                             where cats.CategoryID != (int)AttributeCategoryType.RequiredSkills
                             && cats.CategoryID != (int)AttributeCategoryType.AI
                             && att1.AttributeID != (int)AttributeIdType.PrimaryAttribute
                             && att1.AttributeID != (int)AttributeIdType.SecondaryAttribute
                             && att1.AttributeID != (int)AttributeIdType.SkillLevel
                             select new
                             {
                                 TypeAttribute = att1,
                                 AttributeType = att2,
                                 AttributeCategory = cats,
                                 InventoryType = inv,
                                 InventoryCategory = invcat,
                                 Unit = unt,
                                 InventoryGroup = invgroup
                             };

            _skillAffects = (from affect in allAffects
                             join userSkill in Character.CharacterSheet.Skills on affect.TypeAttribute.TypeID equals userSkill.TypeID
                             select new SkillAffect
                             {
                                 AttributeID = affect.TypeAttribute.AttributeID,
                                 Level = userSkill.Level,
                                 TypeDescription = affect.InventoryType.Description,
                                 TypeID = affect.InventoryType.TypeID,
                                 TypeName = affect.InventoryType.TypeName,
                                 ValueFloat = affect.TypeAttribute.ValueFloat.HasValue ? affect.TypeAttribute.ValueFloat.Value : affect.TypeAttribute.ValueInt.GetValueOrDefault(),
                                 UnitName = affect.Unit != null ? (UnitType)affect.Unit.UnitID : UnitType.None,
                                 InventoryGroup = affect.InventoryGroup.GroupID
                             }).GroupBy(s => s.AttributeID);
        }

        protected override void GoBackClicked()
        {
            Reset();
        }

        protected override void LastChildClicked()
        {
            var query = from groups in SelectedInventory.TypeAttributes
                        from grp in groups
                        select grp;

            SelectedFitting = new ShipFittingModel
            {
                Name = SelectedInventory.InventoryItem.TypeName,
                Description = SelectedInventory.InventoryItem.Description,//use until user creates a description
                Inventory = SelectedInventory,
                TypeAttributes = query,
                RequiredSkills = SelectedInventory.RequiredSkills
            };

            HandleFittings();

            RaisePropertyChanged(() => SelectedFitting);

        }

        #region Slot Selection
        public async void CreateSlotSelection()
        {
            IsBusyDataLoader.LoadingState = LoadingStateType.Loading;

            Func<string, bool> filter = typeName => string.IsNullOrEmpty(_slotItemNameFilter) ? true : typeName.StartsWith(_slotItemNameFilter, StringComparison.CurrentCultureIgnoreCase);

            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<DgmEffect> dgmeffects = await EveDataRepository.GetAllDgmEffects();
            IList<DgmTypeEffect> dgmtypeeffects = await EveDataRepository.GetAllDgmTypeEffects();
            IList<InvGroup> invgroups = await EveDataRepository.GetAllInvGroups();
            IList<DgmTypeAttribute> dgmTypes = await EveDataRepository.GetAllDgmTypeAttributes();

            SlotSelectionList = (from itemcat in invTypes
                                 join dgmtypeeffect in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect.TypeID
                                 join dgmeffect in dgmeffects on dgmtypeeffect.EffectID equals dgmeffect.EffectID
                                 join invgroup in invgroups on itemcat.GroupID equals invgroup.GroupID
                                 join types2 in dgmTypes on itemcat.TypeID equals types2.TypeID into cpuandpowerTypes
                                 where itemcat.Published.GetValueOrDefault() && dgmeffect.EffectID == (int)SelectedSlotType &&
                                 IsModFittable(cpuandpowerTypes, itemcat.TypeID, SelectedFitting.Resources) && filter(itemcat.TypeName)
                                 select new SlotSelectionModel
                                 {
                                     TypeName = itemcat.TypeName,
                                     TypeID = itemcat.TypeID,
                                     GroupName = invgroup.GroupName,
                                     Description = itemcat.Description,
                                     Cpu = GetValue(cpuandpowerTypes, itemcat.TypeID, AttributeIdType.Cpu),
                                     Power = GetValue(cpuandpowerTypes, itemcat.TypeID, AttributeIdType.Power)
                                 }).OrderBy(s => s.TypeName).GroupBy(s => s.GroupName).OrderBy(s => s.Key);

            RaisePropertyChanged(() => SlotSelectionList);

            IsBusyDataLoader.LoadingState = LoadingStateType.Finished;
        }

        private static bool IsModFittable(IEnumerable<DgmTypeAttribute> dgmType, int typeId, ResourcePanelModel resource)
        {
            var cpu = (from item in dgmType
                       where item.AttributeID == (int)AttributeIdType.Cpu &&
                       item.TypeID == typeId
                       select item).FirstOrDefault();

            var power = (from item in dgmType
                         where item.AttributeID == (int)AttributeIdType.Power &&
                         item.TypeID == typeId
                         select item).FirstOrDefault();

            if (power != null && cpu != null)
            {
                return (power.ValueInt != null ? power.ValueInt.Value <= resource.Powergrid.Allowed : (int)power.ValueFloat.GetValueOrDefault() <= resource.Powergrid.Allowed) &&
                    (cpu.ValueInt != null ? cpu.ValueInt.Value <= resource.CPU.Allowed : (int)cpu.ValueFloat.GetValueOrDefault() <= resource.CPU.Allowed);
            }

            return false;
        }

        private static int GetValue(IEnumerable<DgmTypeAttribute> dgmType, int typeId, AttributeIdType attType)
        {
            var query = (from item in dgmType
                         where item.AttributeID == (int)attType &&
                         item.TypeID == typeId
                         select item).FirstOrDefault();

            if (query != null)
            {
                return query.ValueInt != null ? query.ValueInt.Value : (int)query.ValueFloat.GetValueOrDefault();
            }

            return 0;
        }

        /// <summary>
        /// handles turrets and launchers
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        //private async static Task<bool> IsModFittable(InventoryModel invmodel)
        //{
        //        var query = (from item in ItemCategories
        //                     where item...AttributeID == (int)DgmEffectIdType.LauncherFitted
        //                     select grp.TypeAttribute).FirstOrDefault();


        //    return false;

        //}

        //private bool IsModLauncherOrTurret(InventoryModel invmodel, DgmEffectIdType dgmId)
        //{
        //    var query = (from item in ItemCategories
        //                 where item...AttributeID == (int)DgmEffectIdType.LauncherFitted
        //                 select grp.TypeAttribute).FirstOrDefault();

        //    if (query != null)
        //    {
        //        int x= query.ValueInt != null ? query.ValueInt.Value : (int)query.ValueFloat.GetValueOrDefault();
        //    }
        //}
        #endregion
        #endregion

        #region Properties
        public ShipFittingModel SelectedFitting { get; set; }

        public ObservableCollection<ShipFittingModel> Fittings { get; private set; }

        #region Slot Selection
        public ISlotControl SelectedSlot { get; set; }

        public string SlotItemNameFilter
        {
            get { return _slotItemNameFilter; }
            set { _slotItemNameFilter = value; CreateSlotSelection(); }
        }

        public DgmEffectIdType SelectedSlotType { get; set; }

        public IEnumerable<IGrouping<string, SlotSelectionModel>> SlotSelectionList { get; private set; }

        public bool PopupOpen { get { return SelectedFitting == null; } }
        #endregion
        #endregion

        #region Events

        #endregion
    }
}
