﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class ShipHierarchyViewModel : ItemHierarchyViewModelBase
    {
        #region Private Members

        #endregion

        #region CTOR
        public ShipHierarchyViewModel()
            : base()
        {

        }
        #endregion

        #region Methods
        public override void Initialize()
        {
            Initialize((int)MarketGroupType.Ships);
        }

        protected async override void FilteredData()
        {
            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<InvMarketGroup> mktGrp = await EveDataRepository.GetAllInvMarketGroups();
            IList<InvGroup> invgroups = await EveDataRepository.GetAllInvGroups();
            IList<InvCategory> invcats = await EveDataRepository.GetAllInvCategories();

            ItemsSource = from inv in invTypes
                          join invgroup in invgroups on inv.GroupID equals invgroup.GroupID
                          join invcat in invcats on invgroup.CategoryID equals invcat.CategoryID
                          join mkt in mktGrp on inv.MarketGroupID equals mkt.MarketGroupID
                          where inv.Published.GetValueOrDefault() && invcat.CategoryID == (int)InventoryCategoryType.Ship
                          && FilterFunction(inv.TypeName)
                          orderby inv.TypeName
                          select new InvCategoryModel
                          {
                              Description = inv.Description,
                              MarketGroupID = mkt.MarketGroupID,
                              MarketGroupName = inv.TypeName,
                              ParentGroupID = mkt.ParentGroupID,
                              Item = inv,
                              IsFilter = true
                          };
        }

        #endregion


    }
}
