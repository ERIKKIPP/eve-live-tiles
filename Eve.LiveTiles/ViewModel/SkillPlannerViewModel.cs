﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Eve.LiveTiles.Extensions;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml;

namespace Eve.LiveTiles
{
    internal sealed class SkillPlannerViewModel : PlannerBaseViewModel
    {
        #region Private Members
        private Popup _popup;
        private const string FILENAME = "{0}-SkillPlans.xml";
        private SkillQueued _selectedSkill;
        private SkillQueued _selectedQueuedSkill;
        private SkillPlan _selectedPlan;
        private IEnumerable<RequiredSkillModel> _requiredSkills;
        private CollectionViewSource _skillsPlanningCollection;
        private ReorderedItem<SkillQueued> _reorderedItem;
        private string _skillFilter;
        private string _grpFilter;
        private ComboBoxItem _skillsFilterType;
        private SkillsFilterType _selectedFilterType;
        private AttributeFilterModel _selectedAttributesItem;
        private string _selectedGroupFilter;
        private List<ComboBoxItem> _filterList;
        #endregion

        #region CTOR
        public SkillPlannerViewModel()
        {
            _selectedFilterType = SkillsFilterType.None;

            _skillsPlanningCollection = new CollectionViewSource();
            _skillsPlanningCollection.IsSourceGrouped = true;

            SkillPlans = new ObservableCollection<SkillPlan>();
            SkillPlans.CollectionChanged += SkillPlans_CollectionChanged;

            AddSkillToQueueCommand = new RelayCommand(() => AddSkillToQueueAction(), () => CanExecuteAddSkillToQueueAction());

            AddSkillsToQueueCommand = new RelayCommand(() => AddSkillsToQueueAction(), () => IsVisibleInventory);

            AddCertsToQueueCommand = new RelayCommand(() => AddCertsToQueueAction(), () => IsVisibleInventory);

            EditNoteCommand = new RelayCommand(() => EditNoteAction(), () => SelectedQueuedSkill != null && _selectedPlan != null);

            RemoveSkillFromQueueCommand = new RelayCommand(() => RemoveSkillFromQueueAction(), () => CanExecuteRemoveSkillFromQueueAction());

            RemoveSkillPlanCommand = new RelayCommand(() => RemovePlanAction(), () => _selectedPlan != null);

            AddSkillPlanCommand = new RelayCommand(() => AddSkillPlanAction());

            NavigateSkillsCommand = new RelayCommand(() =>
            {
                NavigationHelper.NavigateToView<SkillPlanningSkillsPage>(Character);
            });

            NavigateShipsCommand = new RelayCommand(() =>
            {
                NavigationHelper.NavigateToView<SkillPlanningShipsPage>(Character);
            });

            NavigateItemsCommand = new RelayCommand(() =>
            {
                NavigationHelper.NavigateToView<SkillPlanningItemsPage>(Character);
            });

            NeuralRemapCommand = new RelayCommand(() =>
            {
                NeuralRemapAction();
            });
        }


        #endregion

        #region Methods
        /// <summary>
        /// TODO
        /// </summary>
        private void AddCertsToQueueAction()
        {
            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();

            foreach (var certs in SelectedInventory.RequiredCerts)
            {
                //var skill = (from skillGrp in eve.SkillTree.SkillGroups
                //             from skilllvl in skillGrp.Skills
                //             join userSkill in _selectedCharacter.CharacterSheet.Skills on skilllvl.TypeID equals userSkill.TypeID into userSkills
                //             from item in userSkills.DefaultIfEmpty(new CharacterSkill { Level = 0 })
                //             where skilllvl.TypeID == reqSkill.TypeID && reqSkill.SkillLevel > item.Level
                //             select new SkillQueued
                //             {
                //                 Skill = skilllvl,
                //                 ToLevel = reqSkill.SkillLevel,
                //                 FromLevel = item.Level
                //             }).FirstOrDefault();

                //if (skill != null)
                //{
                //    await AddSkillToQueue(skill);
                //}
            }
        }

        /// <summary>
        /// sends a message of type AddSkillPlan
        /// </summary>
        private void AddSkillPlanAction()
        {
            AddPlanDialog addPlan = new AddPlanDialog();
            addPlan.CancelRequested += CancelRequested;
            addPlan.UpdateRequested += async (object sender, EventArgs e) =>
                {

                    SkillPlan sp = new SkillPlan()
                    {
                        Name = (e as GenericEventArgs<string>).Data
                    };

                    SkillPlans.Add(sp);
                    await Save();

                    _popup.IsOpen = false;

                };

            OpenPopup(addPlan);
        }

        private void OpenPopup(Page page)
        {
            _popup = new Popup();
            _popup.Child = page;

            var bounds = Window.Current.Bounds;
            page.Width = bounds.Width;
            page.Height = bounds.Height;

            double verticalOff = bounds.Height - page.Height;
            _popup.VerticalOffset = verticalOff / 2;

            _popup.IsOpen = true;
        }

        /// <summary>
        /// checks whether or not RemoveSkillFromQueueCommand is enabled
        /// </summary>
        /// <returns></returns>
        private bool CanExecuteRemoveSkillFromQueueAction()
        {
            if (SelectedQueuedSkill == null || _selectedPlan == null)
            {
                return false;
            }

            return !AnyRequiredSkillInQueueFor(SelectedQueuedSkill.Skill);
        }

        /// <summary>
        /// called from CanExecuteRemoveSkillFromQueueAction
        /// </summary>
        /// <param name="parentSkill"></param>
        /// <returns></returns>
        private bool AnyRequiredSkillInQueueFor(Skill parentSkill)
        {
            return _selectedPlan.Any(s => s.Skill.RequiredSkills.Any(x => x.TypeID == parentSkill.TypeID));
        }

        /// <summary>
        /// checks whether or not RemoveSkillFromQueueCommand is enabled
        /// </summary>
        /// <returns></returns>
        private bool CanExecuteAddSkillToQueueAction()
        {
            bool b = _selectedSkill != null && _selectedPlan != null;

            //bail if these are null
            if (!b)
            {
                return b;
            }

            //if exists in queue already
            SkillQueued queued = SelectedSkillPlan.SkillQueuedList.Where(s => s.Skill.TypeID == _selectedSkill.Skill.TypeID).FirstOrDefault();
            if (queued != null)
            {
                b &= _selectedSkill.ToLevel > queued.ToLevel;//the selected skill level must be greater than the queued level
            }
            else
            {
                b &= _selectedSkill.ToLevel > _selectedSkill.FromLevel;
            }

            return b;
        }

        /// <summary>
        /// 
        /// </summary>
        private void NeuralRemapAction()
        {
            NeuralRemap = new NeuralRemapModel(this);
            NeuralRemapPage remap = new NeuralRemapPage();
            remap.OkRequested += (object sender, EventArgs e) =>
            {

                _popup.IsOpen = false;
            };
            OpenPopup(remap);
        }

        /// <summary>
        /// sends a message of type EditNote
        /// </summary>
        private void EditNoteAction()
        {
            EditNoteDialog editNote = new EditNoteDialog(SelectedQueuedSkill.Note);
            editNote.CancelRequested += CancelRequested;
            editNote.UpdateRequested += async (object sender, EventArgs e) =>
            {
                SelectedQueuedSkill.Note = (e as GenericEventArgs<string>).Data;
                await Save();

                _popup.IsOpen = false;
            };

            OpenPopup(editNote);
        }

        private async void AddSkillsToQueueAction()
        {
            foreach (var reqSkill in SelectedInventory.RequiredSkills)
            {
                var skill = (from skillGrp in EveDataRepository.SkillTree
                             from skilllvl in skillGrp.Skills
                             join userSkill in Character.CharacterSheet.Skills on skilllvl.TypeID equals userSkill.TypeID into userSkills
                             from item in userSkills.DefaultIfEmpty(new CharacterSkill { Level = 0 })
                             where skilllvl.TypeID == reqSkill.TypeID && reqSkill.SkillLevel > item.Level
                             select new SkillQueued
                             {
                                 Skill = skilllvl,
                                 ToLevel = reqSkill.SkillLevel,
                                 FromLevel = item.Level
                             }).FirstOrDefault();

                if (skill != null)
                {
                    await AddSkillToQueue(skill);
                }
            }
        }

        /// <summary>
        /// adds a skill to the queue called from AddSkillToQueueCommand
        /// </summary>
        private async void AddSkillToQueueAction()
        {
            await AddSkillToQueue(_selectedSkill);
            AddSkillToQueueCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// adds the skill to the queue
        /// </summary>
        /// <param name="queuedSkill"></param>
        /// <returns></returns>
        private async Task AddSkillToQueue(SkillQueued queuedSkill)
        {
            IList<SkillQueued> skillQueue = new List<SkillQueued>();
            GetRequiredSkills(queuedSkill.Skill, skillQueue);

            //reverse the order and add to queue
            foreach (var item in skillQueue.Reverse())
            {
                //if exists in queue already
                SkillQueued queued = SelectedSkillPlan.SkillQueuedList.Where(s => s.Skill.TypeID == item.Skill.TypeID).FirstOrDefault();
                if (queued != null)
                {
                    if (queued.ToLevel >= item.ToLevel)//the queued skill level must be >= the required level
                    {
                        continue;
                    }
                    else
                    {
                        queued.ToLevel = item.ToLevel;//upgrade already queued level
                        continue;
                    }
                }

                _selectedPlan.SkillQueuedList.Add(item);
            }

            _selectedPlan.SkillQueuedList.Add(queuedSkill);
            await Save();
        }

        /// <summary>
        /// gets required skills for parentSkill, populates skillQueue
        /// </summary>
        /// <param name="parentSkill"></param>
        /// <param name="skillQueue"></param>
        private void GetRequiredSkills(Skill parentSkill, IList<SkillQueued> skillQueue)
        {
            foreach (var reqSkill in parentSkill.RequiredSkills)
            {
                var skill = (from grp in EveDataRepository.SkillTree
                             where grp.Skills.Any(c => c.TypeID == reqSkill.TypeID)
                             select (grp.Skills.Where(s => s.TypeID == reqSkill.TypeID)).FirstOrDefault()).FirstOrDefault();

                CharacterSkill charSkill = Character.CharacterSheet.Skills.Where(s => s.TypeID == reqSkill.TypeID).FirstOrDefault();

                //check if character has trained the skill
                if (charSkill != null)
                {
                    //if the character skill is less than required skill then add
                    if (charSkill.Level < reqSkill.SkillLevel)
                    {
                        skillQueue.Add(new SkillQueued
                        {
                            FromLevel = charSkill.Level,
                            ToLevel = reqSkill.SkillLevel,
                            Note = string.Format(CultureInfo.CurrentUICulture, ResourceHelper.Instance.GetValue("RequiredSkillNote"), parentSkill.TypeName),
                            Skill = skill
                        });
                    }
                }
                else
                {
                    skillQueue.Add(new SkillQueued
                    {
                        FromLevel = 0,
                        ToLevel = reqSkill.SkillLevel,
                        Note = string.Format(CultureInfo.CurrentUICulture, ResourceHelper.Instance.GetValue("RequiredSkillNote"), parentSkill.TypeName),
                        Skill = skill
                    });
                }

                GetRequiredSkills(skill, skillQueue);
            }
        }

        /// <summary>
        /// removes a skill from the queue, called from RemoveSkillFromQueueCommand
        /// </summary>
        private async void RemoveSkillFromQueueAction()
        {
            _selectedPlan.SkillQueuedList.Remove(SelectedQueuedSkill);
            await Save();
        }

        /// <summary>
        /// removes a plan, called from RemoveSkillPlanCommand
        /// </summary>
        private async void RemovePlanAction()
        {
            SkillPlans.Remove(_selectedPlan);
            await Save();
        }

        /// <summary>
        /// saves the skill plans
        /// </summary>
        /// <returns></returns>
        public async Task Save()
        {
            Storage<ObservableCollection<SkillPlan>> database = new Storage<ObservableCollection<SkillPlan>>();

            await database.SaveAsync(SkillPlans, string.Format(CultureInfo.InvariantCulture, FILENAME, Character.CharacterID), StorageLocation.Local);
        }

        /// <summary>
        /// loads the skill plans
        /// </summary>
        /// <returns></returns>
        public async Task Load()
        {
            bool needsSaving = false;
            SkillPlans.Clear();
            Storage<ObservableCollection<SkillPlan>> database = new Storage<ObservableCollection<SkillPlan>>();
            string filename = string.Format(CultureInfo.InvariantCulture, FILENAME, Character.CharacterID);

            if (await ApplicationData.Current.LocalFolder.ContainsFileAsync(filename))
            {
                ObservableCollection<SkillPlan> temp = await database.OpenAsync(filename, StorageLocation.Local);

                foreach (var sp in temp)
                {
                    for (int i = 0; i < sp.Count(); i++)
                    {
                        if (sp.SkillQueuedList[i].IsCompleted(Character))
                        {
                            needsSaving = true;
                            sp.SkillQueuedList.RemoveAt(i);
                        }
                    }

                    SkillPlans.Add(sp);
                }

                //made changes to the plan
                if (needsSaving)
                {
                    await Save();
                }
            }
        }

        /// <summary>
        /// called when the user drags and drops a skill in queue
        /// </summary>
        /// <returns></returns>
        public async Task Reordered()
        {
            if (_reorderedItem != null)
            {
                int replacedIndex = -1;
                IList<RequiredSkillModel> skillList = new List<RequiredSkillModel>();

                //moved up
                if (_reorderedItem.OriginalIndex > _reorderedItem.NewIndex)
                {
                    //get required skills for this skill
                    GetRequiredSkillsFor(_reorderedItem.Item.Skill, skillList);

                    //enumerate through the required skills
                    foreach (var item in skillList)
                    {
                        //see if this skill is queued
                        int x = _selectedPlan.SkillQueuedList.IndexOf<SkillQueued>(s => s.Skill.TypeID == item.TypeID);

                        //if the skill is queued and at this point we know it's mandatory the skill needs to be > NewIndex
                        if (x != -1)
                        {
                            //get the character skill to check current level
                            CharacterSkill cSkill = Character.CharacterSheet.Skills.Where(s => s.TypeID == item.TypeID).FirstOrDefault();

                            //if the character skill isn't found it could mean the character has not injected the skill but still has it planned
                            //if the skill is planned and the minimum criteria is met then skip
                            if (cSkill != null && cSkill.Level >= item.SkillLevel)
                            {
                                continue;
                            }
                            else if (x > _reorderedItem.NewIndex)
                            {
                                if (replacedIndex < x)
                                {
                                    replacedIndex = x;
                                }
                            }
                        }
                    }
                }
                else//moved down
                {
                    for (int i = _reorderedItem.NewIndex; i >= 0; i--)
                    {
                        SkillQueued queue = _selectedPlan.SkillQueuedList[i];
                        skillList.Clear();
                        GetRequiredSkillsFor(queue.Skill, skillList);

                        //enumerate through the required skills
                        foreach (var item in skillList)
                        {
                            //see if this skill is queued
                            int x = _selectedPlan.SkillQueuedList.IndexOf<SkillQueued>(s => s.Skill.TypeID == item.TypeID);

                            //if the skill is queued and at this point we know it's mandatory
                            if (x != -1)
                            {
                                //get the character skill to check current level
                                CharacterSkill cSkill = Character.CharacterSheet.Skills.Where(s => s.TypeID == item.TypeID).FirstOrDefault();

                                //if the character skill isn't found it could mean the character has not injected the skill but still has it planned
                                //if the skill is planned and the minimum criteria is met then skip
                                if (cSkill != null && cSkill.Level >= item.SkillLevel)
                                {
                                    continue;
                                }
                                else if (x == _reorderedItem.NewIndex)
                                {
                                    replacedIndex = i;
                                }
                            }
                        }
                    }
                }

                //a skill is mandatory below the moved skill so we need to move back
                if (replacedIndex != -1)
                {
                    _selectedPlan.SkillQueuedList.RemoveAt(_reorderedItem.NewIndex);
                    _selectedPlan.SkillQueuedList.Insert(replacedIndex, _reorderedItem.Item);
                }

                await Save();
            }
        }

        protected override void Reset()
        {
            SelectedInventory = null;
            RaisePropertyChanged(() => IsVisibleInventory);

            AddSkillsToQueueCommand.RaiseCanExecuteChanged();
            AddCertsToQueueCommand.RaiseCanExecuteChanged();
        }

        protected override void GoBackClicked()
        {
            AddSkillsToQueueCommand.RaiseCanExecuteChanged();
            AddCertsToQueueCommand.RaiseCanExecuteChanged();
        }

        protected override void LastChildClicked()
        {
            AddSkillsToQueueCommand.RaiseCanExecuteChanged();
            AddCertsToQueueCommand.RaiseCanExecuteChanged();
        }
        #endregion

        #region Properties
        public NeuralRemapModel NeuralRemap { get; private set; }

        public string SkillFilter
        {
            get { return _skillFilter; }
            set { _skillFilter = value; RaisePropertyChanged(() => SkillsPlanningList); }
        }

        /// <summary>
        /// used in shipplanning page
        /// </summary>
        public RelayCommand AddSkillsToQueueCommand { get; private set; }

        /// <summary>
        /// used in shipplanning page
        /// </summary>
        public RelayCommand AddCertsToQueueCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand NavigateSkillsCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand NavigateShipsCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand NavigateItemsCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand NeuralRemapCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand AddSkillToQueueCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand EditNoteCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand RemoveSkillFromQueueCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand AddSkillPlanCommand { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand RemoveSkillPlanCommand { get; private set; }

        /// <summary>
        /// selected skill from the skill selection view
        /// has not yet been added to queue
        /// </summary>
        public SkillQueued SelectedSkill
        {
            get { return _selectedSkill; }
            set
            {
                _selectedSkill = value;
                RaisePropertyChanged(() => SelectedSkill);
                AddSkillToQueueCommand.RaiseCanExecuteChanged();

                IList<RequiredSkillModel> skillList = new List<RequiredSkillModel>();
                GetRequiredSkillsFor(SelectedSkill.Skill, skillList);
                RequiredSkills = skillList;
            }
        }

        /// <summary>
        /// queued skill
        /// </summary>
        public SkillQueued SelectedQueuedSkill
        {
            get { return _selectedQueuedSkill; }
            set
            {
                _selectedQueuedSkill = value;
                RaisePropertyChanged(() => SelectedQueuedSkill);
                EditNoteCommand.RaiseCanExecuteChanged();
                RemoveSkillFromQueueCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// selected skill plan
        /// </summary>
        public SkillPlan SelectedSkillPlan
        {
            get { return _selectedPlan; }
            set
            {
                _selectedPlan = value;
                RaisePropertyChanged(() => SelectedSkillPlan);
                AddSkillToQueueCommand.RaiseCanExecuteChanged();
                RemoveSkillPlanCommand.RaiseCanExecuteChanged();
                RemoveSkillFromQueueCommand.RaiseCanExecuteChanged();
                EditNoteCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// all the skill plans
        /// </summary>
        public ObservableCollection<SkillPlan> SkillPlans { get; private set; }

        /// <summary>
        /// required skills displayed in skill detail page
        /// </summary>
        public IEnumerable<RequiredSkillModel> RequiredSkills
        {
            get { return _requiredSkills; }
            set
            {
                _requiredSkills = value;

                RaisePropertyChanged(() => RequiredSkills);
            }
        }

        #region Filters
        /// <summary>
        /// item source for the group filter
        /// GroupFilter
        /// </summary>
        public IEnumerable<string> SkillGroups
        {
            get
            {
                List<string> list = new List<string>();
                list.Add(" ");

                list.AddRange(from skillGrp in EveDataRepository.SkillTree
                              where skillGrp.Skills.Any(s => s.Published)
                              orderby skillGrp.GroupName
                              select skillGrp.GroupName);

                return list;

            }
        }

        /// <summary>
        /// when an item is selected in the group filter
        /// </summary>
        public string SelectedGroupFilter
        {
            get { return _grpFilter; }
            set
            {
                _grpFilter = value;
                _selectedGroupFilter = null;

                if (!string.IsNullOrWhiteSpace(_grpFilter))
                {
                    _selectedGroupFilter = _grpFilter;
                }

                RaisePropertyChanged(() => SkillsPlanningList);
            }
        }

        public ComboBoxItem FilterType
        {
            get { return _skillsFilterType; }
            set
            {
                _skillsFilterType = value;
                _selectedAttributesItem = null;
                _selectedFilterType = SkillsFilterType.None;

                _selectedFilterType = (SkillsFilterType)value.Tag;

                if (_selectedFilterType != SkillsFilterType.Attribute)
                {
                    RaisePropertyChanged(() => SkillsPlanningList);
                }

                RaisePropertyChanged(() => AttributesVisible);
            }
        }

        public bool AttributesVisible { get { return _selectedFilterType == SkillsFilterType.Attribute; } }

        public ObservableCollection<AttributeFilterModel> AttributesList
        {
            get
            {
                return new ObservableCollection<AttributeFilterModel>((from primary in EveDataRepository.SkillTree.SelectMany(s => s.Skills)
                                                                       where primary.PrimaryAttribute != AttributeType.None || primary.SecondaryAttribute != AttributeType.None
                                                                       select new AttributeFilterModel//C# compiler overrides Equals and GetHashCode for anonymous types so distinct works
                                                                            {
                                                                                PrimaryAttribute = primary.PrimaryAttribute,
                                                                                SecondaryAttribute = primary.SecondaryAttribute
                                                                            }).Distinct().OrderBy(s => s.PrimaryAttribute));

            }
        }

        public AttributeFilterModel SelectedAttributesItem { get { return _selectedAttributesItem; } set { _selectedAttributesItem = value; RaisePropertyChanged(() => SkillsPlanningList); } }

        public IEnumerable<ComboBoxItem> FilterList
        {
            get
            {
                if (_filterList == null)
                {
                    _filterList = new List<ComboBoxItem>();
                    _filterList.Add(new ComboBoxItem() { Content = " ", Tag = SkillsFilterType.None });
                    _filterList.Add(new ComboBoxItem() { Content = ResourceHelper.Instance.GetValue("Attribute"), Tag = SkillsFilterType.Attribute });
                    _filterList.Add(new ComboBoxItem() { Content = ResourceHelper.Instance.GetValue("Known"), Tag = SkillsFilterType.Known });
                    _filterList.Add(new ComboBoxItem() { Content = ResourceHelper.Instance.GetValue("NotKnown"), Tag = SkillsFilterType.NotKnown });
                    _filterList.Add(new ComboBoxItem() { Content = ResourceHelper.Instance.GetValue("Injected"), Tag = SkillsFilterType.Injected });
                }

                return _filterList;
            }
        }
        #endregion

        /// <summary>
        /// source for SkillPlanningSkillsPage
        /// </summary>
        public ObservableCollection<IGrouping<string, SkillModel>> SkillsPlanningList
        {
            get
            {
                Func<string, bool> filter = typeName => string.IsNullOrEmpty(_skillFilter) ? true : typeName.StartsWith(_skillFilter, StringComparison.CurrentCultureIgnoreCase);
                Func<string, bool> grpfilter = grpName => string.IsNullOrEmpty(_selectedGroupFilter) ? true : grpName.Equals(_selectedGroupFilter, StringComparison.CurrentCultureIgnoreCase);

                Func<CharacterSkill, bool> miscfilter = (skill) =>
                    {
                        switch (_selectedFilterType)
                        {
                            case SkillsFilterType.Known://anything with level => 0
                                return skill.Level >= 0;
                            case SkillsFilterType.NotKnown://anything with not trained
                                return skill.Level == -1;
                            case SkillsFilterType.Injected://level 0
                                return skill.Level == 0;
                            default:
                                break;
                        }

                        return true;

                    };

                Func<Skill, bool> attfilter = (skill) =>
                {
                    if (_selectedAttributesItem == null)
                    {
                        return true;
                    }

                    return skill.PrimaryAttribute == _selectedAttributesItem.PrimaryAttribute &&
                        skill.SecondaryAttribute == _selectedAttributesItem.SecondaryAttribute;
                };

                IEnumerable<IGrouping<string, SkillModel>> skillList = (from skillGrp in EveDataRepository.SkillTree
                                                                        from skill in skillGrp.Skills
                                                                        join skillpln in _selectedPlan.SkillQueuedList on skill.TypeID equals skillpln.Skill.TypeID into queuedSkills
                                                                        from queue in queuedSkills.DefaultIfEmpty()
                                                                        join userSkill in Character.CharacterSheet.Skills on skill.TypeID equals userSkill.TypeID into userSkills
                                                                        from item in userSkills.DefaultIfEmpty(new CharacterSkill { Level = -1 })
                                                                        where skill.Published && filter(skill.TypeName) && grpfilter(skillGrp.GroupName)
                                                                        && miscfilter(item) && attfilter(skill)
                                                                        select new SkillModel
                                                                        {
                                                                            GroupName = skillGrp.GroupName,
                                                                            Description = skill.Description,
                                                                            TypeName = skill.TypeName,
                                                                            TypeID = skill.TypeID,
                                                                            Level = item.Level,
                                                                            PrimaryAttribute = skill.PrimaryAttribute,
                                                                            SecondaryAttribute = skill.SecondaryAttribute,
                                                                            InQueue = queue != null

                                                                        }).OrderBy(s => s.TypeName).GroupBy(x => x.GroupName).OrderBy(s => s.Key);

                return new ObservableCollection<IGrouping<string, SkillModel>>(skillList);
            }
        }


        #endregion

        #region Events

        private void CancelRequested(object sender, EventArgs e)
        {
            _popup.IsOpen = false;
        }

        void SkillQueuedList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (_reorderedItem != null && _reorderedItem.Item.Skill.TypeID == (e.NewItems[0] as SkillQueued).Skill.TypeID)
                    {
                        _reorderedItem.NewIndex = e.NewStartingIndex;
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Remove:
                    _reorderedItem = new ReorderedItem<SkillQueued>() { Item = e.OldItems[0] as SkillQueued, OriginalIndex = e.OldStartingIndex };
                    break;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
                default:
                    break;
            }
        }

        void SkillPlans_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (var item in e.NewItems)
                        {
                            (item as SkillPlan).SkillQueuedList.CollectionChanged += SkillQueuedList_CollectionChanged;
                        }
                        break;
                    case NotifyCollectionChangedAction.Move:
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (var item in e.NewItems)
                        {
                            (item as SkillPlan).SkillQueuedList.CollectionChanged -= SkillQueuedList_CollectionChanged;
                        }
                        break;
                    case NotifyCollectionChangedAction.Replace:
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion



    }
}
