﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Popups;

namespace Eve.LiveTiles
{
    internal sealed class RemoveAccountViewModel : VMBase
    {

        public RemoveAccountViewModel()
            : base()
        {
            RemoveAccountCommand = new RelayCommand<string>((keyId) => RemoveAccountAction(keyId));
        }

        private async void RemoveAccountAction(string keyId)
        {
            // Create the message dialog and set its content;
            var yesnoDialog = new MessageDialog(ResourceHelper.Instance.GetValue("DeleteAccountConfirmation"));

            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            yesnoDialog.Commands.Add(new UICommand(ResourceHelper.Instance.GetValue("Ok"), async (command) =>
                {
                    EveAccount account = Accounts.Where(s => string.Equals(keyId, s.KeyId)).FirstOrDefault();

                    if (Accounts.Remove(account))
                    {
                        IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();

                        //reset
                        Characters = new ObservableCollection<EveCharacter>(Accounts.SelectMany(a => a.Characters));

                        //save to disk
                        await eve.SaveAccountsAsync(Accounts);

                        //send message to listeners, might have removed all accounts so we need to display a msg
                        Messenger.Default.Send<MessageType>(MessageType.Update);
                    }

                }));

            yesnoDialog.Commands.Add(new UICommand(ResourceHelper.Instance.GetValue("CancelOption"), null));

            // Set the command that will be invoked by default
            yesnoDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            yesnoDialog.CancelCommandIndex = 1;

            // Show the message dialog
            await yesnoDialog.ShowAsync();
        }

        public RelayCommand<string> RemoveAccountCommand { get; private set; }

    }
}
