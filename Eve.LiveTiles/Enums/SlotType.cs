﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal enum SlotType
    {
        HighSlots,

        MediumSlots,

        LowSlots,

        RigSlots,

        SubSlots,

        Panel
    }
}
