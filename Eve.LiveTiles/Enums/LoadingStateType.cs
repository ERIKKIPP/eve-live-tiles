﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    /// <summary>
    /// Possible loading states for the DataLoader
    /// </summary>
    public enum LoadingStateType
    {
        /// <summary>None</summary>
        None,
        /// <summary>Loading</summary>
        Loading,
        /// <summary>Finished</summary>
        Finished,
        /// <summary>Error</summary>
        Error
    }
}
