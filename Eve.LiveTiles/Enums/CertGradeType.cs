﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    public enum CertGradeType
    {
        None,
        Basic, 
        Standard, 
        Improved, 
        Advanced, 
        Elite
    }
}
