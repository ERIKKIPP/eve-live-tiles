﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    public enum MessageType
    {
        /// <summary>
        /// 
        /// </summary>
        Update,

        /// <summary>
        /// 
        /// </summary>
        FinishedInitalLoad,

        /// <summary>
        /// 
        /// </summary>
        ClearBreadCrumb,

        /// <summary>
        /// navigate up a level from 
        /// </summary>
        GoBack

    }
}
