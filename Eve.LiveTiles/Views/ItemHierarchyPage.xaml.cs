﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Eve.LiveTiles
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ItemHierarchyPage : Page
    {     
        public ItemHierarchyPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            object obj = e.Parameter;

            if (obj == null)
            {
                (DataContext as ItemHierarchyViewModel).Initialize();
            }
            else
            {
                (DataContext as ItemHierarchyViewModel).ItemsSource = obj as IEnumerable<InvCategoryModel>;
            }
        }

        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            InvCategoryModel model = e.ClickedItem as InvCategoryModel;

            if (model != null)
            {
                (DataContext as ItemHierarchyViewModel).BreadCrumbDown(model);

                if (model.HasTypes != null && !model.HasTypes.Value)//the market group was selected
                {
                    this.Frame.Navigate(typeof(ItemHierarchyPage), model.Children);
                }
                else if (model.HasTypes != null && model.HasTypes.Value)//an inventory item was selected
                {
                    var children = from inv in model.Inventory
                                   where inv.MarketGroupID == model.MarketGroupID
                                   orderby inv.TypeName
                                   select new InvCategoryModel
                                   {
                                       Description = inv.Description,
                                       MarketGroupID = inv.MarketGroupID,
                                       MarketGroupName = inv.TypeName,
                                       ParentGroupID = model.MarketGroupID,//this is where we want to go back to
                                       Item = inv

                                   };

                    this.Frame.Navigate(typeof(ItemHierarchyPage), children);
                }
                else
                {
                    Messenger.Default.Send<InvCategoryModel>(model);

                    this.Frame.Navigate(typeof(ItemHierarchyPage), model.Children);//display empty results
                }
            }
        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send<MessageType>(MessageType.GoBack);

            (DataContext as ItemHierarchyViewModel).BreadCrumbUp();

            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }

    }
}
