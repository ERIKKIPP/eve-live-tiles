﻿using Eve.LiveTiles.DataModel;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Eve.LiveTiles
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class FitterPage : LayoutAwarePage
    {
        public FitterPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)//need to reset with new character data
            {
                (DataContext as FitterViewModel).Character = e.Parameter as EveCharacter;
                (DataContext as FitterViewModel).Initialize();
            }

            Messenger.Default.Send<MessageType>(MessageType.ClearBreadCrumb);
            ItemsFrame.Navigate(typeof(ShipHierarchyPage));
        }

        private void HighSlotsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            UpdateSlotInfo((e.ClickedItem as ISlotControl), DgmEffectIdType.HiPower);
        }

        private void MedSlotsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            UpdateSlotInfo((e.ClickedItem as ISlotControl), DgmEffectIdType.MedPower);
        }

        private void LowSlotsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            UpdateSlotInfo((e.ClickedItem as ISlotControl), DgmEffectIdType.LoPower);
        }

        private void RigSlotsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            UpdateSlotInfo((e.ClickedItem as ISlotControl), DgmEffectIdType.RigSlot);
        }

        private void SubSlotsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            UpdateSlotInfo((e.ClickedItem as ISlotControl), DgmEffectIdType.SubSystem);
        }

        private void UpdateSlotInfo(ISlotControl control, DgmEffectIdType slotType)
        {
            (DataContext as FitterViewModel).SelectedSlot = control;
            (DataContext as FitterViewModel).SelectedSlotType = slotType;
            this.Frame.Navigate(typeof(SlotSelectionPage));
        }

       
    }
}
