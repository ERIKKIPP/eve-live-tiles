﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Eve.LiveTiles
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CharacterSheetPage : Page
    {
        public CharacterSheetPage()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            CharacterDetailViewModel vm = DataContext as CharacterDetailViewModel;

            List<IVariableGridViewItem> list = new List<IVariableGridViewItem>();

            if (vm.HasCharacterSheetAccess)
            {
                list.Add(new CharacterSheetControl(vm.Character));
                list.Add(new CharacterAttributesControl(vm.Character));
            }         

            if (vm.HasAccess(Data.CharacterMethodType.SkillInTraining))
            {
                list.Add(new CharacterTrainingControl(vm.Character));
            }

            if (vm.HasAccess(Data.CharacterMethodType.SkillQueue))
            {
                list.Add(new CharacterSkillQueueControl(vm.Character));
            }

            list.Add(new AccountInfoControl(vm.Character));

            VariableGridView.ItemsSource = list;
        }


    }
}
