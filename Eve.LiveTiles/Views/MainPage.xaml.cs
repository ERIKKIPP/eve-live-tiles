﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using Eve.LiveTiles.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Eve.LiveTiles
{
    /// <summary>
    /// 
    /// </summary>
    public sealed partial class MainPage : LayoutAwarePage
    {
        #region Private Members
        private const string NOTIFICATION_ID = "NotificationTask";
        private const string NOTIFICATION_ENTRY = "Eve.LiveTiles.Tasks.TileNotificationTask";

        private const string MAINTENANCE_ID = "MaintenanceTask";
        private const string MAINTENANCE_ENTRY = "Eve.LiveTiles.Tasks.MaintenanceTask";

        private const string SKILLQUEUE_ID = "SkillQueueLessThan24Task";
        private const string SKILLQUEUE_ENTRY = "Eve.LiveTiles.Tasks.SkillQueueTask";

        private const uint FRESHNESSTIME = 15;
        #endregion

        #region CTOR
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationData.Current.DataChanged += new TypedEventHandler<ApplicationData, object>(DataChangedHandler);

        }
        #endregion

        #region Methods
        /// 
        /// </summary>
        private async Task UpdateProgressUI(bool active)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                progressRing.IsIndeterminate = active;
            });
        }
        #endregion

        #region Events
        async void OnProgress(BackgroundTaskRegistration sender, BackgroundTaskProgressEventArgs args)
        {
            await UpdateProgressUI(true);
        }

        async void OnCompleted(BackgroundTaskRegistration sender, BackgroundTaskCompletedEventArgs args)
        {
            await UpdateProgressUI(false);
        }

        void DataChangedHandler(Windows.Storage.ApplicationData appData, object o)
        {
            (DataContext as MainViewModel).InitialLoadCommand.Execute(null);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.New)
            {                   
                (DataContext as MainViewModel).UpdateCommand.Execute(null);

                if (!BackgroundTaskManager.BackgroundTaskExists(NOTIFICATION_ID))
                {
                    IBackgroundTaskRegistration task = BackgroundTaskManager.RegisterBackgroundTask(NOTIFICATION_ENTRY, NOTIFICATION_ID, new MaintenanceTrigger(FRESHNESSTIME, false), new SystemCondition(SystemConditionType.InternetAvailable));
                }

                if (!BackgroundTaskManager.BackgroundTaskExists(MAINTENANCE_ID))
                {
                    IBackgroundTaskRegistration task = BackgroundTaskManager.RegisterBackgroundTask(MAINTENANCE_ENTRY, MAINTENANCE_ID, new MaintenanceTrigger(FRESHNESSTIME, false), new SystemCondition(SystemConditionType.InternetAvailable));
                }

                if (!BackgroundTaskManager.BackgroundTaskExists(SKILLQUEUE_ID))
                {
                    IBackgroundTaskRegistration task = BackgroundTaskManager.RegisterBackgroundTask(SKILLQUEUE_ENTRY, SKILLQUEUE_ID, new MaintenanceTrigger(FRESHNESSTIME, false), new SystemCondition(SystemConditionType.InternetAvailable));
                }

                foreach (var task in BackgroundTaskRegistration.AllTasks)
                {
                    task.Value.Progress += new BackgroundTaskProgressEventHandler(OnProgress);
                    task.Value.Completed += new BackgroundTaskCompletedEventHandler(OnCompleted);
                }

                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 3, 0);
                timer.Tick += ServerStatusTimer_Tick;

                timer.Start();

                ServerStatusTimer_Tick(null, null);
            }
        }

        void ServerStatusTimer_Tick(object sender, object e)
        {
            DependencyFactory.Resolve<MainViewModel>().GetServerStatusCommand.Execute(null);
        }

        /// <summary>
        /// Invoked when an item is clicked.
        /// </summary>
        /// <param name="sender">The GridView (or ListView when the application is snapped)
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Navigate to the appropriate destination page, configuring the new page
            // by passing required information as a navigation parameter
            this.Frame.Navigate(typeof(CharacterDetailPage), e.ClickedItem);
        }
        #endregion
    }
}



//1. Update to Retribution_1.1_84566
//2. manual sync also updates skills from CCP
//Added color coding in the skill selection page for planning
//Added group filter to current skills
//Added group and miscellaneous filters to skill planning skill selection page
//Added search to items and ship selection 

