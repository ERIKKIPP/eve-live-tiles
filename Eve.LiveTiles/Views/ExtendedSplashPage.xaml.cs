﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Eve.LiveTiles
{
    /// <summary>
    /// loads the skilltree and accounts
    /// </summary>
    public sealed partial class ExtendedSplashPage : Page
    {
        internal Rect _splashImageRect; // Rect to store splash screen image coordinates.
        internal Frame _rootFrame;
        private SplashScreen _splash; // Variable to hold the splash screen object.

        public ExtendedSplashPage(SplashScreen splashscreen)
        {
            this.InitializeComponent();

            // Listen for window resize events to reposition the extended splash screen image accordingly.
            // This is important to ensure that the extended splash screen is formatted properly in response to snapping, unsnapping, rotation, etc...
            Window.Current.SizeChanged += new WindowSizeChangedEventHandler(ExtendedSplashPage_OnResize);

            _splash = splashscreen;

            if (_splash != null)
            {
                // Retrieve the window coordinates of the splash screen image.
                _splashImageRect = _splash.ImageLocation;
                PositionImage();
            }

            // Create a Frame to act as the navigation context 
            _rootFrame = new Frame();

            Messenger.Default.Register<MessageType>(this, (s) =>
            {
                switch (s)
                {
                    case MessageType.FinishedInitalLoad:
                        // Navigate to mainpage    
                        _rootFrame.Navigate(typeof(MainPage));

                        // Place the frame in the current Window 
                        Window.Current.Content = _rootFrame;
                        break;
                    default:
                        break;
                }

            });
            
            (DataContext as MainViewModel).InitialLoadCommand.Execute(null);
        }

        private void PositionImage()
        {
            SplashImage.SetValue(Canvas.LeftProperty, _splashImageRect.X);
            SplashImage.SetValue(Canvas.TopProperty, _splashImageRect.Y - 10);
            SplashImage.Height = _splashImageRect.Height;
            SplashImage.Width = _splashImageRect.Width;
        }

        private void ExtendedSplashPage_OnResize(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            // Safely update the extended splash screen image coordinates. This function will be fired in response to snapping, unsnapping, rotation, etc...
            if (_splash != null)
            {
                // Update the coordinates of the splash screen image.
                _splashImageRect = _splash.ImageLocation;
                PositionImage();
            }
        }

    }
}
