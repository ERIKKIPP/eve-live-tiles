﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Eve.LiveTiles
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddPlanDialog : Page
    {
        public event EventHandler CancelRequested;
        public event EventHandler UpdateRequested;

        public AddPlanDialog()
        {
            this.InitializeComponent();
        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {
            if (CancelRequested != null)
            {
                CancelRequested(this, EventArgs.Empty);
            }
        }

        private void SaveClicked(object sender, RoutedEventArgs e)
        {
            if (UpdateRequested != null)
            {
                UpdateRequested(this, new GenericEventArgs<string>() { Data = PlanNameTextBlock.Text });
            }
        }
    }
}
