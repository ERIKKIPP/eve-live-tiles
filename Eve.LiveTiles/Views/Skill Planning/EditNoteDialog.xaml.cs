﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Eve.LiveTiles
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditNoteDialog : Page
    {
        public event EventHandler CancelRequested;
        public event EventHandler UpdateRequested;

        public EditNoteDialog()
            : this(null)
        {
            this.InitializeComponent();
        }

        public EditNoteDialog(string note)
        {
            this.InitializeComponent();

            if (!string.IsNullOrEmpty(note))
            {
                EditNoteTextBlock.Text = note;
            }
        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {
            if (CancelRequested != null)
            {
                CancelRequested(this, EventArgs.Empty);
            }
        }

        private void SaveClicked(object sender, RoutedEventArgs e)
        {
            if (UpdateRequested != null)
            {
                UpdateRequested(this, new GenericEventArgs<string>() { Data = EditNoteTextBlock.Text });
            }
        }
    }
}
