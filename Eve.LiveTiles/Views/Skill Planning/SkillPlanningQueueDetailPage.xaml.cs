﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Eve.LiveTiles
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class SkillPlanningQueueDetailPage : LayoutAwarePage
    {
        public SkillPlanningQueueDetailPage()
        {
            this.InitializeComponent();

            FullGrid.AddHandler(Control.DropEvent, new DragEventHandler(OnDropEvent), true);
            SnappedGrid.AddHandler(Control.DropEvent, new DragEventHandler(SnappedOnDropEvent), true);
        }

        private async void OnDropEvent(object sender, DragEventArgs e)
        {
            await (DataContext as SkillPlannerViewModel).Reordered();
        }

        private async void SnappedOnDropEvent(object sender, DragEventArgs e)
        {
            await (DataContext as SkillPlannerViewModel).Reordered();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            (DataContext as SkillPlannerViewModel).SelectedSkillPlan = e.Parameter as SkillPlan;       
        }

    }
}
