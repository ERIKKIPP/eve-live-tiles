﻿using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Eve.LiveTiles
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class SkillPlanningSkillsDetailPage : LayoutAwarePage
    {
        public SkillPlanningSkillsDetailPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SkillModel model = (e.Parameter as SkillModel);

            var query = from grp in EveDataRepository.SkillTree
                        where grp.Skills.Any(c => c.TypeID == model.TypeID)
                        select (grp.Skills.Where(s => s.TypeID == model.TypeID)).FirstOrDefault();

            SkillQueued sq = new SkillQueued
            {
                Skill = query.FirstOrDefault()
            };

            (DataContext as SkillPlannerViewModel).SelectedSkill = sq;
            SnapperSlider.Tag = model.Level;
            SnapperSlider.Value = model.Level;
            LevelSlider.Tag = model.Level;
            LevelSlider.Value = model.Level;

        }

        private void LevelSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            Slider slider = sender as Slider;

            if (slider != null && slider.Tag != null)
            {
                int level = (int)slider.Tag;

                if (e.NewValue < level)
                {
                    slider.Value = level;
                }

                SkillQueued oldcopy = (DataContext as SkillPlannerViewModel).SelectedSkill;

                SkillQueued newcopy = new SkillQueued
                {
                    FromLevel = level,
                    ToLevel = (int)slider.Value,
                    Note = oldcopy.Note,
                    Skill = oldcopy.Skill
                };

                (DataContext as SkillPlannerViewModel).SelectedSkill = newcopy;
            }
        }

    }
}
