﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class AllowedUsedModel<T>
    {
        public AllowedUsedModel()
        {

        }

        public T Allowed { get; set; }

        public T Used { get; set; }

        public override string ToString()
        {
            if (Allowed != null)
            {
                return string.Format(CultureInfo.CurrentUICulture, "{0}/{1}", Used, Allowed);
            }

            return null;
        }
    }
}
