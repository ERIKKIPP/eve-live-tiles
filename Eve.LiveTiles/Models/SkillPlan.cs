﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Eve.LiveTiles
{
    [DataContract]
    internal sealed class SkillPlan : IEnumerable<SkillQueued>
    {
        private ObservableCollection<SkillQueued> _skillQueuedList;
      
        public SkillPlan()
        {
            _skillQueuedList = new ObservableCollection<SkillQueued>();
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ObservableCollection<SkillQueued> SkillQueuedList 
        {
            get { return _skillQueuedList; }
            set
            {
                if (_skillQueuedList == null)
                {
                    _skillQueuedList = new ObservableCollection<SkillQueued>();
                }

                foreach (var item in value)
                {
                    _skillQueuedList.Add(item);
                }
            }
        
        }

        public IEnumerator<SkillQueued> GetEnumerator()
        {
            return (this.SkillQueuedList ?? Enumerable.Empty<SkillQueued>()).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this.SkillQueuedList ?? Enumerable.Empty<SkillQueued>()).GetEnumerator();
        }
    }
}
