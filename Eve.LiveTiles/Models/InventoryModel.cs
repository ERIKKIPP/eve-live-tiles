﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class InventoryModel
    {
        public InventoryModel()
        {

        }       

        public InvType InventoryItem { get; set; }

        public IEnumerable<IGrouping<string, TypeAttributesModel>> TypeAttributes { get; set; }

        public IEnumerable<RequiredSkillModel> RequiredSkills { get; set; }

        public IEnumerable<CertificateModel> RequiredCerts { get; set; }
    }
}
