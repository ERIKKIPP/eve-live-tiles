﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Eve.LiveTiles.Data;
using Eve.LiveTiles.Extensions;
using System.Threading.Tasks;
using Eve.LiveTiles.DataModel;

namespace Eve.LiveTiles
{
    [DataContract]
    internal sealed class SkillQueued : ObservableBase
    {
        private string _note;

        public SkillQueued()
        {

        }

        [DataMember]
        public string Note { get { return _note; } set { SetProperty(ref _note, value); } }

        [DataMember]
        public int FromLevel { get; set; }

        [DataMember]
        public int ToLevel { get; set; }

        [DataMember]
        public Skill Skill { get; set; }

        /// <summary>
        /// Skillpoints At Specific Level For Specific Rank
        /// gets the needed skill points for a particular level
        /// used to determine the amount of time to train
        /// </summary>
        /// <returns></returns>
        public int SkillPointsAtLevel()
        {
            return SkillPointsAtLevel(ToLevel);
        }

        /// <summary>
        /// used to determine skillpoints needed at various levels
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public int SkillPointsAtLevel(int level)
        {
            switch (level)
            {
                case -1:
                case 0:
                    return 0;
                case 1:
                    return 250 * Skill.Rank;
                case 2:
                    switch (Skill.Rank)
                    {
                        case 1:
                            return 1415;
                        default:
                            return (int)(Skill.Rank * 1414.3f + 0.5f);
                    }
                case 3:
                    return 8000 * Skill.Rank;
                case 4:
                    return Convert.ToInt32(Math.Ceiling(Math.Pow(2, (2.5 * level) - 2.5) * 250 * Skill.Rank));
                case 5:
                    return 256000 * Skill.Rank;
                default:
                    return 0;
            }
        }

        /// <summary>
        ///  calculates the skill points needed per hour
        /// </summary>
        /// <returns></returns>
        public float GetSPPerHour(EveCharacter character)
        {
            return GetSPPerMinute(character) * 60.0f;
        }

        /// <summary>
        /// calculates the skill points needed per minute
        /// </summary>
        /// <returns></returns>
        public float GetSPPerMinute(EveCharacter character)
        {
            float primaryAttribute = character.GetEffectiveAttribute(Skill.PrimaryAttribute);
            float secondaryAttribute = character.GetEffectiveAttribute(Skill.SecondaryAttribute);

            return primaryAttribute + (secondaryAttribute / 2);
        }

        /// <summary>
        /// calculates the current skillpoints for the queued skill
        /// </summary>
        /// <returns></returns>
        public int GetCurrentSkillpoints(EveCharacter character)
        {
            if (character.CharacterSheet != null && character.CharacterSheet.Skills != null)
            {
                CharacterSkill charSkill = character.CharacterSheet.Skills.Where(s => s.TypeID == Skill.TypeID).FirstOrDefault();

                if (charSkill == null)
                {
                    return 0;
                }

                if (character.SkillInTraining.TrainingTypeID == Skill.TypeID)
                {
                    return (int)(SkillPointsAtLevel() - (character.SkillInTraining.TrainingEndTime.Subtract(DateTime.Now)).TotalHours * GetSPPerHour(character));
                }
                else
                {
                    return charSkill.Skillpoints;
                }
            }

            return 0;
        }

        public int GetCurrentLevel(EveCharacter character)
        {
            if (character.CharacterSheet != null && character.CharacterSheet.Skills != null)
            {
                CharacterSkill charSkill = character.CharacterSheet.Skills.Where(s => s.TypeID == Skill.TypeID).FirstOrDefault();

                if (charSkill == null)
                {
                    return 0;
                }

                return charSkill.Level;
            }

            return 0;
        }

        public float GetPercentComplete(EveCharacter character)
        {
            int currentLevel = GetCurrentLevel(character);
            int nextLevelSp = SkillPointsAtLevel(currentLevel + 1);
            int levelSp = SkillPointsAtLevel(currentLevel);
            int currentSp = GetCurrentSkillpoints(character);

            float fraction = (float)(currentSp - levelSp) / (float)(nextLevelSp - levelSp);

            if (fraction <= 0)
            {
                return 0;
            }

            return (fraction <= 1 ? fraction : fraction % 1);
        }

        /// <summary>
        /// returns TimeSpanText
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public string GetTrainingTime(EveCharacter character)
        {
            float skillpointsPerHour = GetSPPerHour(character);
            int skillpointsAtLevel = SkillPointsAtLevel();
            int currentSp = GetCurrentSkillpoints(character);

            if (skillpointsPerHour > 0)
            {
                return TimeSpan.FromHours((skillpointsAtLevel - currentSp) / skillpointsPerHour).TimeSpanText();
            }

            return TimeSpan.FromHours(0).TimeSpanText();
        }

        /// <summary>
        /// returns TimeSpan
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public TimeSpan GetTrainingTimeSpan(EveCharacter character)
        {
            return GetTrainingTimeSpan(character, ToLevel);
        }

        public TimeSpan GetTrainingTimeSpan(EveCharacter character, int level)
        {
            float skillpointsPerHour = GetSPPerHour(character);
            int skillpointsAtLevel = SkillPointsAtLevel(level);
            int currentSp = GetCurrentSkillpoints(character);

            if (skillpointsPerHour > 0)
            {
                return TimeSpan.FromHours((skillpointsAtLevel - currentSp) / skillpointsPerHour);
            }

            return TimeSpan.FromHours(0);
        }

        public TimeSpan GetRemainingTrainingTimeSpan(EveCharacter character, int level)
        {
            float skillpointsPerHour = GetSPPerHour(character);
            int skillpointsAtLevel = SkillPointsAtLevel(level);
            int currentSp = GetCurrentSkillpoints(character);

            int startSP = skillpointsAtLevel - Math.Max(currentSp, SkillPointsAtLevel(level - 1));

            return TimeSpan.FromHours(startSP / skillpointsPerHour);
        }

        public bool IsCompleted(EveCharacter character)
        {
            if (character.CharacterSheet != null && character.CharacterSheet.Skills != null)
            {
                CharacterSkill charSkill = character.CharacterSheet.Skills.Where(s => s.TypeID == Skill.TypeID).FirstOrDefault();

                if (charSkill != null)
                {
                    return charSkill.Level >= ToLevel;
                }
            }

            return false;
        }
    }
}
