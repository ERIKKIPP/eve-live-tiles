﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class OptimizedAttributesModel
    {
        public OptimizedAttributesModel()
        {

        }

        public int Intelligence { get; set; }

        public int Memory { get; set; }

        public int Charisma { get; set; }

        public int Perception { get; set; }

        public int Willpower { get; set; }

        public TimeSpan TrainingTime { get; set; }
    }
}
