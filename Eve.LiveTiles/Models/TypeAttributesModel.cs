﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class TypeAttributesModel
    {
        public TypeAttributesModel()
        {

        }

        public InvType InventoryType { get; set; }

        public DgmTypeAttribute TypeAttribute { get; set; }

        public DgmAttributeCategory AttributeCategory { get; set; }

        public DgmAttributeType AttributeType { get; set; }

        public CrtRecommendation Recommendation { get; set; }

        public EveUnit Unit { get; set; }
    }
}
