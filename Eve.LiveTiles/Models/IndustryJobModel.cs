﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class IndustryJobModel
    {
        public IndustryJobModel()
        {

        }

        public string Station { get; set; }

        public string TypeName { get; set; }

        public string Activity { get; set; }

        public string Status { get; set; }

        public bool Completed { get; set; }

        public DateTime InstallDate { get; set; }

        public DateTime EndDate { get; set; }

        public int ActivityID { get; set; }

        public IndustryCompletedStatusType CompletedStatus { get; set; }

    }
}
