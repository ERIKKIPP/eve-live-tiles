﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class SkillGroupModel : IEnumerable<SkillModel>
    {

        public SkillGroupModel()
        {
            Skills = new List<SkillModel>();
        }

        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public IList<SkillModel> Skills { get; set; }

        public IEnumerator<SkillModel> GetEnumerator()
        {
            return (this.Skills ?? Enumerable.Empty<SkillModel>()).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this.Skills ?? Enumerable.Empty<SkillModel>()).GetEnumerator();
        }
    }
}
