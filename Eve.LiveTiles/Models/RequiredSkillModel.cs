﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class RequiredSkillModel
    {
        public RequiredSkillModel()
        {

        }

        public string TypeName { get; set; }

        public int SkillLevel { get; set; }

        public int TypeID { get; set; }

        public InventoryGroupType InventoryGroup { get; set; }
    }
}
