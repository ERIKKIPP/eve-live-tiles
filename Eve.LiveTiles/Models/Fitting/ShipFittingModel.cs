﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    /// <summary>
    /// What will be persisted to disk
    /// </summary>
    internal sealed class ShipFittingModel
    {
        #region Private Members

        #endregion

        #region CTOR
        public ShipFittingModel()
        {

        }
        #endregion

        #region Methods
        public T GetAffectValue<T>(IEnumerable<SkillAffect> query, double attValue)
        {
            foreach (var item in query)
            {
                switch (item.UnitName)
                {
                    case UnitType.None:
                    case UnitType.Bonus:
                        attValue += (item.Level * item.ValueFloat);
                        break;
                    case UnitType.Length:
                        break;
                    case UnitType.Mass:
                        break;
                    case UnitType.Time:
                        break;
                    case UnitType.ElectricCurrent:
                        break;
                    case UnitType.Temperature:
                        break;
                    case UnitType.AmountOfSubstance:
                        break;
                    case UnitType.LuminousIntensity:
                        break;
                    case UnitType.Area:
                        break;
                    case UnitType.Volume:
                        break;
                    case UnitType.Speed:
                        break;
                    case UnitType.Acceleration:
                        break;
                    case UnitType.WaveNumber:
                        break;
                    case UnitType.MassDensity:
                        break;
                    case UnitType.SpecificVolume:
                        break;
                    case UnitType.CurrentDensity:
                        break;
                    case UnitType.MagneticFieldStrength:
                        break;
                    case UnitType.AmountOfSubstanceConcentration:
                        break;
                    case UnitType.Luminance:
                        break;
                    case UnitType.MassFraction:
                        break;
                    case UnitType.Milliseconds:
                        break;
                    case UnitType.Millimeters:
                        break;
                    case UnitType.MegaPascals:
                        break;
                    case UnitType.Multiplier:
                        break;
                    case UnitType.RealPercent:
                    case UnitType.Percentage:
                        attValue = attValue * (1 + ((item.Level * item.ValueFloat) / 100));
                        break;
                    case UnitType.Teraflops:
                        break;
                    case UnitType.MegaWatts:
                        break;
                    case UnitType.InverseAbsolutePercent:
                        break;
                    case UnitType.ModifierPercent:
                        break;
                    case UnitType.InversedModifierPercent:
                        break;
                    case UnitType.RadiansSecond:
                        break;
                    case UnitType.Hitpoints:
                        break;
                    case UnitType.CapacitorUnits:
                        break;
                    case UnitType.Sizeclass:
                        break;
                    case UnitType.Oreunits:
                        break;
                    case UnitType.AttributePoints:
                        break;
                    case UnitType.Fittingslots:
                        break;
                    case UnitType.TrueTime:
                        break;
                    case UnitType.ModifierRelativePercent:
                        break;
                    case UnitType.Newton:
                        break;
                    case UnitType.LightYear:
                        break;
                    case UnitType.AbsolutePercent:
                        break;
                    case UnitType.Dronebandwidth:
                        break;
                    case UnitType.Hours:
                        break;
                    case UnitType.Money:
                        break;
                    case UnitType.LogisticalCapacity:
                        break;
                    case UnitType.AstronomicalUnit:
                        break;
                    case UnitType.Slot:
                        break;
                    case UnitType.Boolean:
                        break;
                    case UnitType.Units:
                        break;
                    case UnitType.Level:
                        break;
                    case UnitType.Hardpoints:
                        break;
                    default:
                        break;
                }
            }

            return (T)Convert.ChangeType(attValue, typeof(T));
        }

        public T GetAffectValue<T>(IEnumerable<IGrouping<int, SkillAffect>> affects, AttributeIdType attType, InventoryGroupType invGroup, double attValue)
        {
            var affect = from aff in affects
                         from aff1 in aff
                         where aff1.AttributeID == (int)attType && aff1.InventoryGroup == (int)invGroup
                         select aff1;

            return GetAffectValue<T>(affect, attValue);           
        }

        public T GetAffectValueRequiredOnly<T>(IEnumerable<IGrouping<int, SkillAffect>> affects, AttributeIdType attType, InventoryGroupType invGroup, double attValue)
        {
            var affect = from aff in affects
                         from aff1 in aff
                         from reqSkill in RequiredSkills
                         where aff1.AttributeID == (int)attType && aff1.InventoryGroup == (int)invGroup
                         && aff1.TypeID == reqSkill.TypeID
                         select aff1;

            return GetAffectValue<T>(affect, attValue);     
        }

        public double GetResistValue(AttributeIdType attType)
        {
            double value = GetValue(attType);

            return (1.0 - value) * 100.0;

        }

        public double GetValue(AttributeIdType attType)
        {
            string unit = null;
            return GetValue(attType, out unit);
        }

        public double GetValue(AttributeIdType attType, out string unit)
        {
            var query = (from item in TypeAttributes
                         where item.AttributeType.AttributeID == (int)attType
                         select item).FirstOrDefault();

            unit = null;

            if (query != null)
            {
                if (query.Unit != null)
                {
                    unit = query.Unit.DisplayName;
                }

                return query.TypeAttribute.ValueInt != null ? (double)query.TypeAttribute.ValueInt : query.TypeAttribute.ValueFloat.GetValueOrDefault();
            }

            return 0;
        }

        /// <summary>
        /// defaults to {0:N2}
        /// </summary>
        /// <param name="attType"></param>
        /// <returns></returns>
        public string GetStringValue(AttributeIdType attType)
        {
            return GetStringValue(attType, "{0:N2}");
        }

        public string GetStringValue(AttributeIdType attType, string format)
        {
            string unit = null;
            double value = GetValue(attType, out unit);

            string strvalue = string.Format(CultureInfo.InvariantCulture, format, value);//get value
            return string.Format(CultureInfo.InvariantCulture, "{0} {1}", strvalue, unit);//get unit name

        }
        #endregion

        #region Properties

        /// <summary>
        /// name of the fitting or the TypeName if empty
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of the fitting
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// inventory item of this ship
        /// </summary>
        public InventoryModel Inventory { get; set; }

        /// <summary>
        /// all the attributes of this ship
        /// </summary>
        public IEnumerable<TypeAttributesModel> TypeAttributes { get; set; }

        /// <summary>
        /// all the required skills for this ship
        /// </summary>
        public IEnumerable<RequiredSkillModel> RequiredSkills { get; set; }

        public IList<IVariableGridViewItem> HighSlots { get; set; }

        public IList<IVariableGridViewItem> MediumSlots { get; set; }

        public IList<IVariableGridViewItem> LowSlots { get; set; }

        public IList<IVariableGridViewItem> RigSlots { get; set; }

        public IList<IVariableGridViewItem> SubSlots { get; set; }

        public IList<IVariableGridViewItem> Panel { get; set; }

        public ResourcePanelModel Resources { get; set; }
        #endregion

        #region Events

        #endregion







    }
}
