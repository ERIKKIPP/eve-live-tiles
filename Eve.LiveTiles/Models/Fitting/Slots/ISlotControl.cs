﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal interface ISlotControl
    {
        int Index { get; set; }

        InventoryModel InventoryItem { get; set; }
    }
}
