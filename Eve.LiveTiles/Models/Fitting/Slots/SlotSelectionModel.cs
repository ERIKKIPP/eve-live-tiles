﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class SlotSelectionModel
    {
        public SlotSelectionModel()
        {

        }
        
        public string TypeName { get; set; }

        public int TypeID { get; set; }

        public string GroupName { get; set; }

        public string Description { get; set; }

        public int Cpu { get; set; }

        public int Power { get; set; }

    }
}
