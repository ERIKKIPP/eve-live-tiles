﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class DefensePanelModel
    {
        public DefensePanelModel()
        {

        }

        public double ShieldEm { get; set; }

        public double ShieldExplosive { get; set; }

        public double ShieldKinetic { get; set; }

        public double ShieldThermal { get; set; }

        public double ArmorEm { get; set; }

        public double ArmorExplosive { get; set; }

        public double ArmorKinetic { get; set; }

        public double ArmorThermal { get; set; }

        public double StructureEm { get; set; }

        public double StructureExplosive { get; set; }

        public double StructureKinetic { get; set; }

        public double StructureThermal { get; set; }

        public string ShieldCapacity { get; set; }

        public string ArmorHitpoints { get; set; }

        public string StructureHitpoints { get; set; }

        public string EffectiveHp { get; set; }

        public string EveEffectiveHp { get; set; }

        public string ShieldTooltip { get; set; }

        public string ArmorTooltip { get; set; }

        public string StructureTooltip { get; set; }
    }
}
