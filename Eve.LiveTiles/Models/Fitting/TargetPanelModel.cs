﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    public sealed class TargetPanelModel
    {
        public TargetPanelModel()
        {

        }

        public string SensorStrength { get; set; }

        public string SensorStrengthTooltip { get; set; }

        public string ScanResolution { get; set; }

        public string ScanResolutionTooltip { get; set; }

        public string MaxTargetCount { get; set; }

        public string SignatureRadius { get; set; }

        public string MaxTargetRange { get; set; }
    }
}
