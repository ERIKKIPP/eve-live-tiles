﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Eve.LiveTiles
{
    internal sealed class CapacitorPanelModel
    {
        public CapacitorPanelModel()
        {

        }

        public string Capacity { get; set; }

        public string RechargeRate { get; set; }

        public string PeakRecharge { get; set; }

        public string StableMessage { get; set; }

        public bool IsCapStable { get; set; }

    }
}
