﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class NavigationPanelModel
    {
        public NavigationPanelModel()
        {

        }

        public string InertiaModifier { get; set; }

        public string ShipwarpSpeed { get; set; }

        public string Mass { get; set; }

        public string MaxVelocity { get; set; }

        public string AlignTime { get; set; }
    }
}
