﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class OffensePanelModel
    {
        public OffensePanelModel()
        {

        }

        public double WeaponDps { get; set; }

        public double Volley { get; set; }

        public double TotalDps { get; set; }

        public double DroneDps { get; set; }
    }
}
