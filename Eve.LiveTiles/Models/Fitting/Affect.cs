﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal abstract class Affect
    {
        public Affect()
        {

        }

        /// <summary>
        /// id of the type
        /// </summary>
        public int TypeID { get; set; }

        /// <summary>
        /// id of the attribute
        /// </summary>
        public int AttributeID { get; set; }

        /// <summary>
        /// name of the type
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// description of the type
        /// </summary>
        public string TypeDescription { get; set; }

        /// <summary>
        /// value from the attribute
        /// </summary>
        public double ValueFloat { get; set; }

        /// <summary>
        /// unit type
        /// </summary>
        public UnitType UnitName { get; set; }

        /// <summary>
        /// inventory group
        /// parent item the inventory item is in
        /// </summary>
        public int InventoryGroup { get; set; }
    }
}
