﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class ResourcePanelModel
    {
        public ResourcePanelModel()
        {
            Launchers = new AllowedUsedModel<int?>();
            Turrets = new AllowedUsedModel<int?>();
            DroneBandwidth = new AllowedUsedModel<double?>();
            DroneCargoCapacity = new AllowedUsedModel<double?>();
            CPU = new AllowedUsedModel<double?>();
            Powergrid = new AllowedUsedModel<double?>();
            Calibration = new AllowedUsedModel<double?>();
            Drones = new AllowedUsedModel<int?>();
        }

        public AllowedUsedModel<int?> Launchers { get; set; }

        public AllowedUsedModel<int?> Turrets { get; set; }

        public AllowedUsedModel<double?> DroneBandwidth { get; set; }

        public AllowedUsedModel<double?> DroneCargoCapacity { get; set; }

        public AllowedUsedModel<double?> CPU { get; set; }

        public AllowedUsedModel<double?> Powergrid { get; set; }

        public AllowedUsedModel<double?> Calibration { get; set; }

        public AllowedUsedModel<int?> Drones { get; set; }

        public string CargoBay { get; set; }
    }
}
