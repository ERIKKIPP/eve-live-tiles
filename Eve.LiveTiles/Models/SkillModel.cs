﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class SkillModel
    {
        public int Level { get; set; }

        public string TypeName { get; set; }

        public string Description { get; set; }

        public int TypeID { get; set; }

        public string GroupName { get; set; }

        public AttributeType PrimaryAttribute { get; set; }

        public AttributeType SecondaryAttribute { get; set; }

        public bool InQueue { get; set; }
    }
}
