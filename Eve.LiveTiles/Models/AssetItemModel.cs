﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class AssetItemModel
    {
        public AssetItemModel()
        {

        }

        public AssetItemModel(AssetItem item, IList<InvType> types, IList<InvFlag> flags, IList<StaStation> stations, IList<ConquerableStation> conquerableStations)
        {
            var name = types.Where(s => s.TypeID == item.TypeID).FirstOrDefault();

            if (name != null)
            {
                TypeName = name.TypeName;
            }

            var flag = flags.Where(s => s.FlagID == item.Flag).FirstOrDefault();

            if (flag != null)
            {
                Flag = flag.FlagName;
            }

            Quantity = item.Quantity;

            if (item.LocationID > 0)
            {
                var station = stations.Where(s => s.StationID == item.LocationID).FirstOrDefault();

                if (station != null)
                {
                    Location = station.StationName;
                }
                else//try conquerable stations
                {
                    var conquer = conquerableStations.Where(s => s.StationID == item.LocationID).FirstOrDefault();
                    if (conquer != null)
                    {
                        Location = conquer.StationName;
                    }
                }
            }
        }

        /// <summary>
        /// Reference to a solar system or a station. Not used for items within items.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// The type of item. This references to the invTypes table.
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// How many of this item is in the stack.
        /// </summary>
        public long Quantity { get; set; }

        /// <summary>
        /// Used to differentiate between hanger divisions, drone bays, fitting locations and similiar.
        /// </summary>
        public string Flag { get; set; }

        /// <summary>
        /// where this item resides
        /// </summary>
        public string Container { get; set; }
    }
}
