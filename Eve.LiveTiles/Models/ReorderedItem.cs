﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class ReorderedItem<T>
    {
        public ReorderedItem()
        {

        }

        public T Item { get; set; }

        public int OriginalIndex { get; set; }

        public int NewIndex { get; set; }
    }
}
