﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class AttributeFilterModel 
    {
        public AttributeFilterModel()
        {

        }

        public AttributeType PrimaryAttribute { get; set; }

        public AttributeType SecondaryAttribute { get; set; }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}-{1}", PrimaryAttribute, SecondaryAttribute);
        }

        public override bool Equals(object obj)
        {
            AttributeFilterModel other = obj as AttributeFilterModel;
            return other != null && PrimaryAttribute.Equals(other.PrimaryAttribute) && SecondaryAttribute.Equals(other.SecondaryAttribute);
        }

        public override int GetHashCode()
        {
            return PrimaryAttribute.GetHashCode() + SecondaryAttribute.GetHashCode();
        } 
    }
}
