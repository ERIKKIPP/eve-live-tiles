﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class MarketOrderModel
    {
        public MarketOrderModel()
        {

        }

        /// <summary>
        /// ID of the station the order was placed in.
        /// </summary>
        public string Station { get; set; }

        /// <summary>
        /// Quantity of items required/offered to begin with.
        /// Quantity of items still for sale or still desired.
        /// </summary>
        public string VolumeText { get; set; }
                
        public string OrderStateText { get; set; }

        /// <summary>
        /// ID of the type (references the invTypes table) of the items this order is buying/selling.
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// The range this order is good for. 
        /// For sell orders, this is always 32767. 
        /// For buy orders, allowed values are: -1 = station, 0 = solar system, 
        /// 5/10/20/40 Jumps, 32767 = region.
        /// </summary>
        public int Range { get; set; }

        public string Expiration { get; set; }
        
        /// <summary>
        /// The cost per unit for this order.
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// When this order was issued.
        /// </summary>
        public DateTime Issued { get; set; }

    }
}
