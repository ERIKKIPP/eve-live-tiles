﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class CertificateModel
    {
        public CertificateModel()
        {

        }

        public int CertificateID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public CertGradeType Grade { get; set; }

        public string GroupName { get; set; }
    }
}
