﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class InvCategoryModel
    {
        public InvCategoryModel()
        {
            Inventory = new List<InvType>();
            Children = Enumerable.Empty<InvCategoryModel>();
        }

        public int? MarketGroupID { get; set; }

        public int? ParentGroupID { get; set; }

        public string MarketGroupName { get; set; }

        public string Description { get; set; }

        public bool? HasTypes { get; set; }

        public bool IsFilter { get; set; }

        /// <summary>
        /// when populated from inventory I need this to display the data when selected
        /// </summary>
        public IEnumerable<InvType> Inventory { get; set; }

        public InvType Item { get; set; }

        public IEnumerable<InvCategoryModel> Children { get; set; }
    }
}
