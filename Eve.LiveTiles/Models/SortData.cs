﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class SortData
    {
       public SortData()
       {

       }

       /// <summary>
       /// set to next sort type
       /// </summary>
       public SortType SortType { get; set; }

       public string SortColumn { get; set; }
    }
}
