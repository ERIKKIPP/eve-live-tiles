﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eve.LiveTiles.Extensions;
using Eve.LiveTiles.Service;
using Eve.LiveTiles.Data;
using System.Globalization;
using GalaSoft.MvvmLight.Command;
using Windows.UI.Core;
using Windows.System.Threading;

namespace Eve.LiveTiles
{
    internal sealed class NeuralRemapModel : ObservableBase
    {
        #region Private Members
        private SkillPlan _selectedPlan;
        private EveCharacter _dummycharacter;
        private EveCharacter _character;
        private TimeSpan _currentTraining;
        private const int BASEPOINTS = 17;
        private const int MAPPABLEPOOL = 14;
        private const int MAXREMAPPERATTRIBUTE = 10;
        private int _intelligenceBase;
        private int _memoryBase;
        private int _charismaBase;
        private int _perceptionBase;
        private int _willpowerBase;
        private int _intelligenceMappable;
        private int _memoryMappable;
        private int _charismaMappable;
        private int _perceptionMappable;
        private int _willpowerMappable;
        private int _mappablePool;
        private int _intelligenceImplant;
        private int _memoryImplant;
        private int _charismaImplant;
        private int _perceptionImplant;
        private int _willpowerImplant;
        #endregion

        #region CTOR
        public NeuralRemapModel(SkillPlannerViewModel model)
        {
            Initialize(model);

            OptimizeCommand = new RelayCommand(() => OptimizeAction());
        }

        #endregion

        #region Methods
        private void Initialize(SkillPlannerViewModel model)
        {
            _mappablePool = MAPPABLEPOOL;//needs to be set first
            _character = model.Character;

            if (_character.CharacterSheet != null)
            {
                foreach (var item in _character.CharacterSheet.Attributes)
                {
                    switch (item.Attribute)
                    {
                        case AttributeType.None:
                            break;
                        case AttributeType.Intelligence:
                            IntelligenceBase = item.Value;
                            break;
                        case AttributeType.Memory:
                            MemoryBase = item.Value;
                            break;
                        case AttributeType.Charisma:
                            CharismaBase = item.Value;
                            break;
                        case AttributeType.Perception:
                            PerceptionBase = item.Value;
                            break;
                        case AttributeType.Willpower:
                            WillpowerBase = item.Value;
                            break;
                        default:
                            break;
                    }
                }

                foreach (var item in _character.CharacterSheet.AttributeEnhancers)
                {
                    switch (item.Enhancer)
                    {
                        case AttributeType.None:
                            break;
                        case AttributeType.Intelligence:
                            IntelligenceImplant = item.AugmentatorValue;
                            break;
                        case AttributeType.Memory:
                            MemoryImplant = item.AugmentatorValue;
                            break;
                        case AttributeType.Charisma:
                            CharismaImplant = item.AugmentatorValue;
                            break;
                        case AttributeType.Perception:
                            PerceptionImplant = item.AugmentatorValue;
                            break;
                        case AttributeType.Willpower:
                            WillpowerImplant = item.AugmentatorValue;
                            break;
                        default:
                            break;
                    }
                }
            }

            _selectedPlan = model.SelectedSkillPlan;
            _currentTraining = GetPlanTime(_selectedPlan.SkillQueuedList, _character);

            _dummycharacter = new EveCharacter();
            _dummycharacter.CharacterSheet = new CharacterSheetResponse();
            _dummycharacter.CharacterSheet.AttributeEnhancers = new List<AttributeEnhancer>();
            _dummycharacter.CharacterSheet.Attributes = new List<CharacterAttribute>();
            _dummycharacter.CharacterSheet.Skills = _character.CharacterSheet != null ? _character.CharacterSheet.Skills : null;
            _dummycharacter.SkillInTraining = _character.SkillInTraining;
        }

        /// <summary>
        /// get skill plan training time
        /// </summary>
        /// <param name="list"></param>
        /// <param name="character"></param>
        /// <returns></returns>
        private TimeSpan GetPlanTime(IEnumerable<SkillQueued> list, EveCharacter character)
        {
            TimeSpan ts = TimeSpan.Zero;

            foreach (var item in list)
            {
                ts = ts.Add(item.GetTrainingTimeSpan(character));
            }

            return ts;
        }

        /// <summary>
        /// adds attributes from the current values
        /// </summary>
        /// <returns></returns>
        private EveCharacter GetCurrentCharacter(int charisma, int intelligence, int memory, int perception, int willpower)
        {
            _dummycharacter.CharacterSheet.AttributeEnhancers.Clear();
            _dummycharacter.CharacterSheet.Attributes.Clear();

            AttributeEnhancer enh = new AttributeEnhancer();
            enh.Enhancer = AttributeType.Charisma;
            enh.AugmentatorValue = CharismaImplant;
            _dummycharacter.CharacterSheet.AttributeEnhancers.Add(enh);

            enh = new AttributeEnhancer();
            enh.Enhancer = AttributeType.Intelligence;
            enh.AugmentatorValue = IntelligenceImplant;
            _dummycharacter.CharacterSheet.AttributeEnhancers.Add(enh);

            enh = new AttributeEnhancer();
            enh.Enhancer = AttributeType.Memory;
            enh.AugmentatorValue = MemoryImplant;
            _dummycharacter.CharacterSheet.AttributeEnhancers.Add(enh);

            enh = new AttributeEnhancer();
            enh.Enhancer = AttributeType.Perception;
            enh.AugmentatorValue = PerceptionImplant;
            _dummycharacter.CharacterSheet.AttributeEnhancers.Add(enh);

            enh = new AttributeEnhancer();
            enh.Enhancer = AttributeType.Willpower;
            enh.AugmentatorValue = WillpowerImplant;
            _dummycharacter.CharacterSheet.AttributeEnhancers.Add(enh);

            CharacterAttribute attrib = new CharacterAttribute();
            attrib.Attribute = AttributeType.Charisma;
            attrib.Value = BASEPOINTS + charisma;
            _dummycharacter.CharacterSheet.Attributes.Add(attrib);

            attrib = new CharacterAttribute();
            attrib.Attribute = AttributeType.Intelligence;
            attrib.Value = BASEPOINTS + intelligence;
            _dummycharacter.CharacterSheet.Attributes.Add(attrib);

            attrib = new CharacterAttribute();
            attrib.Attribute = AttributeType.Memory;
            attrib.Value = BASEPOINTS + memory;
            _dummycharacter.CharacterSheet.Attributes.Add(attrib);

            attrib = new CharacterAttribute();
            attrib.Attribute = AttributeType.Perception;
            attrib.Value = BASEPOINTS + perception;
            _dummycharacter.CharacterSheet.Attributes.Add(attrib);

            attrib = new CharacterAttribute();
            attrib.Attribute = AttributeType.Willpower;
            attrib.Value = BASEPOINTS + willpower;
            _dummycharacter.CharacterSheet.Attributes.Add(attrib);

            return _dummycharacter;
        }
        
        private void OptimizeAction()
        {
            Optimize();
        }

        private void Optimize()
        {
            EveCharacter character = GetCurrentCharacter(CharismaMappable, IntelligenceMappable, MemoryMappable, PerceptionMappable, WillpowerMappable);//get temp character
            OptimizedAttributesModel model = new OptimizedAttributesModel
            {
                TrainingTime = GetPlanTime(_selectedPlan.SkillQueuedList, character),//get current plan time
                Charisma = CharismaMappable,
                Willpower = WillpowerMappable,
                Perception = PerceptionMappable,
                Memory = MemoryMappable,
                Intelligence = IntelligenceMappable
            };

            //PER
            for (int per = 0; per <= MAXREMAPPERATTRIBUTE; per++)
            {
                // WIL
                int maxWillpower = MAPPABLEPOOL - per;
                for (int will = 0; will <= maxWillpower && will <= MAXREMAPPERATTRIBUTE; will++)
                {
                    // INT
                    int maxIntelligence = maxWillpower - will;
                    for (int intell = 0; intell <= maxIntelligence && intell <= MAXREMAPPERATTRIBUTE; intell++)
                    {
                        // MEM
                        int maxMemory = maxIntelligence - intell;
                        for (int mem = 0; mem <= maxMemory && mem <= MAXREMAPPERATTRIBUTE; mem++)
                        {
                            // CHA
                            int cha = maxMemory - mem;

                            // Reject invalid combinations
                            if (cha > MAXREMAPPERATTRIBUTE)
                            {
                                continue;
                            }

                            character = GetCurrentCharacter(cha, intell, mem, per, will);//get temp character
                            TimeSpan ts = GetPlanTime(_selectedPlan.SkillQueuedList, character);//get new plan time

                            if (TimeSpan.Compare(ts, model.TrainingTime) == -1)//t1 is shorter than t2.
                            {
                                model.TrainingTime = ts;
                                model.Intelligence = intell;
                                model.Memory = mem;
                                model.Perception = per;
                                model.Willpower = will;
                                model.Charisma = cha;
                            }

                        }

                    }
                }
            }

            CharismaMappable = model.Charisma;
            WillpowerMappable = model.Willpower;
            PerceptionMappable = model.Perception;
            MemoryMappable = model.Memory;
            IntelligenceMappable = model.Intelligence;

            MappablePool = 0;//reset

        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public RelayCommand OptimizeCommand { get; private set; }

        public int IntelligenceBase
        {
            get { return _intelligenceBase; }
            set
            {
                _intelligenceBase = value;
                IntelligenceMappable = value - BASEPOINTS;

                //set the pool
                MappablePool -= IntelligenceMappable;
            }
        }

        public int IntelligenceImplant
        {
            get { return _intelligenceImplant; }
            set
            {
                _intelligenceImplant = value;
                RaisePropertyChanged(() => IntelligenceTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int IntelligenceMappable
        {
            get { return _intelligenceMappable; }
            set
            {
                SetProperty(ref _intelligenceMappable, value);
                RaisePropertyChanged(() => IntelligenceTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int IntelligenceTotal { get { return BASEPOINTS + IntelligenceImplant + IntelligenceMappable; } }

        public int MemoryBase
        {
            get { return _memoryBase; }
            set
            {
                _memoryBase = value; MemoryMappable = value - BASEPOINTS;
                MemoryMappable = value - BASEPOINTS;

                //set the pool
                MappablePool -= MemoryMappable;
            }
        }

        public int MemoryImplant
        {
            get { return _memoryImplant; }
            set
            {
                _memoryImplant = value;
                RaisePropertyChanged(() => MemoryTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int MemoryMappable
        {
            get { return _memoryMappable; }
            set
            {
                SetProperty(ref _memoryMappable, value);
                RaisePropertyChanged(() => MemoryTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int MemoryTotal { get { return BASEPOINTS + MemoryImplant + MemoryMappable; } }

        public int CharismaBase
        {
            get { return _charismaBase; }
            set
            {
                _charismaBase = value;
                CharismaMappable = value - BASEPOINTS;

                //set the pool
                MappablePool -= CharismaMappable;
            }
        }

        public int CharismaImplant
        {
            get { return _charismaImplant; }
            set
            {
                _charismaImplant = value;
                RaisePropertyChanged(() => CharismaTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int CharismaMappable
        {
            get { return _charismaMappable; }
            set
            {
                SetProperty(ref _charismaMappable, value);
                RaisePropertyChanged(() => CharismaTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int CharismaTotal { get { return BASEPOINTS + CharismaImplant + CharismaMappable; } }

        public int PerceptionBase
        {
            get { return _perceptionBase; }
            set
            {
                _perceptionBase = value;
                PerceptionMappable = value - BASEPOINTS;

                //set the pool
                MappablePool -= PerceptionMappable;
            }
        }

        public int PerceptionImplant
        {
            get { return _perceptionImplant; }
            set
            {
                _perceptionImplant = value;
                RaisePropertyChanged(() => PerceptionTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int PerceptionMappable
        {
            get { return _perceptionMappable; }
            set
            {
                SetProperty(ref _perceptionMappable, value);
                RaisePropertyChanged(() => PerceptionTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int PerceptionTotal { get { return BASEPOINTS + PerceptionImplant + PerceptionMappable; } }

        public int WillpowerBase
        {
            get { return _willpowerBase; }
            set
            {
                _willpowerBase = value;
                WillpowerMappable = value - BASEPOINTS;

                //set the pool
                MappablePool -= WillpowerMappable;
            }
        }

        public int WillpowerImplant
        {
            get { return _willpowerImplant; }
            set
            {
                _willpowerImplant = value;
                RaisePropertyChanged(() => WillpowerTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int WillpowerMappable
        {
            get { return _willpowerMappable; }
            set
            {
                SetProperty(ref _willpowerMappable, value);
                RaisePropertyChanged(() => WillpowerTotal);
                RaisePropertyChanged(() => RevisedTime);
            }
        }

        public int WillpowerTotal { get { return BASEPOINTS + WillpowerImplant + WillpowerMappable; } }

        public int BasePoints { get { return BASEPOINTS; } }

        public int MappablePool
        {
            get { return _mappablePool; }
            set { SetProperty(ref _mappablePool, value); }
        }

        public string RevisedTime
        {
            get
            {
                EveCharacter character = GetCurrentCharacter(CharismaMappable, IntelligenceMappable, MemoryMappable, PerceptionMappable, WillpowerMappable);
                TimeSpan ts = GetPlanTime(_selectedPlan.SkillQueuedList, character);

                _currentTraining = GetPlanTime(_selectedPlan.SkillQueuedList, _character);

                TimeSavingTimespan = _currentTraining - ts;
                RaisePropertyChanged(() => TimeSaving);
                RaisePropertyChanged(() => TimeSavingTimespan);
                RaisePropertyChanged(() => CurrentTrainingTime);

                return ts.TimeSpanTextNegative();

            }
        }

        /// <summary>
        /// training needs to be updated since training is based on current time so needs to be in sync with RevisedTime
        /// </summary>
        public string CurrentTrainingTime { get { return _currentTraining.TimeSpanTextNegative(); } }

        public string TimeSaving { get { return TimeSavingTimespan.TimeSpanTextNegative(); } }

        public TimeSpan TimeSavingTimespan { get; private set; }
        #endregion
    }
}
