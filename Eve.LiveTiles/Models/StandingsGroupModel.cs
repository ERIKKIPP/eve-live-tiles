﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal sealed class StandingsGroupModel : IEnumerable<Standings>
    {
        public StandingsGroupModel()
        {

        }

        public string Name { get; set; }

        public IList<Standings> StandingsList { get; set; }

        public IEnumerator<Standings> GetEnumerator()
        {
            return (this.StandingsList ?? Enumerable.Empty<Standings>()).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (this.StandingsList ?? Enumerable.Empty<Standings>()).GetEnumerator();
        }
    }
}
