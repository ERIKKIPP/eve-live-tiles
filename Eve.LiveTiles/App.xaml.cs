﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources.Core;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.ApplicationSettings;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Split App template is documented at http://go.microsoft.com/fwlink/?LinkId=234228

namespace Eve.LiveTiles
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {

        /// <summary>
        /// Initializes the singleton Application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            //this.Suspending += OnSuspending;
            this.Resuming += (sender, e) => App_Resuming(sender, e);
        }

        void App_Resuming(object sender, object e)
        {
            //since the app was in suspended state refresh all chararacters data

        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (args.PreviousExecutionState != ApplicationExecutionState.Running)
            {
                ExtendedSplashPage extendedSplash = new ExtendedSplashPage(args.SplashScreen);
                Window.Current.Content = extendedSplash;
            }

            Window.Current.Activate();
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += OnSettingsCommandsRequested;
        }

        #region Settings Window
        private void OnSettingsCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            UICommandInvokedHandler abouthandler = new UICommandInvokedHandler(AboutCommand);
            UICommandInvokedHandler policyhandler = new UICommandInvokedHandler(PrivacyCommand);

            args.Request.ApplicationCommands.Add(new SettingsCommand("AboutId", ResourceHelper.Instance.GetValue("About"), abouthandler));
            args.Request.ApplicationCommands.Add(new SettingsCommand("PrivacyId", ResourceHelper.Instance.GetValue("PrivacyPolicy"), policyhandler));
        }

        private void AboutCommand(IUICommand command)
        {
            Flyout flyout = new Flyout();
            flyout.ShowFlyout(new AboutControl());
        }

        private void PrivacyCommand(IUICommand command)
        {
            Flyout flyout = new Flyout();
            flyout.ShowFlyout(new PrivacyPolicyControl());
        }
        #endregion

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        //private async void OnSuspending(object sender, SuspendingEventArgs e)
        //{
        //    var deferral = e.SuspendingOperation.GetDeferral();

        //    deferral.Complete();
        //}

    }
}
