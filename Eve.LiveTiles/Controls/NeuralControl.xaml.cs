﻿using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal partial class NeuralControl : UserControl
    {
        #region Private Members

        #endregion

        #region CTOR
        public NeuralControl()
        {
            this.InitializeComponent();

            Model = DependencyFactory.Resolve<SkillPlannerViewModel>().NeuralRemap;
        }
        #endregion

        #region Methods
        public void UpdateControl()
        {
            SolidColorBrush scb = null;

            //reset colors
            for (int i = 0; i < Maximum; i++)
            {
                scb = new SolidColorBrush();
                scb.Color = Color.FromArgb(255, 67, 67, 67);
                Rectangle rect = LevelsStackPanel.Children[i] as Rectangle;

                if (rect != null)
                {
                    rect.Fill = scb;
                }
            }

            //set green
            for (int i = 0; i < Value; i++)
            {
                scb = new SolidColorBrush();
                scb.Color = Color.FromArgb(255, 30, 220, 36);
                Rectangle rect = LevelsStackPanel.Children[i] as Rectangle;

                if (rect != null)
                {
                    rect.Fill = scb;
                }
            }
        }
        #endregion

        #region Properties
        protected NeuralRemapModel Model { get; private set; }

        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(int), typeof(NeuralControl), new PropertyMetadata(0, OnValueChanged));

        public bool DisableButtons
        {
            get { return (bool)GetValue(DisableButtonsProperty); }
            set { SetValue(DisableButtonsProperty, value); }
        }

        public static readonly DependencyProperty DisableButtonsProperty =
            DependencyProperty.Register("DisableButtons", typeof(bool), typeof(NeuralControl), new PropertyMetadata(0, (DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e) =>
            {
                NeuralControl neural = dependencyObject as NeuralControl;
                neural.MinusBtn.Visibility = neural.DisableButtons ? Visibility.Collapsed : Visibility.Visible;
                neural.PlusBtn.Visibility = neural.DisableButtons ? Visibility.Collapsed : Visibility.Visible;
            }));

        public int Maximum
        {
            get { return (int)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(int), typeof(NeuralControl), new PropertyMetadata(0, (DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e) =>
            {
                NeuralControl neural = dependencyObject as NeuralControl;
                SolidColorBrush scb = null;
                Rectangle myRectangle = null;

                for (int i = 0; i < neural.Maximum; i++)
                {
                    myRectangle = new Rectangle();
                    myRectangle.Width = 16;
                    myRectangle.Height = 14;
                    if (i == neural.Maximum - 1)
                    {
                        myRectangle.Margin = new Thickness(1, 0, 1, 0);
                    }
                    else
                    {
                        myRectangle.Margin = new Thickness(1, 0, 0, 0);
                    }
                    scb = new SolidColorBrush();
                    scb.Color = Color.FromArgb(255, 67, 67, 67);
                    myRectangle.Fill = scb;
                    neural.LevelsStackPanel.Children.Add(myRectangle);
                }

            }));

        #endregion

        #region Events
        private static void OnValueChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            NeuralControl neural = dependencyObject as NeuralControl;
            neural.UpdateControl();
        }

        protected virtual void OnMinusBtnClick(object sender, RoutedEventArgs e)
        {
            if (Value > 0)
            {
                Value -= 1;
                Model.MappablePool += 1;
            }
        }

        protected virtual void OnPlusBtnClick(object sender, RoutedEventArgs e)
        {
            if (Value < Maximum && Model.MappablePool > 0)
            {
                Value += 1;
                Model.MappablePool -= 1;
            }
        }
        #endregion

    }
}
