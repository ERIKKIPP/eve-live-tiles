﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Eve.LiveTiles
{
    public sealed class ExtendedTextBox : TextBox
    {
        public ExtendedTextBox()
        {
            TextChanged += OnTextChanged;
        }

        private void OnTextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            if (ImmediateUpdates)
            {
                ExText = base.Text;
            }
        }

        public static readonly DependencyProperty ExTextProperty = DependencyProperty.Register("ExText", typeof(string),
            typeof(ExtendedTextBox), new PropertyMetadata(default(string), (o, args) => ((ExtendedTextBox)o).SetText(args.NewValue)));

        private void SetText(object newValue)
        {
            base.Text = newValue != null ? (String)newValue : string.Empty;
        }

        public string ExText
        {
            get { return (string)GetValue(ExTextProperty); }
            set { SetValue(ExTextProperty, value); }
        }

        public static readonly DependencyProperty ImmediateUpdatesProperty = DependencyProperty.Register("ImmediateUpdates", typeof(bool), typeof(ExtendedTextBox), new PropertyMetadata(true));
        public bool ImmediateUpdates
        {
            get { return (bool)GetValue(ImmediateUpdatesProperty); }
            set { SetValue(ImmediateUpdatesProperty, value); }
        }
    }
}
