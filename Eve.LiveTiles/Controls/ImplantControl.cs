﻿using Eve.LiveTiles.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Eve.LiveTiles
{
    internal sealed class ImplantControl : NeuralControl
    {
        public ImplantControl()
            : base()
        {

        }

        private void UpdateImplant()
        {
            switch (ImplantType)
            {
                case AttributeType.None:
                    break;
                case AttributeType.Intelligence:
                    Model.IntelligenceImplant = Value;
                    break;
                case AttributeType.Memory:
                    Model.MemoryImplant = Value;
                    break;
                case AttributeType.Charisma:
                    Model.CharismaImplant = Value;
                    break;
                case AttributeType.Perception:
                    Model.PerceptionImplant = Value;
                    break;
                case AttributeType.Willpower:
                    Model.WillpowerImplant = Value;
                    break;
                default:
                    break;
            }
        }

        protected override void OnPlusBtnClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (Value < Maximum)
            {
                Value += 1;
                UpdateImplant();
            }
        }

        protected override void OnMinusBtnClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (Value > 0)
            {
                Value -= 1;
                UpdateImplant();
            }
        }
        
        public AttributeType ImplantType
        {
            get { return (AttributeType)GetValue(ImplantTypeProperty); }
            set { SetValue(ImplantTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImplantType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImplantTypeProperty =
            DependencyProperty.Register("ImplantType", typeof(AttributeType), typeof(ImplantControl), new PropertyMetadata(0));


    }
}
