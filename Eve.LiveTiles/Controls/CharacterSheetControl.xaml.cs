﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    public sealed partial class CharacterSheetControl : UserControl, IVariableGridViewItem
    {
        public CharacterSheetControl()
        {
            this.InitializeComponent();
        }

        public CharacterSheetControl(EveCharacter character)
        {
            this.InitializeComponent();

            this.DataContext = character;
        }

        public int ColumnSpan { get { return 8; } }

        public int RowSpan { get { return 4; } }

        public string Title { get { return ResourceHelper.Instance.GetValue("CharacterInfo"); } }

        public object ControlContent { get { return this; } }
    }
}
