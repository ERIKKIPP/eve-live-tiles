﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    public sealed partial class LevelControl : UserControl
    {
        public LevelControl()
        {
            this.InitializeComponent();
        }
        
        public void UpdateControl()
        {
            Rectangle myRectangle = null;
            SolidColorBrush scb = null;

            for (int i = 0; i < Level - 1; i++)
            {
                myRectangle = new Rectangle();
                myRectangle.Name = i.ToString();
                myRectangle.Width = 8;
                myRectangle.Height = 4;
                myRectangle.Margin = new Thickness(1, 0, 0, 0);
                scb = new SolidColorBrush();
                scb.Color = Color.FromArgb(255, 129, 129, 129);
                myRectangle.Fill = scb;
                LevelsStackPanel.Children.Insert(0, myRectangle);
            }
        }

        public static DependencyProperty LevelProperty = DependencyProperty.Register("Level", typeof(int), typeof(LevelControl), new PropertyMetadata(0, OnLevelValueChanged));
        public int Level
        {
            get { return (int)GetValue(LevelProperty); }
            set { SetValue(LevelProperty, value); }
        }

        public static DependencyProperty QueuePositionProperty = DependencyProperty.Register("QueuePosition", typeof(int), typeof(LevelControl), new PropertyMetadata(0, OnQueuePositionValueChanged));
        public int QueuePosition
        {
            get { return (int)GetValue(QueuePositionProperty); }
            set { SetValue(QueuePositionProperty, value); }
        }

        private static void OnQueuePositionValueChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            LevelControl lvl = dependencyObject as LevelControl;

            if (lvl.QueuePosition > 0)
            {
                lvl.Visibility = Visibility.Collapsed;
            }
        }

        private static void OnLevelValueChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            LevelControl lvl = dependencyObject as LevelControl;
            lvl.UpdateControl();
        }
    }
}
