﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal sealed partial class ResourcesControl : BaseFittingControl
    {


        public ResourcesControl()
            : base()
        {
            this.InitializeComponent();
        }

        public ResourcesControl(ShipFittingModel ship, IEnumerable<IGrouping<int, SkillAffect>> skillAffects)
            : base(ship, skillAffects)
        {
            this.InitializeComponent();
        }

        protected override void UpdateUI()
        {
            ColumnSpan = 33;
            RowSpan = 22;
            Title = ResourceHelper.Instance.GetValue("ResourcesTitle");
            ControlContent = this;
        }

        protected async override Task LoadData()
        {
            await Task.Run(() =>
                  {
                      Ship.Resources.CPU.Allowed = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.CpuOutputBonus2, InventoryGroupType.Electronics, Ship.GetValue(AttributeIdType.CpuOutput));
                      Ship.Resources.CPU.Used = 0;

                      Ship.Resources.Powergrid.Allowed = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.PowerEngineeringOutputBonus, InventoryGroupType.Engineering, Ship.GetValue(AttributeIdType.PowerOutput));
                      Ship.Resources.Powergrid.Used = 0;

                      Ship.Resources.Calibration.Allowed = Ship.GetValue(AttributeIdType.UpgradeCapacity);
                      Ship.Resources.Calibration.Used = 0;

                      Ship.Resources.Launchers.Allowed = (int)Ship.GetValue(AttributeIdType.LauncherSlotsLeft);
                      Ship.Resources.Launchers.Used = 0;

                      Ship.Resources.Turrets.Allowed = (int)Ship.GetValue(AttributeIdType.TurretSlotsLeft);
                      Ship.Resources.Turrets.Used = 0;

                      Ship.Resources.DroneCargoCapacity.Allowed = Ship.GetValue(AttributeIdType.DroneCapacity); //drone bay max
                      Ship.Resources.DroneCargoCapacity.Used = 0;

                      Ship.Resources.DroneBandwidth.Allowed = Ship.GetValue(AttributeIdType.DroneBandwidth);//drone bandwidth (bandwidth of drones allowed in space)
                      Ship.Resources.DroneBandwidth.Used = 0;

                      //drones allowed (count of drones allowed in space)
                      Ship.Resources.Drones.Allowed = Ship.GetAffectValue<int>(SkillAffects, AttributeIdType.MaxActiveDroneBonus, InventoryGroupType.Drones, 0);
                      Ship.Resources.Drones.Used = 0;

                      Ship.Resources.CargoBay = string.Format(CultureInfo.InvariantCulture, "{0} m3", Ship.Inventory.InventoryItem.Capacity);

                  });
        }

        private async void BaseFittingControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DataLoaded)
            {
                Ship.Resources = new ResourcePanelModel();
                Model = Ship.Resources;
                await IsBusyDataLoader.LoadAsync(() => LoadData());

                RaisePropertyChanged(() => Model);

                DataLoaded = true;
            }
        }

        public ResourcePanelModel Model { get; private set; }
    }
}
