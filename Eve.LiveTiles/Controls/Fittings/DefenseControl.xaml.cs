﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Eve.LiveTiles.Extensions;
using System.Threading.Tasks;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal sealed partial class DefenseControl : BaseFittingControl
    {
        public DefenseControl()
            : base()
        {
            this.InitializeComponent();
        }

        public DefenseControl(ShipFittingModel ship, IEnumerable<IGrouping<int, SkillAffect>> skillAffects)
            : base(ship, skillAffects)
        {
            this.InitializeComponent();
        }

        private static string GetText(ShipFittingModel ship, AttributeIdType primaryattType, AttributeIdType secondaryattType,
            InventoryGroupType invGroup, IEnumerable<IGrouping<int, SkillAffect>> skillAffects, out double attValue)
        {
            attValue = 0;
            var query = (from item in ship.TypeAttributes
                         where item.AttributeType.AttributeID == (int)primaryattType
                         select item).FirstOrDefault();

            if (query != null)
            {
                double queryValue = query.TypeAttribute.ValueInt != null ? (double)query.TypeAttribute.ValueInt : query.TypeAttribute.ValueFloat.GetValueOrDefault();
                attValue = ship.GetAffectValue<double>(skillAffects, secondaryattType, invGroup, queryValue);
                return string.Format(CultureInfo.InvariantCulture, "{0:N1} {1}", attValue, query.Unit.DisplayName);
            }

            return null;
        }

        protected override void UpdateUI()
        {
            ColumnSpan = 35;
            RowSpan = 17;
            Title = ResourceHelper.Instance.GetValue("DefenseTitle");
            ControlContent = this;
        }

        protected async override Task LoadData()
        {
            await Task.Run(() =>
            {
                Model.ShieldEm = Ship.GetResistValue(AttributeIdType.ShieldEmDamageResonance);
                Model.ShieldExplosive = Ship.GetResistValue(AttributeIdType.ShieldExplosiveDamageResonance);
                Model.ShieldKinetic = Ship.GetResistValue(AttributeIdType.ShieldKineticDamageResonance);
                Model.ShieldThermal = Ship.GetResistValue(AttributeIdType.ShieldThermalDamageResonance);

                Model.ArmorEm = Ship.GetResistValue(AttributeIdType.ArmorEmDamageResonance);
                Model.ArmorExplosive = Ship.GetResistValue(AttributeIdType.ArmorExplosiveDamageResonance);
                Model.ArmorKinetic = Ship.GetResistValue(AttributeIdType.ArmorKineticDamageResonance);
                Model.ArmorThermal = Ship.GetResistValue(AttributeIdType.ArmorThermalDamageResonance);

                Model.StructureEm = Ship.GetResistValue(AttributeIdType.EmDamageResonance);
                Model.StructureExplosive = Ship.GetResistValue(AttributeIdType.ExplosiveDamageResonance);
                Model.StructureKinetic = Ship.GetResistValue(AttributeIdType.KineticDamageResonance);
                Model.StructureThermal = Ship.GetResistValue(AttributeIdType.ThermalDamageResonance);

                double structure = 0;
                double shield = 0;
                double armor = 0;
                Model.StructureHitpoints = GetText(Ship, AttributeIdType.Hitpoints, AttributeIdType.HullHpBonus, InventoryGroupType.Mechanics, SkillAffects, out structure);
                Model.ShieldCapacity = GetText(Ship, AttributeIdType.ShieldCapacity, AttributeIdType.ShieldCapacityBonus, InventoryGroupType.Engineering, SkillAffects, out shield);
                Model.ArmorHitpoints = GetText(Ship, AttributeIdType.ArmorHP, AttributeIdType.ArmorHpBonus, InventoryGroupType.Mechanics, SkillAffects, out armor);

                //h_eff = 1/(1-r) * h

                double shieldAvg = (Model.ShieldEm + Model.ShieldExplosive + Model.ShieldKinetic + Model.ShieldThermal) / 4;
                double armorAvg = (Model.ArmorEm + Model.ArmorExplosive + Model.ArmorKinetic + Model.ArmorThermal) / 4;
                double hullAvg = (Model.StructureEm + Model.StructureExplosive + Model.StructureKinetic + Model.StructureThermal) / 4;

                double s_eff = 1 / (1 - (shieldAvg / 100.0)) * shield;
                double a_eff = 1 / (1 - (armorAvg / 100.0)) * armor;
                double h_eff = 1 / (1 - (hullAvg / 100.0)) * structure;

                double eff = s_eff + a_eff + h_eff;

                Model.EffectiveHp = string.Format(CultureInfo.InvariantCulture, "{0:N2}", eff);

                //tooltip for shield           
                double rechargerate = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.Rechargeratebonus, InventoryGroupType.Engineering, Ship.GetValue(AttributeIdType.ShieldRechargeRate));
                string shieldtt = ResourceHelper.Instance.GetValue("ShieldTooltip");
                TimeSpan rechargeTimespan = new TimeSpan(0, 0, 0, 0, (int)rechargerate);

                double peak = (5.0 * shield) / (2.0 * rechargerate) * 1000.0;

                Model.ShieldTooltip = string.Format(CultureInfo.CurrentUICulture, shieldtt, rechargeTimespan.TotalSeconds, peak, s_eff);

                //tooltip for armor
                string armortt = ResourceHelper.Instance.GetValue("ArmorTooltip");
                Model.ArmorTooltip = string.Format(CultureInfo.CurrentUICulture, armortt, a_eff);

                //tooltip for hull
                string hulltt = ResourceHelper.Instance.GetValue("StructureTooltip");
                Model.StructureTooltip = string.Format(CultureInfo.CurrentUICulture, hulltt, a_eff);

                //eve eff hp
                //eve uses the lowest resist instead of an average
                double lowshieldResist = Math.Min(Math.Min(Model.ShieldEm, Model.ShieldExplosive), Math.Min(Model.ShieldKinetic, Model.ShieldThermal));
                double lowarmorResist = Math.Min(Math.Min(Model.ArmorEm, Model.ArmorExplosive), Math.Min(Model.ArmorKinetic, Model.ArmorThermal));
                double lowhullResist = Math.Min(Math.Min(Model.StructureEm, Model.StructureExplosive), Math.Min(Model.StructureKinetic, Model.StructureThermal));

                s_eff = 1 / (1 - (lowshieldResist / 100.0)) * shield;
                a_eff = 1 / (1 - (lowarmorResist / 100.0)) * armor;
                h_eff = 1 / (1 - (lowhullResist / 100.0)) * structure;

                eff = s_eff + a_eff + h_eff;

                string effhp = ResourceHelper.Instance.GetValue("EffectiveHPTooltip");
                Model.EveEffectiveHp = string.Format(CultureInfo.InvariantCulture, effhp, eff);
            });
        }

        public async void BaseFittingControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DataLoaded)
            {
                Model = new DefensePanelModel();
                await IsBusyDataLoader.LoadAsync(() => LoadData());

                RaisePropertyChanged(() => Model);

                DataLoaded = true;
            }
        }
        
        public DefensePanelModel Model { get; private set; }
    }
}
