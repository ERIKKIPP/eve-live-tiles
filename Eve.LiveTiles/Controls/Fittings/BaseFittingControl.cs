﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Eve.LiveTiles
{
    internal class BaseFittingControl : UserControl, IVariableGridViewItem, INotifyPropertyChanged
    {
        public BaseFittingControl()
        {
            IsBusyDataLoader = new DataLoader(true);
        }

        public BaseFittingControl(ShipFittingModel ship, IEnumerable<IGrouping<int, SkillAffect>> skillAffects)
            : this()
        {

            this.DataContext = this;
            Ship = ship;
            SkillAffects = skillAffects;
            UpdateUI();
        }

        protected virtual void UpdateUI() { }

        protected virtual Task LoadData() { return null; }

        public DataLoader IsBusyDataLoader { get; private set; }

        protected ShipFittingModel Ship { get; private set; }

        protected bool DataLoaded { get; set; }

        protected IEnumerable<IGrouping<int, SkillAffect>> SkillAffects { get; private set; }

        private static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            MemberExpression memberExpression = expression.Body as MemberExpression;

            if (memberExpression == null)
                memberExpression = (MemberExpression)((UnaryExpression)expression.Body).Operand;

            return memberExpression.Member.Name;
        }

        #region IVariableGridViewItem
        public int ColumnSpan { get; protected set; }

        public int RowSpan { get; protected set; }

        public string Title { get; protected set; }

        public object ControlContent { get; protected set; }
        #endregion

        #region Events
        /// <summary>
        /// PropertyChanged for INotifyPropertyChanged implementation
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// RaisePropertyChanged for INotifyPropertyChanged implementation
        /// </summary>
        /// <param name="propertyName"></param>
        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// RaisePropertyChanged for INotifyPropertyChanged implementation
        /// </summary>
        /// <param name="expression"></param>
        public void RaisePropertyChanged(Expression<Func<object>> expression)
        {
            RaisePropertyChanged(GetPropertyName(expression));
        }

        #endregion
    }
}
