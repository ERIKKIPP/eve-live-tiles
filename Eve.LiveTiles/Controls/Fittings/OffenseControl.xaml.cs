﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal sealed partial class OffenseControl : BaseFittingControl
    {
       
        public OffenseControl()
            : base()
        {
            this.InitializeComponent();
        }

        public OffenseControl(ShipFittingModel ship, IEnumerable<IGrouping<int, SkillAffect>> skillAffects)
            : base(ship, skillAffects)
        {
            this.InitializeComponent();
        }
        
        protected override void UpdateUI()
        {
            ColumnSpan = 33;
            RowSpan = 9;
            Title = ResourceHelper.Instance.GetValue("OffenseTitle");
            ControlContent = this;
        }

        protected async override Task LoadData()
        {
            await Task.Run(() =>
            {
                
            });
        }

        private async void BaseFittingControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DataLoaded)
            {
                Model = new OffensePanelModel();
                await IsBusyDataLoader.LoadAsync(() => LoadData());

                RaisePropertyChanged(() => Model);

                DataLoaded = true;
            }
        }

        public OffensePanelModel Model { get; private set; }
    }
}
