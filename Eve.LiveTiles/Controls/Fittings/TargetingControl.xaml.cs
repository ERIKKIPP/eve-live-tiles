﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal sealed partial class TargetingControl : BaseFittingControl
    {
       
        public TargetingControl()
            : base()
        {
            this.InitializeComponent();
        }

        public TargetingControl(ShipFittingModel ship, IEnumerable<IGrouping<int, SkillAffect>> skillAffects)
            : base(ship, skillAffects)
        {
            this.InitializeComponent();
        }
        
        /// <summary>
        /// Inverse Hyperbolic Sine 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private static double HArcsin(double x)
        {
            return Math.Log(x + Math.Sqrt(x * x + 1));
        }

        /// <summary>
        /// T = (40000/X)/(asinh(Y)^2)
        /// http://wiki.eveonline.com/en/wiki/Targeting_speed
        /// </summary>
        /// <returns></returns>
        private static double LockTime(double scanRes, double sigRadius)
        {
            return ((40000 / scanRes) / (Math.Pow(HArcsin(sigRadius), 2)));
        }
        
        protected override void UpdateUI()
        {
            ColumnSpan = 33;
            RowSpan = 13;
            Title = ResourceHelper.Instance.GetValue("TargetingTitle");
            ControlContent = this;
        }

        protected async override Task LoadData()
        {
            await Task.Run(() =>
            {
                string unit = null;

                double playertargets = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.MaxTargetBonus, InventoryGroupType.Electronics, 2);
                Model.MaxTargetCount = string.Format(CultureInfo.InvariantCulture, "{0}x / {1}x", Ship.GetValue(AttributeIdType.MaxLockedTargets), playertargets);
                Model.SignatureRadius = Ship.GetStringValue(AttributeIdType.SignatureRadius, "{0:N0}");

                double scanres = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.ScanResolutionBonus, InventoryGroupType.Electronics, Ship.GetValue(AttributeIdType.ScanResolution, out unit));
                Model.ScanResolution = string.Format(CultureInfo.InvariantCulture, "{0:N0} {1}", scanres, unit);

                double maxtarget = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.ScanResolutionBonus, InventoryGroupType.Electronics, Ship.GetValue(AttributeIdType.MaxTargetRange));
                Model.MaxTargetRange = string.Format(CultureInfo.InvariantCulture, "{0:N2} km", maxtarget / 1000);

                double radar = Ship.GetValue(AttributeIdType.ScanRadarStrength, out unit);
                double ladar = Ship.GetValue(AttributeIdType.ScanLadarStrength, out unit);
                double mag = Ship.GetValue(AttributeIdType.ScanMagnetometricStrength, out unit);
                double grav = Ship.GetValue(AttributeIdType.ScanGravimetricStrength, out unit);
                string scantype = string.Empty;

                if (radar > 0)
                {
                    scantype = ResourceHelper.Instance.GetValue("ScanRadarStrength");
                    Model.SensorStrength = string.Format(CultureInfo.CurrentUICulture, "{0:N2} {1}", radar, unit);
                }
                else if (ladar > 0)
                {
                    scantype = ResourceHelper.Instance.GetValue("ScanLadarStrength");
                    Model.SensorStrength = string.Format(CultureInfo.CurrentUICulture, "{0:N2} {1}", ladar, unit);
                }
                else if (mag > 0)
                {
                    scantype = ResourceHelper.Instance.GetValue("ScanMagnetometricStrength");
                    Model.SensorStrength = string.Format(CultureInfo.CurrentUICulture, "{0:N2} {1}", mag, unit);
                }
                else if (grav > 0)
                {
                    scantype = ResourceHelper.Instance.GetValue("ScanGravimetricStrength");
                    Model.SensorStrength = string.Format(CultureInfo.CurrentUICulture, "{0:N2} {1}", grav, unit);
                }

                Model.SensorStrengthTooltip = string.Format(CultureInfo.CurrentUICulture, ResourceHelper.Instance.GetValue("SensorStrengthTooltip"), scantype);

                Model.ScanResolutionTooltip = string.Format(CultureInfo.CurrentUICulture, ResourceHelper.Instance.GetValue("ScanResolutionTooltip"),
                    LockTime(scanres, (int)HullType.Capsule),
                    LockTime(scanres, (int)HullType.Frigate),
                    LockTime(scanres, (int)HullType.Destroyer),
                    LockTime(scanres, (int)HullType.Cruiser),
                    LockTime(scanres, (int)HullType.Battlecruiser),
                    LockTime(scanres, (int)HullType.Battleship),
                    LockTime(scanres, (int)HullType.Dreadnought),
                    LockTime(scanres, (int)HullType.Carrier),
                    LockTime(scanres, (int)HullType.Mothership),
                    LockTime(scanres, (int)HullType.Titan));

            });
        }

        private async void BaseFittingControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DataLoaded)
            {
                Model = new TargetPanelModel();
                await IsBusyDataLoader.LoadAsync(() => LoadData());

                RaisePropertyChanged(() => Model);

                DataLoaded = true;
            }
        }

        public TargetPanelModel Model { get; private set; }

    }
}
