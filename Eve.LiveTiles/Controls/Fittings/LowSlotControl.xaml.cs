﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal sealed partial class LowSlotControl : SlotControlBase
    {
        public LowSlotControl()
            : base()
        {
            this.InitializeComponent();
        }

        public LowSlotControl(InventoryModel item)
            : base(item)
        {
            this.InitializeComponent();
            UpdateUI();
            UpdateValues();
        }

        protected override void UpdateUI()
        {
            ControlContent = this;
            ColumnSpan = 4;

            if (InventoryItem != null)
            {
                ControlGrid.Visibility = Visibility.Visible;
                SlotImage.Visibility = Visibility.Collapsed;
                Title = InventoryItem.InventoryItem.TypeName;

                RowSpan = 2;
                ControlGrid.Height = 150;
                MainGrid.Height = 150;
            }
            else
            {
                RowSpan = 1;
                ControlGrid.Height = 75;
                MainGrid.Height = 75;
            }
        }

        protected async override void UpdateValues()
        {
            if (InventoryItem != null)
            {
                await IsBusyDataLoader.LoadAsync(() =>
                {
                    return Task.Run(() =>
                    {
                        CPU = GetDgmValue(InventoryItem.TypeAttributes, AttributeIdType.Cpu);
                        Powergrid = GetDgmValue(InventoryItem.TypeAttributes, AttributeIdType.Power);

                    });
                });

                RaisePropertyChanged(() => CPU);
                RaisePropertyChanged(() => Powergrid);
            }

        }    
    }
}
