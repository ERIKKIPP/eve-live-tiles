﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Eve.LiveTiles
{
    internal class SlotControlBase : UserControl, IVariableGridViewItem, ISlotControl, INotifyPropertyChanged
    {
        #region Private Members
        private InventoryModel _inventoryItem;
        #endregion

        #region CTOR
        public SlotControlBase()
        {

        }

        public SlotControlBase(InventoryModel item)
        {
            _inventoryItem = item;

            IsBusyDataLoader = new DataLoader(false);
        }
        #endregion

        #region Methods
        protected virtual void UpdateValues() { }

        protected virtual void UpdateUI() { }

        protected static int GetDgmValue(IEnumerable<IGrouping<string, TypeAttributesModel>> dgmType, AttributeIdType attType)
        {
            var query = (from item in dgmType
                         from grp in item
                         where grp.TypeAttribute.AttributeID == (int)attType
                         select grp.TypeAttribute).FirstOrDefault();

            if (query != null)
            {
                return query.ValueInt != null ? query.ValueInt.Value : (int)query.ValueFloat.GetValueOrDefault();
            }

            return 0;
        }

        private static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            MemberExpression memberExpression = expression.Body as MemberExpression;

            if (memberExpression == null)
                memberExpression = (MemberExpression)((UnaryExpression)expression.Body).Operand;

            return memberExpression.Member.Name;
        }
        #endregion

        #region Properties

        #region IVariableGridViewItem
        public int ColumnSpan { get; protected set; }

        public int RowSpan { get; protected set; }

        public string Title { get; protected set; }

        public object ControlContent { get; protected set; }
        #endregion

        #region ISlotControl
        public int Index { get; set; }

        public InventoryModel InventoryItem
        {
            get { return _inventoryItem; }
            set
            {
                _inventoryItem = value;
                UpdateUI();
                UpdateValues();
            }
        }


        #endregion

        public DataLoader IsBusyDataLoader { get; private set; }

        //high mid and low
        public int CPU { get; set; }

        public int Powergrid { get; set; }

        public int ActivationCost { get; set; }

        public int ActivationTime { get; set; }

        public int CapacitorRate { get; set; }

        //high and mid
        public int OptimalRange { get; set; }

        public int Falloff { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// PropertyChanged for INotifyPropertyChanged implementation
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// RaisePropertyChanged for INotifyPropertyChanged implementation
        /// </summary>
        /// <param name="propertyName"></param>
        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// RaisePropertyChanged for INotifyPropertyChanged implementation
        /// </summary>
        /// <param name="expression"></param>
        public void RaisePropertyChanged(Expression<Func<object>> expression)
        {
            RaisePropertyChanged(GetPropertyName(expression));
        }

        #endregion
    }
}
