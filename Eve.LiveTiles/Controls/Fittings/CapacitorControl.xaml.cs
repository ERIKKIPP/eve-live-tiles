﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Eve.LiveTiles.Extensions;
using System.Threading.Tasks;


// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal sealed partial class CapacitorControl : BaseFittingControl
    {        
        public CapacitorControl()
            : base()
        {
            this.InitializeComponent();
        }

        public CapacitorControl(ShipFittingModel ship, IEnumerable<IGrouping<int, SkillAffect>> skillAffects)
            : base(ship, skillAffects)
        {
            this.InitializeComponent();
        }

        protected override void UpdateUI()
        {
            ColumnSpan = 33;
            RowSpan = 8;
            Title = ResourceHelper.Instance.GetValue("CapacitorTitle");
            ControlContent = this;
        }

        protected async override Task LoadData()
        {
            await Task.Run(() =>
            {
                var recharge = (from item in Ship.TypeAttributes
                                where item.AttributeType.AttributeID == (int)AttributeIdType.RechargeRate
                                select item).FirstOrDefault();

                var cap = (from item in Ship.TypeAttributes
                           where item.AttributeType.AttributeID == (int)AttributeIdType.CapacitorCapacity
                           select item).FirstOrDefault();

                if (recharge != null && cap != null)
                {
                    //http://mcc3d.com/game/EVE-Online:Formulae
                    double rechargerate = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.CapRechargeBonus, InventoryGroupType.Engineering, recharge.TypeAttribute.ValueFloat.GetValueOrDefault());
                    double capacity = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.CapacitorCapacityBonus, InventoryGroupType.Engineering, cap.TypeAttribute.ValueFloat.GetValueOrDefault());
                    double currentCapacitor = capacity;
                    double minCapacitor = capacity;
                    double timeToDrain = rechargerate;

                    //          5M
                    //peak  =   ---
                    //          2T
                    double peak = (5.0 * capacity) / (2.0 * rechargerate);

                    //R = is the capacitor/shield recharge rate
                    //M = is the maximum capacitor/shield value
                    //T = is the time required for the capacitor/shield to recharge
                    //P = is the current percentage of capacitor/shield available

                    Model.Capacity = string.Format(CultureInfo.InvariantCulture, "{0:n2} {1}", capacity, cap.Unit.DisplayName);
                    TimeSpan rechargeTimespan = new TimeSpan(0, 0, 0, 0, (int)rechargerate);
                    Model.RechargeRate = string.Format(CultureInfo.InvariantCulture, "{0} {1} ({2})", rechargeTimespan.TotalSeconds, recharge.Unit.DisplayName, rechargeTimespan.TimeSpanText());
                    Model.PeakRecharge = string.Format(CultureInfo.InvariantCulture, "{0}{1:n2}", peak >= 0 ? "+" : "-", peak * 1000);

                    if (currentCapacitor > 0)
                    {
                        string msg = ResourceHelper.Instance.GetValue("StableAt");
                        Model.IsCapStable = true;
                        Model.StableMessage = string.Format(CultureInfo.InvariantCulture, msg, (minCapacitor / capacity) * 100);
                    }
                    else
                    {
                        string msg = ResourceHelper.Instance.GetValue("Lasts");
                        Model.IsCapStable = false;
                        TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)timeToDrain);
                        Model.StableMessage = string.Format(CultureInfo.InvariantCulture, msg, ts.TimeSpanText());
                    }

                    //double capConstant = (rechargerate / 5.0);
                    //double currentTime = 0;
                    //double nextTime = 0;

                    //model.Capacity = (Math.Pow((1.0 + ((Math.Sqrt((capacitor / capacitor)) - 1.0) * Math.Exp(((currentTime - nextTime) / capConstant)))), 2.0) * capacitor);

                    //double maxTime = 43200; //12 hour
                    //double minCap = capacitor;
                    //double minCapTime = 0;
                    //double oldCap = 0;

                    //while (((capacitor > 0.0) & (nextTime < maxTime)))
                    //{
                    //    oldCap = capacitor;
                    //    capacitor = ((Math.Pow((1.0 + ((Math.Sqrt((capacitor / CapacitorCapacity)) - 1.0) * Math.Exp(((currentTime - nextTime) / capConstant)))), 2)) * CapacitorCapacity);
                    //    currentTime = nextTime;
                    //    nextTime = maxTime;

                    //    if (capacitor < minCap)
                    //    {
                    //        minCap = capacitor;
                    //        minCapTime = currentTime;
                    //    }
                    //}
                }
            });
        }

        public CapacitorPanelModel Model { get; private set; }

        private async void BaseFittingControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DataLoaded)
            {
                Model = new CapacitorPanelModel();
                await IsBusyDataLoader.LoadAsync(() => LoadData());

                RaisePropertyChanged(() => Model);

                DataLoaded = true;
            }
        }
    }
}
