﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Eve.LiveTiles
{
    internal sealed partial class NavigationControl : BaseFittingControl
    {
        public NavigationControl()
            : base()
        {
            this.InitializeComponent();
        }

        public NavigationControl(ShipFittingModel ship, IEnumerable<IGrouping<int, SkillAffect>> skillAffects)
            : base(ship, skillAffects)
        {
            this.InitializeComponent();
        }

        protected override void UpdateUI()
        {
            ColumnSpan = 33;
            RowSpan = 13;
            Title = ResourceHelper.Instance.GetValue("NavigationTitle");
            ControlContent = this;
        }

        protected async override Task LoadData()
        {
            await Task.Run(() =>
            {
                string unit = null;

                Model.ShipwarpSpeed = string.Format(CultureInfo.InvariantCulture, "{0:N2} AU/s", (Ship.GetValue(AttributeIdType.BaseWarpSpeed) * 3.0) * Ship.GetValue(AttributeIdType.WarpSpeedMultiplier));

                Model.MaxVelocity = string.Format(CultureInfo.InvariantCulture, "{0:N2} {1}",
                    Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.VelocityBonus, InventoryGroupType.Navigation, Ship.GetValue(AttributeIdType.MaxVelocity, out unit)), unit);

                Model.Mass = string.Format(CultureInfo.InvariantCulture, "{0:N2} t", Ship.Inventory.InventoryItem.Mass / 1000.0);

                double modifier = Ship.GetAffectValueRequiredOnly<double>(SkillAffects, AttributeIdType.AgilityBonus, InventoryGroupType.SpaceshipCommand, Ship.GetValue(AttributeIdType.Agility, out unit));
                modifier = Ship.GetAffectValue<double>(SkillAffects, AttributeIdType.AgilityBonus, InventoryGroupType.Navigation, modifier);
                Model.InertiaModifier = string.Format(CultureInfo.InvariantCulture, "{0:N4}{1}", modifier, unit);

                //align time = (ln(2) * Inertia Modifier * Mass) / 500,000.
                Model.AlignTime = string.Format(CultureInfo.InvariantCulture, "{0:N2} s", (Math.Log(2) * modifier * Ship.Inventory.InventoryItem.Mass.GetValueOrDefault()) / 500000);
            });
        }

        private async void BaseFittingControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DataLoaded)
            {
                Model = new NavigationPanelModel();
                await IsBusyDataLoader.LoadAsync(() => LoadData());

                RaisePropertyChanged(() => Model);

                DataLoaded = true;
            }
        }

        public NavigationPanelModel Model { get; private set; }
    }
}
