﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace Eve.LiveTiles
{
    [ContentProperty(Name="ControlContent")]
    public sealed class GridTemplateControl : Control
    {
        public GridTemplateControl()
        {
            this.DefaultStyleKey = typeof(GridTemplateControl);
        }

        public static DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(GridTemplateControl), new PropertyMetadata(null));
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static DependencyProperty ControlContentProperty = DependencyProperty.Register("ControlContent", typeof(object), typeof(GridTemplateControl), new PropertyMetadata(null));
        public object ControlContent
        {
            get { return (object)GetValue(ControlContentProperty); }
            set { SetValue(ControlContentProperty, value); }
        }

        public static DependencyProperty GridWidthProperty = DependencyProperty.Register("GridWidth", typeof(double), typeof(GridTemplateControl), new PropertyMetadata(null));
        public double GridWidth
        {
            get { return (double)GetValue(GridWidthProperty); }
            set { SetValue(GridWidthProperty, value); }
        }

        public static DependencyProperty GridHeightProperty = DependencyProperty.Register("GridHeight", typeof(double), typeof(GridTemplateControl), new PropertyMetadata(null));
        public double GridHeight
        {
            get { return (double)GetValue(GridHeightProperty); }
            set { SetValue(GridHeightProperty, value); }
        }
   
    }
}
