﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    public interface IVariableGridViewItem
    {
        int ColumnSpan { get; }

        int RowSpan { get; }

        string Title { get; }

        object ControlContent { get; }
    }
}
