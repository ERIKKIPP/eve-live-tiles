﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Core;

namespace Eve.LiveTiles
{
    internal sealed class ResourceHelper
    {
        #region Private Members
        private ResourceContext _resourceContext;
        private ResourceMap _stringResourceMap;
        private static object s_lockobject = new object();
        private static ResourceHelper s_instance;
        #endregion

        #region CTOR
        private ResourceHelper()
        {
            _resourceContext = ResourceContext.GetForViewIndependentUse();

            _stringResourceMap = ResourceManager.Current.MainResourceMap.GetSubtree("Resources");
        }
        #endregion

        #region Properties
        public static ResourceHelper Instance
        {
            get
            {
                if (s_instance == null)
                {
                    lock (s_lockobject)
                    {
                        if (s_instance == null)
                        {
                            s_instance = new ResourceHelper();
                        }
                    }
                }

                return s_instance;
            }
        }


        #endregion

        public string GetValue(string resource)
        {
            return _stringResourceMap.GetValue(resource, _resourceContext).ValueAsString;
        }
    }
}
