﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Eve.LiveTiles
{
    public static class NavigationHelper
    {
        public static void NavigateToView<T>()
        {
            var root = (Frame)Window.Current.Content;

            if (!(root.Content is T))
                root.Navigate(typeof(T));
        }

        public static void NavigateBack()
        {
            var root = (Frame)Window.Current.Content;
            if (root != null && root.CanGoBack)
            {
                root.GoBack();
            }
        }

        public static void NavigateHome()
        {
            var root = (Frame)Window.Current.Content;
            if (root != null)
            {
                while (root.CanGoBack)
                {
                    root.GoBack();
                }
            }
          
        }

        public static void NavigateToView<T>(object parameter)
        {
            var root = (Frame)Window.Current.Content;

            root.Navigate(typeof(T), parameter);
        }
    }
}
