﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    public sealed class GenericEventArgs<T> : EventArgs
    {
        public GenericEventArgs()
        {

        }

        public T Data { get; set; }
    }
}
