﻿using Eve.LiveTiles.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    internal static class ExpressionsUtilty
    {
        /// <summary>
        /// used in ItemCategories
        /// </summary>
        public static Func<int?, bool> ItemCategoriesByMarketGroup
        {
            get
            {
                return (marketGroupID) => marketGroupID == (int)MarketGroupType.AmmunitionCharges ||
                            marketGroupID == (int)MarketGroupType.Drones || marketGroupID == (int)MarketGroupType.ImplantsBoosters || marketGroupID == (int)MarketGroupType.ShipEquipment
                            || marketGroupID == (int)MarketGroupType.ShipModifications || marketGroupID == (int)MarketGroupType.StarbaseSovereigntyStructures;
            }
        }

        public static Func<int?, bool> ItemCategoriesByCategory
        {
            get
            {
                return (categoryID) => categoryID == (int)InventoryCategoryType.Module || categoryID == (int)InventoryCategoryType.Charge || categoryID == (int)InventoryCategoryType.Drone
                    || categoryID == (int)InventoryCategoryType.Implant || categoryID == (int)InventoryCategoryType.Subsystem;
            }
        }

    }
}
