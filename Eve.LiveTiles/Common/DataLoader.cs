﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles
{
    /// <summary>
    /// DataLoader that enables easy binding to Loading / Finished / Error properties
    /// </summary>
    public sealed class DataLoader : ObservableBase
    {
        #region Private Members

        private LoadingStateType _loadingState;
        private bool _catchExceptions;
        #endregion

        #region CTOR

        /// <summary>
        /// DataLoader constructors
        /// </summary>
        /// <param name="catchExceptions">Swallows exceptions</param>
        public DataLoader(bool? catchExceptions = null)
        {
            if (catchExceptions.HasValue)
            {
                _catchExceptions = catchExceptions.Value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///  Load data. Errors will be in errorcallback
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="loadingMethod"></param>
        /// <param name="resultCallback"></param>
        /// <param name="errorCallback">optional error callback. Fires when exception is thrown in loadingMethod</param>
        /// <returns></returns>
        public async Task<T> LoadAsync<T>(Func<Task<T>> loadingMethod, Action<T> resultCallback = null, Action<Exception> errorCallback = null)
        {
            //Set loading state
            LoadingState = LoadingStateType.Loading;

            T result = default(T);

            try
            {
                result = await loadingMethod();

                //Set finished state
                LoadingState = LoadingStateType.Finished;

                if (resultCallback != null)
                {
                    resultCallback(result);
                }

            }
            catch (Exception e)
            {
                //Set error state
                LoadingState = LoadingStateType.Error;

                if (errorCallback != null)
                {
                    errorCallback(e);
                }
                else if (!_catchExceptions) //swallow exception if catchexception is true
                {
                    throw; //throw error if no callback is defined
                }

            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadingMethod"></param>
        /// <param name="resultCallback"></param>
        /// <param name="errorCallback"></param>
        /// <returns></returns>
        public async Task LoadAsync(Func<Task> loadingMethod, Action resultCallback = null, Action<Exception> errorCallback = null)
        {
            //Set loading state
            LoadingState = LoadingStateType.Loading;
            
            try
            {
                await loadingMethod();

                //Set finished state
                LoadingState = LoadingStateType.Finished;

                if (resultCallback != null)
                {
                    resultCallback();
                }

            }
            catch (Exception e)
            {
                //Set error state
                LoadingState = LoadingStateType.Error;

                if (errorCallback != null)
                {
                    errorCallback(e);
                }
                else if (!_catchExceptions) //swallow exception if catchexception is true
                {
                    throw; //throw error if no callback is defined
                }

            }
        }

        /// <summary>
        /// First returns result callback with result from cache, then from refresh method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheLoadingMethod"></param>
        /// <param name="refreshLoadingMethod"></param>
        /// <param name="resultCallback"></param>
        /// <param name="errorCallback"></param>
        /// <returns></returns>
        public async Task LoadCacheThenRefreshAsync<T>(Func<Task<T>> cacheLoadingMethod, Func<Task<T>> refreshLoadingMethod, Action<T> resultCallback = null, Action<Exception> errorCallback = null)
        {
            //Set loading state
            LoadingState = LoadingStateType.Loading;

            T cacheResult = default(T);
            T refreshResult = default(T);

            try
            {
                cacheResult = await cacheLoadingMethod();

                if (resultCallback != null)
                {
                    resultCallback(cacheResult);
                }

                refreshResult = await refreshLoadingMethod();

                if (resultCallback != null)
                {
                    resultCallback(refreshResult);
                }

                //Set finished state
                LoadingState = LoadingStateType.Finished;

            }
            catch (Exception e)
            {
                //Set error state
                LoadingState = LoadingStateType.Error;

                if (errorCallback != null)
                {
                    errorCallback(e);
                }
                else if (!_catchExceptions) //swallow exception if catchexception is true
                {
                    throw; //throw error if no callback is defined
                }
            }

        }

        #endregion

        #region Properties
        /// <summary>
        /// Current loading state
        /// </summary>
        public LoadingStateType LoadingState
        {
            get { return _loadingState; }
            set
            {
                SetProperty(ref _loadingState, value);
                RaisePropertyChanged(() => IsBusy);
                RaisePropertyChanged(() => IsError);
                RaisePropertyChanged(() => IsFinished);
            }
        }

        /// <summary>
        /// Indicates LoadingState == LoadingState.Error
        /// </summary>
        public bool IsError
        {
            get
            {
#if DEBUG
                //Always return true for the designer, for easy blend support
                if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
                    return true;
#endif

                if (LoadingState == LoadingStateType.Error)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Indicates LoadingState == LoadingState.Loading
        /// </summary>
        public bool IsBusy
        {
            get
            {
#if DEBUG
                //Always return true for the designer, for easy blend support
                if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
                    return true;
#endif

                if (LoadingState == LoadingStateType.Loading)
                    return true;

                return false;
            }

        }

        /// <summary>
        /// Indicates LoadingState == LoadingState.Finished
        /// </summary>
        public bool IsFinished
        {
            get
            {
#if DEBUG
                //Always return true for the designer, for easy blend support
                if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
                    return true;
#endif
                if (LoadingState == LoadingStateType.Finished)
                    return true;

                return false;
            }

        }

        #endregion

    }
}
