﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media.Animation;

namespace Eve.LiveTiles
{
    /// <summary>
    /// A simple wrapper that handles the boilerplate activity of showing a UserControl as 
    /// your settings while providing some handy events to track any settings flyout being shown or hidden.
    /// </summary>
    public sealed class Flyout
    {
        private const int FLYOUT_WIDTH = 346;
        private Popup _popup;

        /// <summary>
        /// Shows the given control in the flyout.
        /// </summary>
        public void ShowFlyout(UserControl control)
        {
            _popup = new Popup();
            TransitionCollection collection = new TransitionCollection();
            collection.Add(new PopupThemeTransition());
            _popup.Transitions = collection;
            _popup.Opened += OnPopupOpened;
            _popup.Closed += OnPopupClosed;
            Window.Current.Activated += OnWindowActivated;
            _popup.IsLightDismissEnabled = true;
            _popup.Width = FLYOUT_WIDTH;
            _popup.Height = Window.Current.Bounds.Height;

            control.Width = FLYOUT_WIDTH;
            control.Height = Window.Current.Bounds.Height;

            // Add the proper animation for the panel.
            _popup.ChildTransitions = new TransitionCollection();
            _popup.ChildTransitions.Add(new PaneThemeTransition()
            {
                Edge = (SettingsPane.Edge == SettingsEdgeLocation.Right) ?
                       EdgeTransitionLocation.Right :
                       EdgeTransitionLocation.Left
            });
            
            _popup.Child = control;
            _popup.SetValue(Canvas.LeftProperty, SettingsPane.Edge == SettingsEdgeLocation.Right ? (Window.Current.Bounds.Width - FLYOUT_WIDTH) : 0);
            _popup.SetValue(Canvas.TopProperty, 0);
            _popup.IsOpen = true;
        }


        private void OnWindowActivated(object sender, Windows.UI.Core.WindowActivatedEventArgs e)
        {
            if (e.WindowActivationState == Windows.UI.Core.CoreWindowActivationState.Deactivated)
            {
                _popup.IsOpen = false;
            }
        }

        private void OnPopupClosed(object sender, object e)
        {
            Window.Current.Activated -= OnWindowActivated;
            OnSettingsClosed(EventArgs.Empty);
        }

        private void OnPopupOpened(object sender, object e)
        {
            OnSettingsOpened(EventArgs.Empty);
        }

        /// <summary>
        /// Raised to indicate settings flyout has been opened.
        /// </summary>
        public static event EventHandler SettingsOpened;
        private static void OnSettingsOpened(EventArgs args)
        {
            var handler = Flyout.SettingsOpened;
            if (handler != null)
            {
                handler(null, args);
            }
        }

        /// <summary>
        /// Raised to indicate settings flyout has been closed.
        /// </summary>
        public static event EventHandler SettingsClosed;
        private static void OnSettingsClosed(EventArgs args)
        {
            var handler = Flyout.SettingsClosed;
            if (handler != null)
            {
                handler(null, args);
            }
        }
    }
}
