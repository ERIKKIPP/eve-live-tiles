﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Common
{
    /// <summary>
    /// Database location
    /// </summary>
    public enum StorageLocation
    {
        /// <summary>
        /// Stored locally
        /// </summary>
        Local,
        /// <summary>
        /// Roams with the user
        /// </summary>
        Roaming,

        Temporary
    }
}
