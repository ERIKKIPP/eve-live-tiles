﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Windows.Storage;
using Windows.Storage.Streams;
using Eve.LiveTiles.Extensions;

namespace Eve.LiveTiles.Common
{
    public sealed class Storage<T>
    {
        #region Private Members

        #endregion

        #region CTOR
        public Storage()
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">value to save</param>
        /// <param name="storageName">name to save</param>
        /// <param name="storageLocation">local or roaming</param>
        /// <returns></returns>
        public async Task SaveAsync(T value, string storageName, StorageLocation storageLocation, string subFolder = null)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));

            using (MemoryStream data = new MemoryStream())
            {
                serializer.WriteObject(data, value);
                data.Seek(0, SeekOrigin.Begin);
                StorageFolder folder = await GetStorageFolder(storageLocation, subFolder).ConfigureAwait(false);
                StorageFile file = await folder.CreateFileAsync(storageName, CreationCollisionOption.ReplaceExisting).AsTask().ConfigureAwait(false);

                using (Stream fileStream = await file.OpenStreamForWriteAsync().ConfigureAwait(false))
                {
                    await data.CopyToAsync(fileStream).ConfigureAwait(false);
                    await fileStream.FlushAsync().ConfigureAwait(false);
                }
            }

        }

        /// <summary>
        /// will check if file exists
        /// </summary>
        /// <param name="storageName">name of the database to open</param>
        /// <param name="storageLocation">local or roaming</param>
        /// <returns></returns>
        public async Task<T> OpenAsync(string storageName, StorageLocation storageLocation, string subFolder = null)
        {
            StorageFolder folder = await GetStorageFolder(storageLocation, subFolder).ConfigureAwait(false);

            if (await folder.ContainsFileAsync(storageName).ConfigureAwait(false))
            {
                StorageFile file = await folder.GetFileAsync(storageName).AsTask().ConfigureAwait(false);
                using (IInputStream inStream = await file.OpenSequentialReadAsync().AsTask().ConfigureAwait(false))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                    return (T)serializer.ReadObject(inStream.AsStreamForRead());
                }
            }

            return default(T);
        }

        public async Task DeleteAsync(string storageName, StorageLocation storageLocation, string subFolder = null)
        {
            StorageFolder folder = await GetStorageFolder(storageLocation, subFolder).ConfigureAwait(false);

            if (await folder.ContainsFileAsync(storageName).ConfigureAwait(false))
            {
                var file = await folder.GetFileAsync(storageName).AsTask().ConfigureAwait(false);
                await file.DeleteAsync(StorageDeleteOption.PermanentDelete).AsTask().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// open file from app
        /// </summary>
        /// <param name="storageName"></param>
        /// <returns></returns>
        public async Task<T> OpenAsync(string storageName)
        {
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(storageName)).AsTask().ConfigureAwait(false);
            using (IInputStream inStream = await file.OpenSequentialReadAsync().AsTask().ConfigureAwait(false))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                return (T)serializer.ReadObject(inStream.AsStreamForRead());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storageLocation"></param>
        /// <param name="subFolder"></param>
        /// <returns></returns>
        private async static Task<StorageFolder> GetStorageFolder(StorageLocation storageLocation, string subFolder)
        {
            StorageFolder folder = null;

            switch (storageLocation)
            {
                case StorageLocation.Local:
                    folder = ApplicationData.Current.LocalFolder;
                    break;
                case StorageLocation.Roaming:
                    folder = ApplicationData.Current.RoamingFolder;
                    break;
                case StorageLocation.Temporary:
                    folder = ApplicationData.Current.TemporaryFolder;
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(subFolder))
            {
                folder = await folder.CreateFolderAsync(subFolder, CreationCollisionOption.OpenIfExists).AsTask().ConfigureAwait(false);
            }

            return folder;
        }
        #endregion




    }
}
