﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;

namespace Eve.LiveTiles.Common
{
    public static class IOUtility
    {
        /// <summary>
        /// from file should exists prior to calling this
        /// </summary>
        /// <param name="fromfileName"></param>
        /// <param name="from"></param>
        /// <param name="tofileName"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public async static Task CopyFile(string fromfileName, StorageLocation from, string tofileName, StorageLocation to)
        {
            StorageFile file = await GetStorageFile(fromfileName, from);

            //create new file
            StorageFile newFile = await CreateStorageFile(tofileName, to).ConfigureAwait(false);
            //copy file
            await file.CopyAndReplaceAsync(newFile).AsTask().ConfigureAwait(false);
        }

        private async static Task<StorageFile> GetStorageFile(string fileName, StorageLocation storageLocation)
        {
            switch (storageLocation)
            {
                case StorageLocation.Local:
                    return await ApplicationData.Current.LocalFolder.GetFileAsync(fileName).AsTask().ConfigureAwait(false);
                case StorageLocation.Roaming:
                    return await ApplicationData.Current.RoamingFolder.GetFileAsync(fileName).AsTask().ConfigureAwait(false);
                default:
                    break;
            }

            return null;
        }

        private async static Task<StorageFile> CreateStorageFile(string fileName, StorageLocation storageLocation)
        {
            switch (storageLocation)
            {
                case StorageLocation.Local:
                    return await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting).AsTask().ConfigureAwait(false);
                case StorageLocation.Roaming:
                    return await ApplicationData.Current.RoamingFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting).AsTask().ConfigureAwait(false);
                default:
                    break;
            }

            return null;
        }

    }
}
