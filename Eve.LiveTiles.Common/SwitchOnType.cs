﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Common
{
    /// <summary>
    ///  new SwitchOnType<T>()
    ///           .Case<int>(() =>
    ///           {
    ///           })
    ///           .Case<decimal>(() =>
    ///           {
    ///           })
    ///           .Case<string>(() =>
    ///           {
    ///           })
    ///           .Default<DateTime>(() =>
    ///           {
    ///           });
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    public sealed class SwitchOnType<T1>
    {
        private bool _break;

        public SwitchOnType()
        {

        }

        public SwitchOnType<T1> Case<T2>(Action action)
        {
            if (!_break)
            {
                if (typeof(T1) == typeof(T2))
                {
                    action();
                    _break = true;
                }
            }

            return this as SwitchOnType<T1>;
        }

        public void Default<T2>(Action action)
        {
            if (!_break)
            {
                if (typeof(T1) == typeof(T2))
                {
                    action();
                    _break = true;
                }
            }
        }
    }
}
