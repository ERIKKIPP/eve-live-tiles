﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace Eve.LiveTiles.Tasks
{
    public sealed class BackgroundTaskManager
    {
        #region Private Members
      
        #endregion

        #region CTOR
        private BackgroundTaskManager()
        {

        }
        #endregion

        #region Methods

        /// <summary>
        /// Register a background task with the specified taskEntryPoint, name, trigger,
        /// and condition (optional).
        /// </summary>
        /// <param name="taskEntryPoint">Task entry point for the background task.</param>
        /// <param name="name">A name for the background task.</param>
        /// <param name="trigger">The trigger for the background task.</param>
        /// <param name="condition">An optional conditional event that must be true for the task to fire.</param>
        public static IBackgroundTaskRegistration RegisterBackgroundTask(string taskEntryPoint, string name, IBackgroundTrigger trigger, IBackgroundCondition condition)
        {
            var builder = new BackgroundTaskBuilder();

            builder.Name = name;
            builder.TaskEntryPoint = taskEntryPoint;
            builder.SetTrigger(trigger);

            if (condition != null)
            {
                builder.AddCondition(condition);
            }

            return builder.Register();
        }
               

        /// <summary>
        /// Unregister background tasks with specified name.
        /// </summary>
        /// <param name="name">Name of the background task to unregister.</param>
        public static bool UnregisterBackgroundTasks(string name)
        {
            KeyValuePair<Guid, IBackgroundTaskRegistration> task = BackgroundTaskRegistration.AllTasks.Where(s => string.Equals(s.Value.Name, name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            if (task.Value != null)
            {
                task.Value.Unregister(true);
                return true;
            }

            return false;
        }

        /// <summary>
        /// check if the task exists
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool BackgroundTaskExists(string name)
        {
            KeyValuePair<Guid, IBackgroundTaskRegistration> task = BackgroundTaskRegistration.AllTasks.Where(s => string.Equals(s.Value.Name, name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            return task.Value != null;
        }

        /// <summary>
        /// check if the task exists
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IBackgroundTaskRegistration GetBackgroundTask(string name)
        {
            KeyValuePair<Guid, IBackgroundTaskRegistration> task = BackgroundTaskRegistration.AllTasks.Where(s => string.Equals(s.Value.Name, name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            return task.Value;
        }
        #endregion


    }
}
