﻿using Eve.LiveTiles.Common;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using NotificationsExtensions.TileContent;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI.Notifications;
using Eve.LiveTiles.Extensions;
using Eve.LiveTiles.Data;

namespace Eve.LiveTiles.Tasks
{
    public sealed class TileNotificationTask : IBackgroundTask
    {
        private volatile bool _cancelRequested;

        public TileNotificationTask()
        {
            LocatorService ls = new LocatorService();
        }

        private async Task RunTask()
        {
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Clear();
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();

            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();
            IList<EveAccount> accounts = await eve.LoadAccountsAsync();

            if (accounts != null && accounts.Count > 0)
            {
                var updater = TileUpdateManager.CreateTileUpdaterForApplication();
                updater.EnableNotificationQueue(true);

                foreach (var character in accounts.SelectMany(s => s.Characters).ToList())
                {
                    //get skill name
                    var query = from grp in await EveDataRepository.GetSkillTree()
                                where grp.Skills.Any(c => c.TypeID == character.SkillInTraining.TrainingTypeID)
                                select (grp.Skills.Where(s => s.TypeID == character.SkillInTraining.TrainingTypeID)).FirstOrDefault();
                    string skillName = string.Empty;

                    if (query != null)
                    {
                        Skill skill = query.FirstOrDefault();
                        if (skill != null)
                        {
                            skillName = skill.TypeName;
                        }
                    }

                    ITileWideSmallImageAndText02 tileContent = TileContentFactory.CreateTileWideSmallImageAndText02();
                    tileContent.TextHeading.Text = character.CharacterName;
                    tileContent.Image.Src = string.Format(CultureInfo.CurrentUICulture, "ms-appdata:///local/{0}.jpg", character.CharacterName);
                    tileContent.Image.Alt = character.CharacterName;
                    
                    TimeSpan ts = character.SkillInTraining.TrainingEndTime.Subtract(DateTime.Now);
                    string trainingToLevel = string.Format(CultureInfo.CurrentUICulture, "{0} ({1})", skillName, character.SkillInTraining.TrainingToLevel);
                    string trainingEndTime = string.Format(CultureInfo.CurrentUICulture, "{0}", character.SkillInTraining.TrainingEndTime);

                    tileContent.TextBody1.Text = string.Format(CultureInfo.CurrentUICulture, "{0:N}", character.CharacterInfo.SkillPoints);

                    if (character.SkillInTraining.SkillInTraining)
                    {
                        tileContent.TextBody2.Text = trainingToLevel;
                        tileContent.TextBody3.Text = ts.TimeSpanText();
                        tileContent.TextBody4.Text = trainingEndTime;
                    }

                    ITileSquareText01 squareTileContent = TileContentFactory.CreateTileSquareText01();
                    squareTileContent.TextHeading.Text = character.CharacterName;

                    if (character.SkillInTraining.SkillInTraining)
                    {
                        squareTileContent.TextBody1.Text = trainingToLevel;
                        squareTileContent.TextBody2.Text = ts.TimeSpanText();
                        squareTileContent.TextBody3.Text = trainingEndTime;
                    }
                    tileContent.SquareContent = squareTileContent;

                    TileNotification tileNotification = tileContent.CreateNotification();
                    updater.Update(tileNotification);
                }
            }
        }

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Associate a cancellation handler with the background task.
            taskInstance.Canceled += new BackgroundTaskCanceledEventHandler(OnCanceled);
            taskInstance.Progress = 1;

            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            try
            {
                if (!_cancelRequested)
                {
                    await RunTask();
                }
            }
            finally { deferral.Complete(); }
        }

        /// <summary>
        /// Handles background task cancellation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="reason"></param>
        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            // Indicate that the background task is canceled.
            _cancelRequested = true;
        }
    }
}
