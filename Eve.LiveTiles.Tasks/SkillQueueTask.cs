﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using NotificationsExtensions.ToastContent;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;
using Eve.LiveTiles.Extensions;

namespace Eve.LiveTiles.Tasks
{
    public sealed class SkillQueueTask : IBackgroundTask
    {
        private volatile bool _cancelRequested;

        public SkillQueueTask()
        {
            LocatorService ls = new LocatorService();
        }

        private async Task RunTask()
        {
            IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();
            IList<EveAccount> accounts = await eve.LoadAccountsAsync();

            if (accounts != null && accounts.Count > 0)
            {
                foreach (var character in accounts.SelectMany(s => s.Characters).ToList())
                {
                    SkillQueueResponse skill = character.SkillQueue;

                    if (skill != null && skill.SkillsInQueue != null)
                    {
                        TimeSpan total = TimeSpan.Zero;

                        foreach (var item in skill.SkillsInQueue)
                        {
                            TimeSpan ts = TimeSpan.Zero;

                            if (item.QueuePosition == 0)
                            {
                                ts = item.EndTime.Subtract(DateTime.Now);
                            }
                            else
                            {
                                ts = item.EndTime.Subtract(item.StartTime);
                            }

                            total += ts;
                        }

                        if (total.Days == 0 && total.Hours < 24)
                        {
                            HandleNotifications(character, total);
                        }
                    }
                }
            }
        }

        private void HandleNotifications(EveCharacter character, TimeSpan ts)
        {
            IToastNotificationContent toastContent = null;

            IToastImageAndText03 templateContent = ToastContentFactory.CreateToastImageAndText03();

            templateContent.TextHeadingWrap.Text = character.CharacterName;
            templateContent.TextBody.Text = ts.TimeSpanText();
            templateContent.Image.Src = string.Format(CultureInfo.CurrentUICulture, "ms-appdata:///local/{0}.jpg", character.CharacterName);
            templateContent.Image.Alt = character.CharacterName;
            toastContent = templateContent;
            toastContent.Duration = ToastDuration.Long;

            ToastNotification toast = toastContent.CreateNotification();
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Associate a cancellation handler with the background task.
            taskInstance.Canceled += new BackgroundTaskCanceledEventHandler(OnCanceled);
            taskInstance.Progress = 1;

            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            try
            {
                if (!_cancelRequested)
                {
                    await RunTask();
                }
            }
            finally { deferral.Complete(); }
        }

        /// <summary>
        /// Handles background task cancellation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="reason"></param>
        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            // Indicate that the background task is canceled.
            _cancelRequested = true;
        }
    }
}
