﻿using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace Eve.LiveTiles.Tasks
{
    public sealed class MaintenanceTask : IBackgroundTask
    {
        private volatile bool _cancelRequested;

        public MaintenanceTask()
        {
            LocatorService ls = new LocatorService();
        }

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Associate a cancellation handler with the background task.
            taskInstance.Canceled += new BackgroundTaskCanceledEventHandler(OnCanceled);
            taskInstance.Progress = 1;

            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            try
            {
                if (!_cancelRequested)
                {
                    //load accounts from storage
                    IEveDataSource eve = DependencyFactory.Resolve<IEveDataSource>();
                    IList<EveAccount> accounts = await eve.LoadAccountsAsync();

                    foreach (var item in accounts)
                    {
                        await eve.GetCharacterDataAsync(item, true);
                    }

                    await eve.SaveAccountsAsync(accounts);
                }
            }
            finally { deferral.Complete(); }

        }

        /// <summary>
        /// Handles background task cancellation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="reason"></param>
        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            // Indicate that the background task is canceled.
            _cancelRequested = true;
        }
    }
}
