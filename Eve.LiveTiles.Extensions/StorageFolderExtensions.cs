﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Eve.LiveTiles.Extensions
{
    public static class StorageFolderExtensions
    {
        public static async Task<bool> ContainsFileAsync(this StorageFolder folder, string fileName)
        {
            var files = await folder.GetFilesAsync().AsTask().ConfigureAwait(false);
            return files.Any(s => string.Equals(s.Name, fileName, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
