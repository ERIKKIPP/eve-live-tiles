﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Extensions
{
    public static class EnumerableExtensions
    {
        public static int IndexOf<T>(this IEnumerable<T> list, Predicate<T> condition)
        {
            System.Diagnostics.Contracts.Contract.Requires(list != null, "list can't be null");
            System.Diagnostics.Contracts.Contract.Requires(condition != null, "condition can't be null");

            int index = -1;
            return list.Any(item => { index++; return condition(item); }) ? index : -1;
        }

        /// <summary>
        /// var flattenedNodes = _itemsSource.Map(p => true, (InvCategoryModel n) => { return n.Children; });
        /// Traverses an object hierarchy and return a flattened list of elements based on a predicate.
        /// </summary>
        /// <typeparam name="TSource">The type of object in your collection.</typeparam>
        /// <param name="source">The collection of your topmost TSource objects.</param>
        /// <param name="selectorFunction">A predicate for choosing the objects you want.</param>
        /// <param name="getChildrenFunction">A function that fetches the child collection from an object.</param>
        /// <returns>A flattened list of objects which meet the criteria in selectorFunction.</returns>
        public static IEnumerable<TSource> Flatten<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> selectorFunction, Func<TSource, IEnumerable<TSource>> getChildrenFunction)
        {
            // Add what we have to the stack
            var flattenedList = source.Where(selectorFunction);

            // Go through the input enumerable looking for children,
            // and add those if we have them
            foreach (TSource element in source)
            {
                flattenedList = flattenedList.Concat(getChildrenFunction(element).Flatten(selectorFunction, getChildrenFunction));
            }

            return flattenedList;
        }
    }
}
