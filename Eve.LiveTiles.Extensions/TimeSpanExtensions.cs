﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Extensions
{
    public static class TimeSpanExtensions
    {
        public static int YearsTotal(this TimeSpan ts)
        {
            return (int)Math.Floor(ts.TotalDays / 365.25);
        }

        public static int MonthsTotal(this TimeSpan ts)
        {
            int years = YearsTotal(ts);
            int months = years * 12;

            return (int)Math.Floor(ts.TotalDays / (365.25 / 12)) - months;
        }

        public static int DaysTotal(this TimeSpan ts)
        {
            int months = (int)Math.Floor(ts.TotalDays / (365.25 / 12));
            int days = (int)Math.Floor(months * (365.25 / 12));
            return (int)Math.Floor(ts.TotalDays - days);
        }

        public static string TimeSpanTextNegative(this TimeSpan ts)
        {
            StringBuilder sb = new StringBuilder();

            if (ts.Days != 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}d ", ts.Days);
            }

            if (ts.Hours != 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}h ", ts.Hours);
            }

            if (ts.Minutes != 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}m ", ts.Minutes);
            }

            if (ts.Seconds != 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}s", ts.Seconds);
            }

            return sb.ToString();       
        }

        public static string TimeSpanText(this TimeSpan ts)
        {
            StringBuilder sb = new StringBuilder();

            if (ts.Days > 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}d ", ts.Days);
            }

            if (ts.Hours > 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}h ", ts.Hours);
            }

            if (ts.Minutes > 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}m ", ts.Minutes);
            }

            if (ts.Seconds > 0)
            {
                sb.AppendFormat(CultureInfo.CurrentUICulture, "{0:D1}s", ts.Seconds);
            }

            return sb.ToString();
        }
    }
}
