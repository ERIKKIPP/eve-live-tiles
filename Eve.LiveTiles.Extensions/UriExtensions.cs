﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;

namespace Eve.LiveTiles.Extensions
{
    public static class UriExtensions
    {
        public async static Task<IStorageFile> DownloadFile(this Uri uri, string fileName)
        {

            BackgroundDownloader downloader = new BackgroundDownloader();

            StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

            DownloadOperation download = downloader.CreateDownload(uri, file);

            DownloadOperation op = await download.StartAsync();

            return op.ResultFile;

        }
    }
}
