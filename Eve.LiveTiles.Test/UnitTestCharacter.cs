﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Test
{

    [TestClass]
    public class UnitTestCharacter
    {
        private IEveDataService _eveData;
        private const string KEYID = "1135931";
        private const string VCODE = "0gxIaVmXn47j8BE6UcbTVN1epFEJE31rlmW0ayeuho51a1KQC3yWMq4XeMsXI4we";
        private const string CHAR_ID = "859325177";

        public UnitTestCharacter()
        {

        }

        /// <summary>
        ///Initialize() is called once during test execution before
        ///test methods in this test class are executed.
        ///</summary>
        [TestInitialize()]
        public void Initialize()
        {
            _eveData = new EveDataService();
        }

        /// <summary>
        ///Cleanup() is called once during test execution after
        ///test methods in this class have executed unless
        ///this test class' Initialize() method throws an exception.
        ///</summary>
        [TestCleanup()]
        public void Cleanup()
        {

            //  TODO: Add test cleanup code
        }

        [TestMethod]
        public async Task GetCharacterSheetTestMethod()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            CharacterSheetResponse response = await _eveData.GetCharacterSheet(apiinfo, CHAR_ID, true) as CharacterSheetResponse;

            Assert.IsNotNull(response);

        }

        [TestMethod]
        public async Task GetCharacterInfoTestMethod()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            CharacterInfoResponse response = await _eveData.GetCharacterInfo(apiinfo, CHAR_ID, true) as CharacterInfoResponse;

            Assert.IsNotNull(response);

        }

        [TestMethod]
        public async Task GetCharacterSkillInTrainingTestMethod()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            SkillInTrainingResponse response = await _eveData.GetCharacterSkillInTraining(apiinfo, CHAR_ID, true) as SkillInTrainingResponse;

            Assert.IsNotNull(response);

        }

        [TestMethod]
        public async Task GetCharacterSkillQueueTestMethod()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            SkillQueueResponse response = await _eveData.GetCharacterSkillQueue(apiinfo, CHAR_ID, true) as SkillQueueResponse;

            Assert.IsNotNull(response);

            Assert.IsTrue(response.SkillsInQueue.Count > 0);

        }

        [TestMethod]
        public async Task GetCharacterNPCStandingsTestMethod()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            StandingsResponse response = await _eveData.GetCharacterNPCStandings(apiinfo, CHAR_ID, true) as StandingsResponse;

            Assert.IsNotNull(response);

        }

        [TestMethod]
        public async Task GetCharacterAssetsTestMethod()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            CharacterAssetResponse response = await _eveData.GetCharacterAssetList(apiinfo, CHAR_ID, true) as CharacterAssetResponse;

            Assert.IsNotNull(response);

        }

        [TestMethod]
        public async Task UpdateAccounts()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            for (int i = 0; i < 10; i++)
            {
                CharacterSheetResponse sheet = await _eveData.GetCharacterSheet(apiinfo, CHAR_ID, false) as CharacterSheetResponse;
                CharacterInfoResponse info = await _eveData.GetCharacterInfo(apiinfo, CHAR_ID, false) as CharacterInfoResponse;
                SkillInTrainingResponse skills = await _eveData.GetCharacterSkillInTraining(apiinfo, CHAR_ID, false) as SkillInTrainingResponse;
                SkillQueueResponse queue = await _eveData.GetCharacterSkillQueue(apiinfo, CHAR_ID, false) as SkillQueueResponse;
            }

            Assert.IsTrue(true);
        }
    }
}
