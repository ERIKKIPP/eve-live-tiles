﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.Service;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Test
{
    [TestClass]
    public class UnitTestStress
    {

        public UnitTestStress()
        {

        }

        /// <summary>
        ///Initialize() is called once during test execution before
        ///test methods in this test class are executed.
        ///</summary>
        [TestInitialize()]
        public void Initialize()
        {

        }

        /// <summary>
        ///Cleanup() is called once during test execution after
        ///test methods in this class have executed unless
        ///this test class' Initialize() method throws an exception.
        ///</summary>
        [TestCleanup()]
        public void Cleanup()
        {

            //  TODO: Add test cleanup code
        }

        [TestMethod]
        public async Task StressTestMethod()
        {

            //await Task.Factory.StartNew(async () =>
            //{

            //});

            IEveDataService eveData = DependencyFactory.Resolve<IEveDataService>();

            SkillTreeResponse skilltree = await eveData.GetSkillTree(false) as SkillTreeResponse;

            ServerStatusResponse response = await eveData.GetServerStatus(false) as ServerStatusResponse;

            //await CacheManager.Add("", "SkillTree", DateTime.Now.AddHours(4));

            //bool b = false;

            //for (int i = 0; i < 1000; i++)
            //{
            //    b = await CacheManager.Exists("SkillTree");
            //    b = await CacheManager.Exists("ServerStatus");
            //}





            Assert.IsNotNull(response);
        }
    }
}
