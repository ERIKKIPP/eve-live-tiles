﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.DataModel;
using Eve.LiveTiles.Service;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Test
{

    [TestClass]
    public class UnitTestStorage
    {
        private const string KEY = "1135931-CharacterAssetList";

        public UnitTestStorage()
        {

        }

        /// <summary>
        ///Initialize() is called once during test execution before
        ///test methods in this test class are executed.
        ///</summary>
        [TestInitialize()]
        public void Initialize()
        {

        }

        /// <summary>
        ///Cleanup() is called once during test execution after
        ///test methods in this class have executed unless
        ///this test class' Initialize() method throws an exception.
        ///</summary>
        [TestCleanup()]
        public void Cleanup()
        {

            //  TODO: Add test cleanup code
        }

        [TestMethod]
        public async Task CacheManagerTestMethod()
        {
            await CacheManager.Add("werwerewfvsfsdewrewfsdsdvwewewerwdwffew", KEY, DateTime.Now.AddHours(1));

            string s = string.Empty;

            for (int i = 0; i < 1000; i++)
            {
                s = await CacheManager.Get(KEY);
            }

            Assert.IsNotNull(s);

        }

        [TestMethod]
        public async Task EveDataTestMethod()
        {

            await EveDataRepository.GetAllInvTypes();

            await EveDataRepository.GetAllCrtCategories();
            await EveDataRepository.GetAllCrtCertificates();
            await EveDataRepository.GetAllCrtClasses();
            await EveDataRepository.GetAllInvCategories();
            await EveDataRepository.GetAllInvFlags();
            await EveDataRepository.GetAllInvGroups();
            await EveDataRepository.GetAllInvMarketGroups();
            await EveDataRepository.GetAllStaStations();


            Assert.IsTrue(true);

        }

        [TestMethod]
        public async Task HighSlotsTestMethod()
        {

            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<InvMarketGroup> mktGrp = await EveDataRepository.GetAllInvMarketGroups();
            IList<DgmEffect> dgmeffects = await EveDataRepository.GetAllDgmEffects();
            IList<DgmTypeEffect> dgmtypeeffects = await EveDataRepository.GetAllDgmTypeEffects();
            IList<InvGroup> invgroups = await EveDataRepository.GetAllInvGroups();
            IList<DgmTypeAttribute> dgmTypes = await EveDataRepository.GetAllDgmTypeAttributes();

            var query = (from itemcat in invTypes
                         join dgmtypeeffect in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect.TypeID
                         where itemcat.Published.GetValueOrDefault()
                         select new
                         {
                             EffectID = dgmtypeeffect.EffectID,
                             TypeID = dgmtypeeffect.TypeID

                         }).GroupBy(s => s.TypeID).Where(s => s.Any(x => x.EffectID == 12));

            IEnumerable<IGrouping<InvType, TestInvEffectModel>> query2 = (from itemcat in invTypes
                                                                          join dgmtypeeffect in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect.TypeID
                                                                          join dgmeffect in dgmeffects on dgmtypeeffect.EffectID equals dgmeffect.EffectID
                                                                          where itemcat.Published.GetValueOrDefault()
                                                                          select new TestInvEffectModel
                                                                         {
                                                                             Effect = dgmeffect,
                                                                             Item = itemcat

                                                                         }).GroupBy(s => s.Item).Where(s => s.Any(x => x.Effect.EffectID == 12));

            var query3 = (from itemcat in invTypes
                          join dgmtypeeffect in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect.TypeID
                          join dgmeffect in dgmeffects on dgmtypeeffect.EffectID equals dgmeffect.EffectID
                          where itemcat.Published.GetValueOrDefault()
                          select new TestInvEffectModel
                          {
                              Effect = dgmeffect,
                              Item = itemcat

                          }).GroupBy(s => s.Item.GroupID).Where(s => s.Any(x => x.Effect.EffectID == 12));



            var query4 = (from itemcat in invTypes
                          join dgmtypeeffect in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect.TypeID
                          join dgmeffect in dgmeffects on dgmtypeeffect.EffectID equals dgmeffect.EffectID
                          where itemcat.Published.GetValueOrDefault() && dgmeffect.EffectID == 12
                          select itemcat).GroupBy(s => s.GroupID);



            var query5 = (from itemcat in invTypes
                          join dgmtypeeffect in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect.TypeID
                          join dgmeffect in dgmeffects on dgmtypeeffect.EffectID equals dgmeffect.EffectID
                          join invgroup in invgroups on itemcat.GroupID equals invgroup.GroupID
                          join types in dgmTypes on itemcat.TypeID equals types.TypeID into cpuandpowerTypes
                          where itemcat.Published.GetValueOrDefault() && dgmeffect.EffectID == (int)12 //&&
                          //IsModFittable(types, model.Resources)
                          select new
                          {
                              TypeName = itemcat.TypeName,
                              GroupName = invgroup.GroupName,
                              Description = itemcat.Description,
                              Stuff = cpuandpowerTypes
                              //Cpu = GetValue(types, AttributeIdType.Cpu),
                              //Power = GetValue(types, AttributeIdType.Power)
                          }).GroupBy(s => s.GroupName);

            var menu = CreateHierarchySlots(mktGrp, query2, (int)MarketGroupType.ShipEquipment);


            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task EffectsQueryTestMethod()
        {
            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<InvMarketGroup> mktGrp = await EveDataRepository.GetAllInvMarketGroups();
            IList<DgmEffect> dgmeffects = await EveDataRepository.GetAllDgmEffects();
            IList<DgmTypeEffect> dgmtypeeffects = await EveDataRepository.GetAllDgmTypeEffects();


            var query = from itemcat in invTypes
                        join dgmtypeeffect in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect.TypeID
                        join dgmeffect in dgmeffects on dgmtypeeffect.EffectID equals dgmeffect.EffectID
                        join dgmtypeeffect2 in dgmtypeeffects on itemcat.TypeID equals dgmtypeeffect2.TypeID
                        join mkt in mktGrp on itemcat.MarketGroupID equals mkt.MarketGroupID
                        where itemcat.Published.GetValueOrDefault() && dgmtypeeffect2.EffectID == (int)DgmEffectIdType.HiPower
                        select new TestInvEffectModel
                        {
                            Item = itemcat,
                            Effect = dgmeffect

                        };

            foreach (var model in CreateHierarchy(mktGrp, query, 9).Where(s => s.MarketGroupID == 938 || s.MarketGroupID == 656 || s.MarketGroupID == 779 || s.MarketGroupID == 141))
            {
                System.Diagnostics.Debug.WriteLine(model.MarketGroupName);


                var children = (from mkt in model.Inventory
                                where mkt.Key.MarketGroupID == model.MarketGroupID
                                select new TestInvCategoryModel
                                {
                                    Description = mkt.Key.Description,
                                    MarketGroupID = mkt.Key.MarketGroupID,
                                    MarketGroupName = mkt.Key.TypeName,
                                    ParentGroupID = model.MarketGroupID,//this is where we want to go back to
                                    Item = mkt.Key

                                }).OrderBy(s => s.MarketGroupName);
            }






            Assert.IsTrue(true);
        }

        private IEnumerable<TestInvCategoryModel> CreateHierarchySlots(IEnumerable<InvMarketGroup> allItems, IEnumerable<IGrouping<InvType, TestInvEffectModel>> invTypes, int? parentId)
        {
            IEnumerable<TestInvCategoryModel> children = (from mkt in allItems
                                                          join inventory in invTypes on mkt.MarketGroupID equals inventory.Key.MarketGroupID into groups
                                                          where mkt.ParentGroupID == parentId
                                                          select new TestInvCategoryModel
                                                          {
                                                              Description = mkt.Description,
                                                              MarketGroupID = mkt.MarketGroupID,
                                                              ParentGroupID = mkt.ParentGroupID,
                                                              MarketGroupName = mkt.MarketGroupName,
                                                              Inventory = groups.ToList()//.GroupBy(s => s.Key).ToList()

                                                          }).OrderBy(s => s.MarketGroupName);

            foreach (var item in children)
            {
                yield return new TestInvCategoryModel()
                {
                    Children = CreateHierarchySlots(allItems, invTypes, item.MarketGroupID),
                    MarketGroupID = item.MarketGroupID,
                    ParentGroupID = item.ParentGroupID,
                    Description = item.Description,
                    MarketGroupName = item.MarketGroupName,
                    Inventory = item.Inventory
                };
            }
        }

        private IEnumerable<TestInvCategoryModel> CreateHierarchy(IEnumerable<InvMarketGroup> allItems, IEnumerable<TestInvEffectModel> invTypes, int? parentId)
        {
            IEnumerable<TestInvCategoryModel> children = (from mkt in allItems
                                                          join inventory in invTypes on mkt.MarketGroupID equals inventory.Item.MarketGroupID into groups
                                                          where mkt.ParentGroupID == parentId
                                                          select new TestInvCategoryModel
                                                          {
                                                              Description = mkt.Description,
                                                              MarketGroupID = mkt.MarketGroupID,
                                                              ParentGroupID = mkt.ParentGroupID,
                                                              MarketGroupName = mkt.MarketGroupName,
                                                              Inventory = groups.GroupBy(s => s.Item).ToList()

                                                          }).OrderBy(s => s.MarketGroupName);

            foreach (var item in children)
            {
                yield return new TestInvCategoryModel()
                {
                    Children = CreateHierarchy(allItems, invTypes, item.MarketGroupID),
                    MarketGroupID = item.MarketGroupID,
                    ParentGroupID = item.ParentGroupID,
                    Description = item.Description,
                    MarketGroupName = item.MarketGroupName,
                    Inventory = item.Inventory
                };
            }
        }

        private IEnumerable<DgmEffect> PopulateEffects(int typeId, IList<DgmEffect> dgmeffects, IList<DgmTypeEffect> dgmtypeeffects)
        {
            return from dgmtypeeffect in dgmtypeeffects
                   join dgmeffect in dgmeffects on dgmtypeeffect.EffectID equals dgmeffect.EffectID
                   join dgmtypeeffect2 in dgmtypeeffects on typeId equals dgmtypeeffect2.TypeID
                   where dgmtypeeffect2.EffectID == (int)DgmEffectIdType.HiPower
                   select dgmeffect;
        }

        [TestMethod]
        public async Task EveCategoryTestMethod()
        {

            IList<InvType> invTypes = await EveDataRepository.GetAllInvTypes();
            IList<InvGroup> invgroups = await EveDataRepository.GetAllInvGroups();
            IList<InvCategory> invcats = await EveDataRepository.GetAllInvCategories();

            var query = from inv in invTypes
                        join invgroup in invgroups on inv.GroupID equals invgroup.GroupID
                        join invcat in invcats on invgroup.CategoryID equals invcat.CategoryID
                        where inv.Published.GetValueOrDefault() && ItemCategories(invcat.CategoryID)
                        select new
                        {
                            InvType = inv,
                            InvGroup = invgroup,
                            InvCategory = invcat

                        };

            Assert.IsTrue(true);

        }

        public static Func<int?, bool> ItemCategories
        {
            get
            {
                return (categoryID) => categoryID == (int)InventoryCategoryType.Module || categoryID == (int)InventoryCategoryType.Charge || categoryID == (int)InventoryCategoryType.Drone
                    || categoryID == (int)InventoryCategoryType.Implant || categoryID == (int)InventoryCategoryType.Subsystem;
            }
        }

        [TestMethod]
        public async Task AttributesListTestMethod()
        {
            await EveDataRepository.GetSkillTree();

            var query = (from primary in EveDataRepository.SkillTree.SelectMany(s => s.Skills)
                         where primary.PrimaryAttribute != AttributeType.None || primary.SecondaryAttribute != AttributeType.None
                         select new
                         {
                             PrimaryAttribute = primary.PrimaryAttribute,
                             SecondaryAttribute = primary.SecondaryAttribute
                         }).Distinct().OrderBy(s => s.PrimaryAttribute);


            var query1 = from att in query
                          select new AttributeFilterModelTest
                         {
                             PrimaryAttribute = att.PrimaryAttribute,
                             SecondaryAttribute = att.SecondaryAttribute
                         };


            Enum.GetNames(typeof(AttributeType)).Where(s => (AttributeType)Enum.Parse(typeof(AttributeType), s) != AttributeType.None)
                .OrderBy(x => x)
                .SelectMany(primaryAttribute => EveDataRepository.SkillTree.SelectMany(s => s.Skills).Where(x => x.PrimaryAttribute == (AttributeType)Enum.Parse(typeof(AttributeType), primaryAttribute))
                    .Select(x => x.SecondaryAttribute)
                    .Distinct()
                    .OrderBy(x => Enum.GetName(typeof(AttributeType), x)).Select(secondaryAttribute => string.Format(CultureInfo.InvariantCulture, "{0} - {1}", primaryAttribute, secondaryAttribute))
                      );

            Assert.IsTrue(true);
        }
    }

    internal sealed class AttributeFilterModelTest
    {
        public AttributeFilterModelTest()
        {

        }

        public AttributeType PrimaryAttribute { get; set; }

        public AttributeType SecondaryAttribute { get; set; }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}-{1}", PrimaryAttribute, SecondaryAttribute);
        }
    }

    internal sealed class TestInvEffectModel
    {
        public TestInvEffectModel()
        {

        }

        public InvType Item { get; set; }

        public DgmEffect Effect { get; set; }
    }



    internal sealed class TestInvCategoryModel
    {
        public TestInvCategoryModel()
        {
            Inventory = new List<IGrouping<InvType, TestInvEffectModel>>();
            Children = Enumerable.Empty<TestInvCategoryModel>();
        }

        public int? MarketGroupID { get; set; }

        public int? ParentGroupID { get; set; }

        public string MarketGroupName { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// when populated from inventory I need this to display the data when selected
        /// </summary>
        public IList<IGrouping<InvType, TestInvEffectModel>> Inventory { get; set; }

        public InvType Item { get; set; }

        public IEnumerable<TestInvCategoryModel> Children { get; set; }

        public IEnumerable<DgmEffect> Effects { get; set; }
    }

}
