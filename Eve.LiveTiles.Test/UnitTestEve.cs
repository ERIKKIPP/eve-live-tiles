﻿using Eve.LiveTiles.Data;
using Eve.LiveTiles.Service;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Test
{
    [TestClass]
    public sealed class UnitTestEve
    {
        private IEveDataService _eveData;
    
        /// <summary>
        ///Initialize() is called once during test execution before
        ///test methods in this test class are executed.
        ///</summary>
        [TestInitialize()]
        public void Initialize()
        {
            _eveData = new EveDataService();
        }

        /// <summary>
        ///Cleanup() is called once during test execution after
        ///test methods in this class have executed unless
        ///this test class' Initialize() method throws an exception.
        ///</summary>
        [TestCleanup()]
        public void Cleanup()
        {

            //  TODO: Add test cleanup code
        }


        [TestMethod]
        public async Task GetSkillTreeTestMethod()
        {

            SkillTreeResponse response = await _eveData.GetSkillTree(true) as SkillTreeResponse;

            Assert.IsNotNull(response);

        }
    }
}
