﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Eve.LiveTiles.Service;
using Eve.LiveTiles.Data;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Test
{
    [TestClass]
    public class UnitTestAccount
    {
        private IEveDataService _eveData;
        private const string KEYID = "1420573";
        private const string VCODE = "OlaMa72A1jvBE3Xx5tSMQgH7EiAcof1M99z0nglvm1ze4sEKO8M9lPZp7sFqxUf6";

        public UnitTestAccount()
        {
            
        }

        /// <summary>
        ///Initialize() is called once during test execution before
        ///test methods in this test class are executed.
        ///</summary>
        [TestInitialize()]
        public void Initialize()
        {
            _eveData = new EveDataService();
        }

        /// <summary>
        ///Cleanup() is called once during test execution after
        ///test methods in this class have executed unless
        ///this test class' Initialize() method throws an exception.
        ///</summary>
        [TestCleanup()]
        public void Cleanup()
        {

            //  TODO: Add test cleanup code
        }

        [TestMethod]
        public async Task GetKeyInfoTestMethod()
        {
            ApiKey apiinfo = new ApiKey() { KeyId = KEYID, VerificationCode = VCODE };

            KeyInfoResponse response = await _eveData.GetKeyInfo(apiinfo, true) as KeyInfoResponse;

            Assert.IsNotNull(response);
           
        }
    }
}
