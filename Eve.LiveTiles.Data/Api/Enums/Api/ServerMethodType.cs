﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    public enum ServerMethodType
    {
        /// <summary>
        /// The Server Status API returns the current Tranquility status and the number of players online. 
        /// </summary>
        [API(BaseUrl = "https://api.eveonline.com/server/ServerStatus.xml.aspx/")]
        ServerStatus
    }
}
