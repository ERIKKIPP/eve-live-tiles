﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    public enum MiscMethodType
    {
        /// <summary>
        /// Corporation/Alliance Logo 	This part of the API allows you to fetch the character portrait 
        /// as well as corporation and alliance logos from the ID. The old character portrait server is 
        /// still functional, but will eventually be shut down in favor of this service. The filename 
        /// for the character portraits are of a general structure <characterID>_<size>.jpg whereas 
        /// the corporation and alliance logos are stored as PNGs. 
        /// Parameters 	allianceID, corporationID, or characterID 
        /// </summary>
        [API(BaseUrl = "http://image.eveonline.com/corporation/", PostData = "{0}_256.png")]
        CorporationImage,

        [API(BaseUrl = "http://image.eveonline.com/character/", PostData = "{0}_256.jpg")]
        CharacterImage,

        [API(BaseUrl = "http://image.eveonline.com/alliance/", PostData = "{0}_256.png")]
        AllianceImage
    }
}
