﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [Flags]
    public enum CorporateMethodType
    {
        None = 0,

        [DisplayAttribute(Description = "Current balance of all corporation accounts.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.AccountMarket)]
        AccountBalance = 1,

        [DisplayAttribute(Description = "List of all corporation assets.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation)]
        AssetList = 2,

        [DisplayAttribute(Description = "List of medals awarded to corporation members.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.CorporationMembers)]
        MemberMedals = 4,

        [DisplayAttribute(Description = "Exposes basic 'Show Info' information as well as Member Limit and basic division and wallet info.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation)]
        CorporationSheet = 8,

        [DisplayAttribute(Description = "Corporate contact list and relationships.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications)]
        ContactList = 16,

        [DisplayAttribute(Description = "Corporate secure container acess log.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation)]
        ContainerLog = 32,

        [DisplayAttribute(Description = "Corporations Factional Warfare Statistics.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PublicInformation)]
        FacWarStats = 64,

        [DisplayAttribute(Description = "Corporation jobs, completed and active.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.ScienceIndustry)]
        IndustryJobs = 128,

        [DisplayAttribute(Description = "Corporation kill log.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PublicInformation)]
        KillLog = 256,

        [DisplayAttribute(Description = "Member roles and titles.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.CorporationMembers)]
        MemberSecurity = 512,

        [DisplayAttribute(Description = "Member role and title change log.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.CorporationMembers)]
        MemberSecurityLog = 1024,

        [DisplayAttribute(Description = "Limited Member information.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.CorporationMembers)]
        MemberTrackingLimited = 2048,

        [DisplayAttribute(Description = "List of all corporate market orders.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.AccountMarket)]
        MarketOrders = 4096,

        [DisplayAttribute(Description = "List of all medals created by the corporation.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PublicInformation)]
        Medals = 8192,

        [DisplayAttribute(Description = "List of all outposts controlled by the corporation.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.OutpostsStarbases)]
        OutpostList = 16384,

        [DisplayAttribute(Description = "List of all service settings of corporate outposts.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.OutpostsStarbases)]
        OutpostServiceDetail = 32768,

        [DisplayAttribute(Description = "Shareholders of the corporation.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.AccountMarket)]
        Shareholders = 65536,

        [DisplayAttribute(Description = "List of all settings of corporate starbases.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.OutpostsStarbases)]
        StarbaseDetail = 131072,

        [DisplayAttribute(Description = "NPC Standings towards corporation.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PublicInformation)]
        Standings = 262144,

        [DisplayAttribute(Description = "List of all corporate starbases.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.OutpostsStarbases)]
        StarbaseList = 524288,

        [DisplayAttribute(Description = "Wallet journal for all corporate accounts.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.AccountMarket)]
        WalletJournal = 1048576,

        [DisplayAttribute(Description = "Market transactions of all corporate accounts.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.AccountMarket)]
        WalletTransactions = 2097152,

        [DisplayAttribute(Description = "Titles of corporation and the roles they grant.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.CorporationMembers)]
        Titles = 4194304,

        [DisplayAttribute(Description = "List of recent Contracts the corporation is involved in.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation)]
        Contracts = 8388608,

        [DisplayAttribute(Description = "Allows the fetching of coordinate and name data for items owned by the corporation.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation)]
        Locations = 16777216,

        [DisplayAttribute(Description = "Extensive Member information. Time of last logoff, last known location and ship.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.CorporationMembers)]
        MemberTrackingExtended = 33554432

    }
}
