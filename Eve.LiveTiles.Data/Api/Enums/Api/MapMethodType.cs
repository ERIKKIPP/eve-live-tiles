﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    public enum MapMethodType
    {
        /// <summary>
        /// The Factional Warfare System activity map contains a list of solar systems, 
        /// the name of the occupying faction (if applicable) and whether the solar system is contested. 
        /// This data can be used to generate an activity map of the EVE universe for Factional Warfare. 
        /// Parameters 	none 
        /// </summary>
        FactionalWarfareSystems,

        /// <summary>
        /// The Jumps API retrieves the number of jumps in each solar system, but does not include 0-jump systems.
        /// Parameters 	none 
        /// </summary>
        Jumps,

        /// <summary>
        ///	The Kills API retrieves the number of ship, faction and pod kills organized by solar system.
        ///	Parameters 	none 
        /// </summary>
        Kills,

        /// <summary>
        /// The Sovereignty API provides an overview of all solar systems with a sovereignty, 
        /// the sovereignty level and the name and ID of the owning faction. 
        /// Parameters 	none 
        /// </summary>
        Sovereignty,
    }
}
