﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [Flags]
    public enum AccountMethodType
    {

        /// <summary>
        /// Returns information about the API Key provided, like the access mask, characters it gives access to and the type of it
        /// Parameters keyID, vCode OR keyID, apiKey 
        /// </summary>
        [DisplayAttribute(Description = "Returns information about the API Key provided.")]
        [API(BaseUrl = "https://api.eveonline.com/account/APIKeyInfo.xml.aspx", PostData = "?keyID={0}&vCode={1}")]
        APIKeyInfo,

        /// <summary>
        /// Retrieves character information for the user ID and API key provided. 
        /// Parameters keyID, vCode OR keyID, apiKey 
        /// </summary>
        [DisplayAttribute(Description = "Retrieves character information for the user ID and API key provided.")]
        [API(BaseUrl = "https://api.eveonline.com/account/Characters.xml.aspx", PostData = "?keyID={0}&vCode={1}")]
        Characters
    }
}

