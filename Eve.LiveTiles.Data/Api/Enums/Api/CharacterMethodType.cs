﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [Flags]
    public enum CharacterMethodType
    {
        None = 0,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Current balance of characters wallet.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.AccountMarket)]
        AccountBalance = 1,

        /// <summary>
        /// Parameters 	keyID, vCode, characterID OR userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Entire asset list of character.")]
        [API(BaseUrl = "https://api.eveonline.com/char/AssetList.xml.aspx", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        AssetList = 2,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID, eventIDs
        /// </summary>
        [DisplayAttribute(Description = "Event attendee responses. Requires UpcomingCalendarEvents to function.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation)]
        CalendarEventAttendees = 4,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID or keyID, vCode, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Character Sheet information. Contains basic'Show Info'information along with clones, account balance, implants, attributes, skills, certificates and corporation roles.")]
        [API(BaseUrl = "https://api.eveonline.com/char/CharacterSheet.xml.aspx", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        CharacterSheet = 8,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "List of character contacts and relationship levels.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        ContactList = 16,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Most recent contact notifications for the character.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        ContactNotifications = 32,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID (only calculated during daily downtime on TQ) 
        /// </summary>
        [DisplayAttribute(Description = "Characters Factional Warfare Statistics.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PublicInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        FacWarStats = 64,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Character jobs, completed and active.")]
        [API(BaseUrl = "http://api.eve-online.com/char/IndustryJobs.xml.aspx", CallGroupId = CallGroupType.ScienceIndustry, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        IndustryJobs = 128,

        /// <summary>
        /// Parameters 	userID, apiKey, beforeKillID, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Characters kill log.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PublicInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        KillLog = 256,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID, ids (comma-delimited list of mail Ids) 
        /// </summary>
        [DisplayAttribute(Description = "EVE Mail bodies. Requires MailMessages as well to function.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        MailBodies = 512,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "List of all Mailing Lists the character subscribes to.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        MailingLists = 1024,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "List of all messages in the characters EVE Mail Inbox.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        MailMessages = 2048,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "List of all Market Orders the character has made.")]
        [API(BaseUrl = "https://api.eveonline.com/char/MarketOrders.xml.aspx", CallGroupId = CallGroupType.AccountMarket, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        MarketOrders = 4096,

        /// <summary>
        /// userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Medals awarded to the character.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PublicInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        Medals = 8192,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "List of recent notifications sent to the character.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        Notifications = 16384,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID, IDs 
        /// </summary>
        [DisplayAttribute(Description = "Actual body of notifications sent to the character. Requires Notification access to function.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.Communications, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        NotificationTexts = 32768,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "List of all Research agents working for the character and the progress of the research.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.ScienceIndustry, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        Research = 65536,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Skill currently in training on the character. Subset of entire Skill Queue.")]
        [API(BaseUrl = "https://api.eveonline.com/char/SkillInTraining.xml.aspx", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        SkillInTraining = 131072,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Entire skill queue of character.")]
        [API(BaseUrl = "https://api.eveonline.com/char/SkillQueue.xml.aspx", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        SkillQueue = 262144,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "NPC Standings towards the character.")]
        [API(BaseUrl = "https://api.eveonline.com/char/Standings.xml.aspx", CallGroupId = CallGroupType.PublicInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        Standings = 524288,

        /// <summary>
        /// Parameters 	userID, apiKey, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Upcoming events on characters calendar.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        UpcomingCalendarEvents = 1048576,

        /// <summary>
        /// Parameters 	keyID, vcode, characterID, (rowCount) 
        /// </summary>
        [DisplayAttribute(Description = "Wallet journal of character.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.AccountMarket, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        WalletJournal = 2097152,

        /// <summary>
        /// Parameters 	keyID, vcode, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Market transaction journal of character.")]
        [API(BaseUrl = "https://api.eveonline.com", CallGroupId = CallGroupType.AccountMarket, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        WalletTransactions = 4194304,

        [DisplayAttribute(Description = "Character information, exposes skill points and current ship information on top of'Show Info'information, and depending on accessMask Sensitive Character Information, exposes account balance and last known location on top of the other Character Information call.")]
        [API(BaseUrl = "https://api.eveonline.com/eve/CharacterInfo.xml.aspx", CallGroupId = CallGroupType.PublicInformation | CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        CharacterInfo = 8388608 | 16777216,

        /// <summary>
        /// Parameters 	keyID, vcode, characterID 
        /// </summary>
        [DisplayAttribute(Description = "EVE player account status.")]
        [API(BaseUrl = "https://api.eveonline.com/account/AccountStatus.xml.aspx", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        AccountStatus = 33554432,

        /// <summary>
        /// Parameters 	keyID, vcode, characterID 
        /// </summary>
        [DisplayAttribute(Description = "List of all Contracts the character is involved in.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        Contracts = 67108864,

        /// <summary>
        /// Parameters 	keyID, vcode, characterID 
        /// </summary>
        [DisplayAttribute(Description = "Allows the fetching of coordinate and name data for items owned by the character.")]
        [API(BaseUrl = "https://api.eveonline.com/", CallGroupId = CallGroupType.PrivateInformation, PostData = "?keyID={0}&vCode={1}&characterID={2}")]
        Locations = 134217728,

    }
}
