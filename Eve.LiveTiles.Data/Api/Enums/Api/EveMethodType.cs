﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [Flags]
    public enum EveMethodType
    {

        /// <summary>
        /// The Alliance List API retrieves a complete list of all alliances in EVE Online.
        /// Parameters version
        /// </summary>
        AllianceList,

        /// <summary>
        /// The Certificate Tree API returns a list of certificates in EVE. 
        /// Parameters 	none 
        /// </summary>
        CertificateTree,

        /// <summary>
        /// The Character ID API returns a list of character IDs to match a specified comma-delimited list of character names. 
        /// Parameters 	names (comma delimited list) 
        /// </summary>
        CharacterID,

        /// <summary>
        /// The Character Name API returns a list of character names to match a specified comma-delimited list of character IDs. 
        /// Parameters 	IDs (comma delimited list) 
        /// </summary>
        CharacterName,

        /// <summary>
        /// The Conquerable Station List API retrieves a list of all conquerable stations
        /// and outposts and the location information for each. The name and ID of current holding corporation is also indicated in each row. 
        /// Parameters 	none 
        /// </summary>
        [DisplayAttribute(Description = "The Conquerable Station List API retrieves a list of all conquerable stations.")]
        [API(BaseUrl = "https://api.eveonline.com/eve/ConquerableStationList.xml.aspx", CallGroupId = CallGroupType.OutpostsStarbases)]
        ConquerableStationList,

        /// <summary>
        /// The Error List API returns a complete list of API errors.
        /// Error types can be separated into four main categories according to the first digit in the three-digit code:
        /// 1xx - user input error
        /// 2xx - authentication error
        /// 3xx - server error
        /// 9xx - miscellaneous
        /// Parameters 	none
        /// </summary>
        ErrorList,

        /// <summary>
        /// The FacWarStats API retrieves some general statistics pertaining to Factional Warfare, 
        /// which is part of the Empyrean Age expansion of EVE-Online. Included in these statistics 
        /// are information about kills and victory points awarded. This information is further 
        /// subdivided according to factions, as shown in the image below. 
        /// Parameters 	none 
        /// </summary>
        FactionalWarfareStatistics,

        /// <summary>
        /// The FW Top Statistics API retrieves kill and victory point information 
        /// for top characters, corporations and factions (as shown in the image below). 
        /// The kills section of the returned XML has been expanded in the image below for 
        /// characters and corporations to show the information nested within. Likewise, 
        /// victory points have been expanded for factions to show the nested XML. 
        /// Parameters 	none 
        /// </summary>
        FactionalWarfareTopStats,

        /// <summary>
        /// The Ref Types API retrieves a complete list of wallet transaction types, which are used in journal entries. 
        /// Parameters 	none 
        /// </summary>
        ReferenceTypes,

        /// <summary>
        /// The Skill Tree API retrieves a list of current in-game skills. 
        /// The Skill Tree is organized into skill groups and each group contains 
        /// a number of skills. Non-published skills, like the Polaris skill, are not listed.
        /// Parameters 	none 
        /// </summary>
        [DisplayAttribute(Description = "Current in-game skills.")]
        [API(BaseUrl = "https://api.eveonline.com/eve/SkillTree.xml.aspx")]
        SkillTree,

        /// <summary>
        /// Returns the name associated with a typeID.
        /// </summary>
        TypeName
    }
}
