﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [Flags]
    public enum CallGroupType
    {
        None = 0,

        AccountMarket = 1,

        ScienceIndustry = 2,

        PrivateInformation = 4,

        PublicInformation = 8,

        CorporationMembers = 16,

        OutpostsStarbases = 32,

        Communications = 64
    }
}
