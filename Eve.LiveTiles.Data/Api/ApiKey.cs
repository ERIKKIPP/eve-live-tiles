﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    /// <summary>
    /// info for https://support.eveonline.com/api/
    /// </summary>
    public sealed class ApiKey
    {
        /// <summary>
        /// 
        /// </summary>
        public ApiKey()
        {

        }
                        
        public string KeyId { get;  set; }

        public string VerificationCode { get; set; }       
    }
}
