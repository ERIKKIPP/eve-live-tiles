﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Storage;
using Windows.Storage.Streams;
using Eve.LiveTiles.Extensions;
using System.Net.Http;


namespace Eve.LiveTiles.Data
{
    /// <summary>
    /// </summary>
    public sealed class EveDataService : IEveDataService
    {

        #region Private Members

        #endregion

        #region CTOR
        public EveDataService()
        {


        }
        #endregion

        #region Private Methods
        private async static Task<XDocument> GetXDocument(string url, string postData, string cacheKey, bool forceUpdate)
        {
            if (!forceUpdate)
            {
                string xml = await CacheManager.Get(cacheKey).ConfigureAwait(false);

                if (!string.IsNullOrEmpty(xml))
                {
                    using (StringReader sr = new StringReader(xml))
                    {
                        return XDocument.Load(sr);
                    }
                }

            }

            return await GetXDocument(url, postData, cacheKey).ConfigureAwait(false);
        }

        private async static Task<XDocument> GetXDocument(string url, string postData, string cacheKey)
        {
            XDocument doc = null;

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    using (Stream stream = await httpClient.GetStreamAsync(string.Format(CultureInfo.InvariantCulture, "{0}{1}", url, postData)).ConfigureAwait(false))
                    {
                        doc = XDocument.Load(stream);
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParse(doc.Element("eveapi").Element("cachedUntil").Value, out dt))
                        {
                            DateTime convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);

                            await CacheManager.Add(doc.ToString(), cacheKey, convertedDate.ToLocalTime()).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (HttpRequestException)
            {

            }
            catch (ArgumentNullException)
            {

            }
            catch (Exception)
            {
            }

            return doc;
        }

        private async static Task<XDocument> GetXDocument(string url, string cacheKey, bool forceUpdate)
        {
            return await GetXDocument(url, null, cacheKey, forceUpdate).ConfigureAwait(false);
        }
        #endregion

        #region IEveDataService

        public async Task<Response> GetKeyInfo(ApiKey accountInfo, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-KeyInfo", accountInfo.KeyId);
            APIAttribute att = AccountMethodType.APIKeyInfo.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new KeyInfoResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterSheet(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterSheet", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.CharacterSheet.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new CharacterSheetResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterInfo(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterInfo", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.CharacterInfo.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new CharacterInfoResponse(doc);//populate object
        }

        public async Task<Response> GetAccountStatus(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-AccountStatus", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.AccountStatus.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new AccountStatusResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterAccountBalance(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterAssetList(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterAssetList", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.AssetList.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new CharacterAssetResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterCalendarEventAttendees(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterContactList(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterContactNotifications(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterFactionalWarfareStatics(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterIndustryJobs(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterIndustryJobs", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.IndustryJobs.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new CharacterIndustryJobsResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterKillLog(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterMailingLists(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterMailBodies(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterMailMessages(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterMarketOrders(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterMarketOrders", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.MarketOrders.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new CharacterMarketOrdersResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterMedals(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterNotificationTexts(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterNotifications(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterNPCStandings(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterNPCStandings", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.Standings.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new StandingsResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterResearch(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterSkillInTraining(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterSkillInTraining", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.SkillInTraining.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new SkillInTrainingResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterSkillQueue(ApiKey accountInfo, string id, bool forceUpdate)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}-CharacterSkillQueue", accountInfo.KeyId);
            APIAttribute att = CharacterMethodType.SkillQueue.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, accountInfo.KeyId, accountInfo.VerificationCode, id);
            XDocument doc = await GetXDocument(att.BaseUrl, postData, key, forceUpdate).ConfigureAwait(false);

            return new SkillQueueResponse(doc);//populate object
        }

        public async Task<Response> GetCharacterUpcomingCalendarEvents(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterWalletJournal(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public async Task<Response> GetCharacterWalletTransactions(ApiKey accountInfo, bool forceUpdate)
        {
            XDocument doc = await GetXDocument("", "", "", forceUpdate).ConfigureAwait(false);
            throw new NotImplementedException();
        }

        public Uri GetPortrait(MiscMethodType imageType, string id)
        {
            APIAttribute att = imageType.GetAttribute<APIAttribute>();
            string postData = string.Format(CultureInfo.InvariantCulture, att.PostData, id);

            return new Uri(string.Format(CultureInfo.InvariantCulture, "{0}{1}", att.BaseUrl, postData));
        }

        public async Task<Response> GetServerStatus(bool forceUpdate)
        {
            APIAttribute att = ServerMethodType.ServerStatus.GetAttribute<APIAttribute>();
            XDocument doc = await GetXDocument(att.BaseUrl, "ServerStatus", forceUpdate).ConfigureAwait(false);

            return new ServerStatusResponse(doc);//populate object
        }

        public async Task<Response> GetSkillTree(bool forceUpdate)
        {
            APIAttribute att = EveMethodType.SkillTree.GetAttribute<APIAttribute>();
            XDocument doc = await GetXDocument(att.BaseUrl, "SkillTree", forceUpdate).ConfigureAwait(false);

            return new SkillTreeResponse(doc);//populate object
        }

        public async Task<Response> GetConquerableStationList(bool forceUpdate)
        {
            APIAttribute att = EveMethodType.ConquerableStationList.GetAttribute<APIAttribute>();
            XDocument doc = await GetXDocument(att.BaseUrl, "ConquerableStationList", forceUpdate).ConfigureAwait(false);

            return new ConquerableStationListResponse(doc);//populate object
        }
        #endregion



    }
}
