﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class Character
    {
        public Character()
        {

        }

        [DataMember]
        public string CharacterID { get; set; }

        [DataMember]
        public string CharacterName { get; set; }

        [DataMember]
        public string CorporationID { get; set; }

        [DataMember]
        public string CorporationName { get; set; }
    }
}
