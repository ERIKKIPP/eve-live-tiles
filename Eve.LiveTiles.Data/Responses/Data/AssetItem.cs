﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class AssetItem
    {
        public AssetItem()
        {

        }

        /// <summary>
        /// Unique ID for the item. Only guaranteed to be unique for the page load as itemIDs are recycled over time.
        /// </summary>
        [DataMember]
        public long ItemID { get; set; }

        /// <summary>
        /// Reference to a solar system or a station. Not used for items within items.
        /// </summary>
        [DataMember]
        public long LocationID { get; set; }

        /// <summary>
        /// The type of item. This references to the invTypes table.
        /// </summary>
        [DataMember]
        public int TypeID { get; set; }

        /// <summary>
        /// How many of this item is in the stack.
        /// </summary>
        [DataMember]
        public long Quantity { get; set; }

        /// <summary>
        /// Used to differentiate between hanger divisions, drone bays, fitting locations and similiar.
        /// </summary>
        [DataMember]
        public int Flag { get; set; }

        /// <summary>
        /// Indicates if the item is a singleton.
        /// </summary>
        [DataMember]
        public bool Singleton { get; set; }

        [DataMember]
        public int RawQuantity { get; set; }

        public IList<AssetItem> Contents { get; set; }
    }
}
