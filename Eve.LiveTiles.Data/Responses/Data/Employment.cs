﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class Employment
    {
        public Employment()
        {

        }

        [DataMember]
        public string RecordID { get; set; }

        [DataMember]
        public string CorporationID { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }
    }
}
