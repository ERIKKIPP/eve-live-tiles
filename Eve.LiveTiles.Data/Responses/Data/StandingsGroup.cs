﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class StandingsGroup
    {
        public StandingsGroup()
        {
            StandingsList = new List<Standings>();
        }

        [DataMember]
        public string Name { get; set; }

        public IList<Standings> StandingsList { get; set; }
    }
}
