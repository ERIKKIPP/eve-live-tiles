﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class ConquerableStation
    {
        public ConquerableStation()
        {

        }

        [DataMember]
        public long StationID { get; set; }

        [DataMember]
        public string StationName { get; set; }

        [DataMember]
        public int StationTypeID { get; set; }

        [DataMember]
        public int SolarSystemID { get; set; }

        [DataMember]
        public int CorporationID { get; set; }

        [DataMember]
        public string CorporationName { get; set; }
    }
}
