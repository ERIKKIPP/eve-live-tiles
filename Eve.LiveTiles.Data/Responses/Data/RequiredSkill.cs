﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class RequiredSkill
    {
        public RequiredSkill()
        {

        }

        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public int SkillLevel { get; set; }
    }
}
