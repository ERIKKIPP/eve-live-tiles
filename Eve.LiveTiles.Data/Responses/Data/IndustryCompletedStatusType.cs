﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    public enum IndustryCompletedStatusType
    {
        Failed = 0,
        Delivered = 1,
        Aborted = 2,
        GMAborted = 3,
        InflightUnanchored = 4,
        Destroyed = 5
    }
}
