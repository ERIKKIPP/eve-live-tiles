﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class SkillQueue
    {
        public SkillQueue()
        {

        }

        [DataMember]
        public int QueuePosition { get; set; }

        [DataMember]
        public int TypeID { get; set; }

        [DataMember]
        public int Level { get; set; }

        [DataMember]
        public int StartSP { get; set; }

        [DataMember]
        public int EndSP { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }
    }
}
