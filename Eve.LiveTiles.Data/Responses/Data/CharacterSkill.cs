﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CharacterSkill : SkillBase
    {
        public CharacterSkill()
        {

        }

        [DataMember]
        public int Skillpoints { get; set; }

        [DataMember]
        public int Level { get; set; }
    }
}
