﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class AttributeEnhancer
    {
        public AttributeEnhancer()
        {

        }

        [DataMember]
        public AttributeType Enhancer { get; set; }

        [DataMember]
        public string AugmentatorName { get; set; }

        [DataMember]
        public int AugmentatorValue { get; set; }
    }
}
