﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    public sealed class Skill : SkillBase
    {
        public Skill()
        {
            RequiredSkills = new List<RequiredSkill>();
            SkillBonusCollection = new List<SkillBonus>();
        }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public int GroupID { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Rank { get; set; }

        [DataMember]
        public IList<RequiredSkill> RequiredSkills { get; set; }

        [DataMember]
        public AttributeType PrimaryAttribute { get; set; }

        [DataMember]
        public AttributeType SecondaryAttribute { get; set; }

        [DataMember]
        public IList<SkillBonus> SkillBonusCollection { get; set; }
    }
}
