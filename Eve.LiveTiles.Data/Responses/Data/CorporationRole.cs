﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CorporationRole
    {
        public CorporationRole()
        {

        }

        [DataMember]
        public int RoleID { get; set; }

        [DataMember]
        public string RoleName { get; set; }
    }
}
