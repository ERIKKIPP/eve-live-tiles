﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class IndustryJob
    {
        public IndustryJob()
        {

        }

        /// <summary>
        /// Unique ID for this job. Subject to the same renumbering as journal entries.
        /// </summary>
        [DataMember]
        public int JobID { get; set; }

        /// <summary>
        /// ID of the assembly line this job is installed in. IDs for lines in stations don't change, but repackaged assembly arrays, repackaged mobile labs, and redeployed Rorquals will get new assemblyLineIDs.
        /// </summary>
        [DataMember]
        public int AssemblyLineID { get; set; }

        /// <summary>
        /// If installed in a station, this is the stationID in the staStations Table. If installed at a POS, this will be 0. See containerTypeID for POS module (i.e. Mobile Laboratory).
        /// </summary>
        [DataMember]
        public long ContainerID { get; set; }

        /// <summary>
        /// Blueprint itemID. See asset list.
        /// </summary>
        [DataMember]
        public long InstalledItemID { get; set; }

        /// <summary>
        /// ID for the location from which the blueprint was installed. Office or POS module. See asset list.
        /// </summary>
        [DataMember]
        public int InstalledItemLocationID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int InstalledItemQuantity { get; set; }

        /// <summary>
        /// Starting PL of blueprint.
        /// </summary>
        [DataMember]
        public int InstalledItemProductivityLevel { get; set; }

        /// <summary>
        /// Starting ML of blueprint.
        /// </summary>
        [DataMember]
        public int InstalledItemMaterialLevel { get; set; }

        /// <summary>
        /// Starting number of runs remaining. (-1 for a BPO)
        /// </summary>
        [DataMember]
        public int InstalledItemLicensedProductionRunsRemaining { get; set; }

        /// <summary>
        /// Destination hanger for product (built item when manufacturing, BPC when copying or inventing).
        /// </summary>
        [DataMember]
        public long OutputLocationID { get; set; }

        /// <summary>
        /// ID of character who started this job.
        /// </summary>
        [DataMember]
        public int InstallerID { get; set; }

        /// <summary>
        /// Number of runs for this job (when making copies, number of BPCs to make).
        /// </summary>
        [DataMember]
        public int Runs { get; set; }

        /// <summary>
        /// Number of runs on output BPCs for copying and inventing.
        /// </summary>
        [DataMember]
        public int LicensedProductionRuns { get; set; }

        /// <summary>
        /// ID for the solar system this job was installed in. See mapSolarSystems table.
        /// </summary>
        [DataMember]
        public int InstalledInSolarSystemID { get; set; }

        /// <summary>
        /// Container of the container. Seems to generally be the solar system ID.
        /// </summary>
        [DataMember]
        public int ContainerLocationID { get; set; }

        /// <summary>
        /// Modifier for amount of materials required over standard BPO/C listing, as effected by installation location (i.e. Rapid Assembly Arrays have a modifier of 1.2, resulting in 20% extra material usage)
        /// </summary>
        [DataMember]
        public decimal MaterialMultiplier { get; set; }

        /// <summary>
        /// Effect character's skills & implants have
        /// </summary>
        [DataMember]
        public decimal CharMaterialMultiplier { get; set; }

        /// <summary>
        /// Effect of installation - ie, an advanced mobile lab as a timeMultiplier of 0.65 when copying.
        /// </summary>
        [DataMember]
        public decimal TimeMultiplier { get; set; }

        /// <summary>
        /// Speed of research/invention/production, as reduced by individual character skills.
        /// </summary>
        [DataMember]
        public decimal CharTimeMultiplier { get; set; }

        /// <summary>
        /// TypeID of blueprint. See tables invTypes and invBlueprintTypes|invBlueprintTypes.
        /// </summary>
        [DataMember]
        public int InstalledItemTypeID { get; set; }

        /// <summary>
        /// TypeID of product. This refers to what's been built, what's being copied, or what's being invented. See invTypes table.
        /// </summary>
        [DataMember]
        public int OutputTypeID { get; set; }

        /// <summary>
        /// TypeID of container, such as station, mobile lab, or assembly array. Again, see invTypes table.
        /// </summary>
        [DataMember]
        public int ContainerTypeID { get; set; }

        /// <summary>
        /// 0 if the blueprint is an original, 1 if it is a copy.
        /// </summary>
        [DataMember]
        public bool InstalledItemCopy { get; set; }

        /// <summary>
        /// 1 if the job has been delivered, 0 if not.
        /// </summary>
        [DataMember]
        public bool Completed { get; set; }

        /// <summary>
        /// Always 0?
        /// </summary>
        [DataMember]
        public bool CompletedSuccessfully { get; set; }

        [DataMember]
        public int InstalledItemFlag { get; set; }

        [DataMember]
        public int OutputFlag { get; set; }

        /// <summary>
        /// Activity ID of this job
        /// </summary>
        [DataMember]
        public int ActivityID { get; set; }

        /// <summary>
        /// 0 = failed
        ///1 = delivered
        ///2 = aborted
        ///3 = GM aborted
        ///4 = inflight unanchored
        ///5 = destroyed
        ///If it's not ready yet, completed = 0 and completedStatus is irrelevant/uninitialized/probably 0
        ///If complete = 1 and status = 0, then it failed
        /// </summary>
        [DataMember]
        public IndustryCompletedStatusType CompletedStatus { get; set; }

        /// <summary>
        /// When this job was installed.
        /// </summary>
        [DataMember]
        public DateTime InstallTime { get; set; }

        /// <summary>
        /// When this job was started (after waiting in line or rounding to the next minute, for example).
        /// </summary>
        [DataMember]
        public DateTime BeginProductionTime { get; set; }

        /// <summary>
        /// When this job will finish or was finished. (Not when it was delivered.)
        /// </summary>
        [DataMember]
        public DateTime EndProductionTime { get; set; }

        /// <summary>
        /// Normally "0001-01-01 00:00:00". If the job was installed into a POS module and that module went offline, this is when that module went offline. 
        /// The S&I window in-game calculates the difference between this and endProductionTime to show time remaining (in red, and not counting down). 
        /// When the module is back up, this is reset to "0001-01-01 00:00:00" and the endProductionTime is updated to reflect the delay.
        /// </summary>
        [DataMember]
        public DateTime PauseProductionTime { get; set; }
    }
}
