﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class Standings
    {
        public Standings()
        {

        }

        [DataMember]
        public string FromName { get; set; }

        [DataMember]
        public decimal Standing { get; set; }
    }
}
