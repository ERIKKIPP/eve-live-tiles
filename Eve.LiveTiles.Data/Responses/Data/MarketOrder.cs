﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    /// <summary>
    /// (if Bid = 0 and OrderState = 0) = current sell orders
    /// (if Bid = 0 and OrderState = 2) = recently sold
    /// (if Bid = 1 and OrderState = 0) = current buy orders
    /// (if Bid = 1 and OrderState = 2) = recently bought
    /// </summary>
    [DataContract]
    public sealed class MarketOrder
    {

        public MarketOrder()
        {

        }

        /// <summary>
        /// Unique order ID for this order. Note that these are not guaranteed to be unique forever, 
        /// they can recycle. But they are unique for the purpose of one data pull.
        /// </summary>
        [DataMember]
        public int OrderID { get; set; }

        /// <summary>
        /// ID of the character that physically placed this order.
        /// </summary>
        [DataMember]
        public string CharID { get; set; }

        /// <summary>
        /// ID of the station the order was placed in.
        /// </summary>
        [DataMember]
        public int StationID { get; set; }

        /// <summary>
        /// Quantity of items required/offered to begin with.
        /// </summary>
        [DataMember]
        public int VolEntered { get; set; }

        /// <summary>
        /// Quantity of items still for sale or still desired.
        /// </summary>
        [DataMember]
        public int VolRemaining { get; set; }

        /// <summary>
        /// For bids (buy orders), the minimum quantity that must be sold in one sale in order to be accepted by this order.
        /// </summary>
        [DataMember]
        public int MinVolume { get; set; }

        /// <summary>
        /// Valid states: 0 = open/active, 1 = closed, 2 = expired (or fulfilled), 3 = cancelled, 4 = pending, 5 = character deleted. 
        /// Note: As of Incarna 1.0, the API will no longer return orders which have an orderState other than 0. 
        /// Thus, no non-open orders will be returned. 
        /// As of 2011-09-08 MarketOrders.xml.aspx will now return all active orders plus all orders issued in the last 7 days. 
        /// An optional "orderID" parameter can be provided to fetch any order belonging to your character/corporation.
        /// </summary>
        [DataMember]
        public OrderStateType OrderState { get; set; }

        /// <summary>
        /// ID of the type (references the invTypes table) of the items this order is buying/selling.
        /// </summary>
        [DataMember]
        public int TypeID { get; set; }

        /// <summary>
        /// The range this order is good for. 
        /// For sell orders, this is always 32767. 
        /// For buy orders, allowed values are: -1 = station, 0 = solar system, 
        /// 5/10/20/40 Jumps, 32767 = region.
        /// </summary>
        [DataMember]
        public int Range { get; set; }

        /// <summary>
        /// Which division this order is using as its account. Always 1000 for characters, but in the range 1000 to 1006 for corporations.
        /// </summary>
        [DataMember]
        public string AccountKey { get; set; }
        
        /// <summary>
        /// How many days this order is good for. Expiration is issued + duration in days.
        /// </summary>
        [DataMember]
        public int Duration { get; set; }

        /// <summary>
        /// How much ISK is in escrow. Valid for buy orders only (I believe).
        /// </summary>
        [DataMember]
        public decimal Escrow { get; set; }

        /// <summary>
        /// The cost per unit for this order.
        /// </summary>
        [DataMember]
        public decimal Price { get; set; }

        /// <summary>
        /// If true, this order is a bid (buy order). Else, sell order.
        /// </summary>
        [DataMember]
        public bool Bid { get; set; }

        /// <summary>
        /// When this order was issued.
        /// </summary>
        [DataMember]
        public DateTime Issued { get; set; }
    }
}
