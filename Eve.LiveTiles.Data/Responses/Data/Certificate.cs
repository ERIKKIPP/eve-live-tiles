﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class Certificate
    {
        public Certificate()
        {

        }

        [DataMember]
        public int CertificateID { get; set; }

        [DataMember]
        public string CertificateName { get; set; }
    }
}
