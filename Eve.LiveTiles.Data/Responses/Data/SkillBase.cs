﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public abstract class SkillBase
    {
        public SkillBase()
        {

        }

        [DataMember]
        public int TypeID { get; set; }
        
        [DataMember]
        public bool Published { get; set; }
    }
}
