﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CorporationTitle
    {
        public CorporationTitle()
        {

        }

        [DataMember]
        public int TitleID { get; set; }

        [DataMember]
        public string TitleName { get; set; }
    }
}
