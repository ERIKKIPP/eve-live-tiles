﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CharacterAttribute
    {
        public CharacterAttribute()
        {

        }

        [DataMember]
        public int Value { get; set; }

        [DataMember]
        public AttributeType Attribute { get; set; }
    }
}
