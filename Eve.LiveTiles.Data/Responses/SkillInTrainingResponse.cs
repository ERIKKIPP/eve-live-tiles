﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class SkillInTrainingResponse : Response
    {
        public SkillInTrainingResponse()
        {

        }

        public SkillInTrainingResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(System.Xml.Linq.XDocument doc)
        {
            DateTime dt = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("currentTQTime"));
            DateTime convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            CurrentTQTime = convertedDate.ToLocalTime();

            dt = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("trainingStartTime"));
            convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            TrainingStartTime = convertedDate.ToLocalTime();

            dt = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("trainingEndTime"));
            convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            TrainingEndTime = convertedDate.ToLocalTime();

            TrainingTypeID = GetValue<int>(doc.Element("eveapi").Element("result").Element("trainingTypeID"));
            TrainingStartSP = GetValue<int>(doc.Element("eveapi").Element("result").Element("trainingStartSP"));
            TrainingDestinationSP = GetValue<int>(doc.Element("eveapi").Element("result").Element("trainingDestinationSP"));
            TrainingToLevel = GetValue<int>(doc.Element("eveapi").Element("result").Element("trainingToLevel"));

            int x = GetValue<int>(doc.Element("eveapi").Element("result").Element("skillInTraining"));
            SkillInTraining = x == 1 ? true : false;
        }

        [DataMember]
        public DateTime CurrentTQTime { get; set; }

        [DataMember]
        public DateTime TrainingEndTime { get; set; }

        [DataMember]
        public DateTime TrainingStartTime { get; set; }

        [DataMember]
        public int TrainingTypeID { get; set; }

        [DataMember]
        public int TrainingStartSP { get; set; }

        [DataMember]
        public int TrainingDestinationSP { get; set; }

        [DataMember]
        public int TrainingToLevel { get; set; }

        [DataMember]
        public bool SkillInTraining { get; set; }

    }
}
