﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CharacterIndustryJobsResponse : Response
    {
        public CharacterIndustryJobsResponse()
        {

        }

        public CharacterIndustryJobsResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            IndustryJobs = new List<IndustryJob>();
            var rows = from c in doc.Descendants("result").Elements("rowset")
                       select c;

            foreach (var parent in rows.Elements())
            {
                IndustryJob item = new IndustryJob
                {
                    ActivityID = GetValue<int>(parent.Attribute("activityID")),
                    AssemblyLineID = GetValue<int>(parent.Attribute("assemblyLineID")),
                    BeginProductionTime = GetValue<DateTime>(parent.Attribute("beginProductionTime")),
                    CharMaterialMultiplier = GetValue<decimal>(parent.Attribute("charMaterialMultiplier")),
                    CharTimeMultiplier = GetValue<decimal>(parent.Attribute("charTimeMultiplier")),
                    CompletedStatus = (IndustryCompletedStatusType)GetValue<int>(parent.Attribute("completedStatus")),
                    ContainerID = GetValue<long>(parent.Attribute("containerID")),
                    ContainerLocationID = GetValue<int>(parent.Attribute("containerLocationID")),
                    ContainerTypeID = GetValue<int>(parent.Attribute("containerTypeID")),
                    EndProductionTime = GetValue<DateTime>(parent.Attribute("endProductionTime")),
                    InstalledInSolarSystemID = GetValue<int>(parent.Attribute("installedInSolarSystemID")),
                    InstalledItemFlag = GetValue<int>(parent.Attribute("installedItemFlag")),
                    InstalledItemID = GetValue<long>(parent.Attribute("installedItemID")),
                    InstalledItemLicensedProductionRunsRemaining = GetValue<int>(parent.Attribute("installedItemLicensedProductionRunsRemaining")),
                    InstalledItemLocationID = GetValue<int>(parent.Attribute("installedItemLocationID")),
                    InstalledItemMaterialLevel = GetValue<int>(parent.Attribute("installedItemMaterialLevel")),
                    InstalledItemProductivityLevel = GetValue<int>(parent.Attribute("installedItemProductivityLevel")),
                    InstalledItemQuantity = GetValue<int>(parent.Attribute("installedItemQuantity")),
                    InstalledItemTypeID = GetValue<int>(parent.Attribute("installedItemTypeID")),
                    InstallerID = GetValue<int>(parent.Attribute("installerID")),
                    InstallTime = GetValue<DateTime>(parent.Attribute("installTime")),
                    JobID = GetValue<int>(parent.Attribute("jobID")),
                    LicensedProductionRuns = GetValue<int>(parent.Attribute("licensedProductionRuns")),
                    MaterialMultiplier = GetValue<decimal>(parent.Attribute("materialMultiplier")),
                    OutputFlag = GetValue<int>(parent.Attribute("outputFlag")),
                    OutputLocationID = GetValue<long>(parent.Attribute("outputLocationID")),
                    OutputTypeID = GetValue<int>(parent.Attribute("outputTypeID")),
                    PauseProductionTime = GetValue<DateTime>(parent.Attribute("pauseProductionTime")),
                    Runs = GetValue<int>(parent.Attribute("runs")),
                    TimeMultiplier = GetValue<decimal>(parent.Attribute("timeMultiplier"))
                };

                int x = GetValue<int>(parent.Attribute("completed"));
                item.Completed = x == 1 ? true : false;

                x = GetValue<int>(parent.Attribute("completedSuccessfully"));
                item.CompletedSuccessfully = x == 1 ? true : false;

                x = GetValue<int>(parent.Attribute("installedItemCopy"));
                item.InstalledItemCopy = x == 1 ? true : false;

                IndustryJobs.Add(item);
            }

        }

        [DataMember]
        public IList<IndustryJob> IndustryJobs { get; set; }

    }
}
