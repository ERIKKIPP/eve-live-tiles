﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class SkillQueueResponse : Response
    {
        public SkillQueueResponse()
        {

        }

        public SkillQueueResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(System.Xml.Linq.XDocument doc)
        {
            SkillsInQueue = new List<SkillQueue>();
            IEnumerable<XElement> skills = (from c in doc.Descendants("result").Elements("rowset").Elements("row")
                                            select c);

            foreach (var item in skills)
            {
                SkillQueue skill = new SkillQueue();

                DateTime dt = GetValue<DateTime>(item.Attribute("startTime"));
                DateTime convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                skill.StartTime = convertedDate.ToLocalTime();

                dt = GetValue<DateTime>(item.Attribute("endTime"));
                convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                skill.EndTime = convertedDate.ToLocalTime();

                skill.QueuePosition = GetValue<int>(item.Attribute("queuePosition"));
                skill.TypeID = GetValue<int>(item.Attribute("typeID"));
                skill.Level = GetValue<int>(item.Attribute("level"));
                skill.StartSP = GetValue<int>(item.Attribute("startSP"));
                skill.EndSP = GetValue<int>(item.Attribute("endSP"));

                SkillsInQueue.Add(skill);
            }

        }

        [DataMember]
        public IList<SkillQueue> SkillsInQueue { get; set; }
    }
}
