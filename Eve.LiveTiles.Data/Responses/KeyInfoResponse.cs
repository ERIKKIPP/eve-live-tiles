﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class KeyInfoResponse : Response
    {
        public KeyInfoResponse()
        {

        }

        public KeyInfoResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        [DataMember]
        public int AccessMask { get; set; }

        [DataMember]
        public KeyType Type { get; set; }

        [DataMember]
        public DateTime? ExpirationDate { get; set; }

        [DataMember]
        public IList<Character> Characters { get; private set; }

        protected override void LoadXml(XDocument doc)
        {
            Characters = new List<Character>();

            IEnumerable<XElement> chars = (from c in doc.Descendants("result").Elements("key").Elements("rowset").Elements("row")
                                           select c);

            XElement key = doc.Element("eveapi").Element("result").Element("key");
            AccessMask = GetValue<int>(key.Attribute("accessMask"));
            Type = (KeyType)Enum.Parse(typeof(KeyType), key.Attribute("type").Value);

            DateTime dt = GetValue<DateTime>(key.Attribute("expires"));
            DateTime convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            ExpirationDate = convertedDate.ToLocalTime();

            foreach (var item in chars)
            {
                Characters.Add(new Character()
                {
                    CharacterID = GetValue<string>(item.Attribute("characterID")),
                    CharacterName = GetValue<string>(item.Attribute("characterName")),
                    CorporationID = GetValue<string>(item.Attribute("corporationID")),
                    CorporationName = GetValue<string>(item.Attribute("corporationName"))
                });
            }
        }
    }
}
