﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CharacterAssetResponse : Response
    {
        public CharacterAssetResponse()
        {

        }

        public CharacterAssetResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            AssetList = new List<AssetItem>();
            var rows = (from c in doc.Descendants("result").Elements("rowset")
                        where string.Equals(c.Attribute("name").Value, "assets")
                        select c);

            foreach (var parent in rows.Elements())
            {
                AssetList.Add(AddAsset(parent));
            }
        }

        private AssetItem AddAsset(XElement element)
        {
            AssetItem asset = new AssetItem();

            asset.ItemID = GetValue<long>(element.Attribute("itemID"));
            asset.LocationID = GetValue<long>(element.Attribute("locationID"));
            asset.TypeID = GetValue<int>(element.Attribute("typeID"));
            asset.Quantity = GetValue<long>(element.Attribute("quantity"));
            asset.Flag = GetValue<int>(element.Attribute("flag"));
            asset.RawQuantity = GetValue<int>(element.Attribute("rawQuantity"));

            int x = GetValue<int>(element.Attribute("singleton"));
            asset.Singleton = x == 0 ? false : true;

            var rows = (from c in element.Elements("rowset")
                        where string.Equals(c.Attribute("name").Value, "contents")
                        select c);

            if (rows != null)
            {
                asset.Contents = new List<AssetItem>();

                foreach (var parent in rows.Elements())
                {
                    asset.Contents.Add(AddAsset(parent));
                }
            }

            return asset;
        }

        [DataMember]
        public IList<AssetItem> AssetList { get; set; }
    }
}
