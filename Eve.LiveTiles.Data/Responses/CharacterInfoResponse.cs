﻿using Eve.LiveTiles.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CharacterInfoResponse : Response
    {
        public CharacterInfoResponse()
        {

        }

        public CharacterInfoResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            CharacterID = GetValue<string>(doc.Element("eveapi").Element("result").Element("characterID"));
            CharacterName = GetValue<string>(doc.Element("eveapi").Element("result").Element("characterName"));
            CorporationName = GetValue<string>(doc.Element("eveapi").Element("result").Element("corporation"));
            CorporationID = GetValue<string>(doc.Element("eveapi").Element("result").Element("corporationID"));
            Race = GetValue<string>(doc.Element("eveapi").Element("result").Element("race"));
            BloodLine = GetValue<string>(doc.Element("eveapi").Element("result").Element("bloodline"));

            ShipName = GetValue<string>(doc.Element("eveapi").Element("result").Element("shipName"));
            ShipTypeID = GetValue<string>(doc.Element("eveapi").Element("result").Element("shipTypeID"));
            ShipTypeName = GetValue<string>(doc.Element("eveapi").Element("result").Element("shipTypeName"));

            AllianceID = GetValue<string>(doc.Element("eveapi").Element("result").Element("allianceID"));
            Alliance = GetValue<string>(doc.Element("eveapi").Element("result").Element("alliance"));
            LastKnownLocation = GetValue<string>(doc.Element("eveapi").Element("result").Element("lastKnownLocation"));

            SkillPoints = GetValue<int>(doc.Element("eveapi").Element("result").Element("skillPoints"));

            SecurityStatus = GetValue<double>(doc.Element("eveapi").Element("result").Element("securityStatus"));

            AccountBalance = GetValue<decimal>(doc.Element("eveapi").Element("result").Element("accountBalance"));

            DateTime dt = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("corporationDate"));
            DateTime convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            CorporationDate = convertedDate.ToLocalTime();

            dt = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("allianceDate"));
            convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            AllianceDate = convertedDate.ToLocalTime();

            EmploymentHistory = new List<Employment>();
            var rows = (from c in doc.Descendants("result").Elements("rowset")
                        where string.Equals(c.Attribute("name").Value, "employmentHistory")
                        select c);

            foreach (var parent in rows.Elements())
            {
                Employment em = new Employment
                {
                    CorporationID = GetValue<string>(parent.Attribute("corporationID")),
                    RecordID = GetValue<string>(parent.Attribute("recordID"))
                };

                dt = GetValue<DateTime>(parent.Attribute("startDate"));
                convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                em.StartDate = convertedDate.ToLocalTime();
                
                EmploymentHistory.Add(em);
            }
        }

        [DataMember]
        public string CharacterID { get; set; }

        [DataMember]
        public string CharacterName { get; set; }

        [DataMember]
        public string CorporationID { get; set; }

        [DataMember]
        public string CorporationName { get; set; }

        [DataMember]
        public string Race { get; set; }

        [DataMember]
        public string BloodLine { get; set; }

        [DataMember]
        public decimal AccountBalance { get; set; }

        [DataMember]
        public int SkillPoints { get; set; }

        [DataMember]
        public string ShipName { get; set; }

        [DataMember]
        public string ShipTypeID { get; set; }

        [DataMember]
        public string ShipTypeName { get; set; }

        [DataMember]
        public DateTime CorporationDate { get; set; }

        [DataMember]
        public string Alliance { get; set; }

        [DataMember]
        public string AllianceID { get; set; }

        [DataMember]
        public DateTime AllianceDate { get; set; }

        [DataMember]
        public string LastKnownLocation { get; set; }

        [DataMember]
        public double SecurityStatus { get; set; }

        [DataMember]
        public IList<Employment> EmploymentHistory { get; set; }

    }
}
