﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public enum OrderStateType
    {
        [EnumMember]
        Open,

        [EnumMember]
        Closed,

        [EnumMember]
        Expired,

        [EnumMember]
        Cancelled,

        [EnumMember]
        Pending,

        [EnumMember]
        CharacterDeleted
    }
}
