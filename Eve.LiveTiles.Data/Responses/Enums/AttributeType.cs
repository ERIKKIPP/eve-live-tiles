﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public enum AttributeType
    {
        [EnumMember]
        None,

        [EnumMember]
        Intelligence,

        [EnumMember]
        Memory,

        [EnumMember]
        Charisma,

        [EnumMember]
        Perception,

        [EnumMember]
        Willpower
    }
}
