﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CharacterSheetResponse : Response
    {
        public CharacterSheetResponse()
        {

        }

        public CharacterSheetResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            CharacterID = GetValue<string>(doc.Element("eveapi").Element("result").Element("characterID"));
            CharacterName = GetValue<string>(doc.Element("eveapi").Element("result").Element("name"));

            DateTime dt = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("DoB"));
            DateTime convertedDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            DateOfBirth = convertedDate.ToLocalTime();

            Race = GetValue<string>(doc.Element("eveapi").Element("result").Element("race"));
            BloodLine = GetValue<string>(doc.Element("eveapi").Element("result").Element("bloodLine"));
            Ancestry = GetValue<string>(doc.Element("eveapi").Element("result").Element("ancestry"));
            Gender = GetValue<string>(doc.Element("eveapi").Element("result").Element("gender"));
            CorporationName = GetValue<string>(doc.Element("eveapi").Element("result").Element("corporationName"));
            CorporationID = GetValue<string>(doc.Element("eveapi").Element("result").Element("corporationID"));
            AllianceName = GetValue<string>(doc.Element("eveapi").Element("result").Element("allianceName"));
            AllianceID = GetValue<string>(doc.Element("eveapi").Element("result").Element("allianceID"));
            CloneName = GetValue<string>(doc.Element("eveapi").Element("result").Element("cloneName"));

            CloneSkillPoints = GetValue<int>(doc.Element("eveapi").Element("result").Element("cloneSkillPoints"));
            Balance = GetValue<int>(doc.Element("eveapi").Element("result").Element("balance"));

            //<attributeEnhancers>
            AttributeEnhancers = new List<AttributeEnhancer>();
            var attenhancers = (from c in doc.Element("eveapi").Element("result").Element("attributeEnhancers").Elements()
                                select c);

            foreach (var parent in attenhancers)
            {
                AttributeEnhancer attrib = new AttributeEnhancer();

                switch (parent.Name.ToString())
                {
                    case "memoryBonus":
                        attrib.Enhancer = AttributeType.Memory;
                        break;
                    case "willpowerBonus":
                        attrib.Enhancer = AttributeType.Willpower;
                        break;
                    case "perceptionBonus":
                        attrib.Enhancer = AttributeType.Perception;
                        break;
                    case "intelligenceBonus":
                        attrib.Enhancer = AttributeType.Intelligence;
                        break;
                    case "charismaBonus":
                        attrib.Enhancer = AttributeType.Charisma;
                        break;
                    default:
                        break;
                }

                attrib.AugmentatorName = GetValue<string>(parent.Element("augmentatorName"));
                attrib.AugmentatorValue = GetValue<int>(parent.Element("augmentatorValue"));

                AttributeEnhancers.Add(attrib);
            }

            //<attributes>
            Attributes = new List<CharacterAttribute>();
            var atts = (from c in doc.Element("eveapi").Element("result").Element("attributes").Elements()
                        select c);

            foreach (var item in atts)
            {
                CharacterAttribute charatt = new CharacterAttribute
                  {
                      Attribute = (AttributeType)Enum.Parse(typeof(AttributeType), item.Name.ToString(), true)

                  };

                charatt.Value = GetValue<int>(item);

                Attributes.Add(charatt);
            }

            //<rowset name="skills" key="typeID" columns="typeID,skillpoints,level,published">
            Skills = new List<CharacterSkill>();
            var rows = (from c in doc.Descendants("result").Elements("rowset")
                        where string.Equals(c.Attribute("name").Value, "skills")
                        select c);

            foreach (var item in rows.Elements())
            {
                CharacterSkill skill = new CharacterSkill();

                skill.TypeID = GetValue<int>(item.Attribute("typeID"));
                skill.Skillpoints = GetValue<int>(item.Attribute("skillpoints"));
                skill.Level = GetValue<int>(item.Attribute("level"));

                int x = GetValue<int>(item.Attribute("published"));
                skill.Published = x == 1 ? true : false;

                Skills.Add(skill);
            }

            //    <rowset name="certificates" key="certificateID" columns="certificateID">
            Certificates = new List<Certificate>();
            var certs = (from c in doc.Descendants("result").Elements("rowset")
                         where string.Equals(c.Attribute("name").Value, "certificates")
                         select c);

            foreach (var item in certs.Elements())
            {
                Certificate cert = new Certificate();
                cert.CertificateID = GetValue<int>(item.Attribute("certificateID"));

                Certificates.Add(cert);
            }

            //<rowset name="corporationRoles" key="roleID" columns="roleID,roleName">
            CorporationRoles = new List<CorporationRole>();

            var roles = (from c in doc.Descendants("result").Elements("rowset")
                         where string.Equals(c.Attribute("name").Value, "corporationRoles") ||
                         string.Equals(c.Attribute("name").Value, "corporationRolesAtHQ") ||
                         string.Equals(c.Attribute("name").Value, "corporationRolesAtBase") ||
                         string.Equals(c.Attribute("name").Value, "corporationRolesAtOther")
                         select c);

            foreach (var item in roles.Elements())
            {
                CorporationRole role = new CorporationRole();

                role.RoleID = GetValue<int>(item.Attribute("roleID"));
                role.RoleName = GetValue<string>(item.Attribute("roleName"));

                CorporationRoles.Add(role);
            }

            // <rowset name="corporationTitles" key="titleID" columns="titleID,titleName">
            CorporationTitles = new List<CorporationTitle>();

            var titles = (from c in doc.Descendants("result").Elements("rowset")
                          where string.Equals(c.Attribute("name").Value, "corporationTitles")
                          select c);

            foreach (var item in titles.Elements())
            {
                CorporationTitle title = new CorporationTitle();

                title.TitleID = GetValue<int>(item.Attribute("titleID"));
                title.TitleName = GetValue<string>(item.Attribute("titleName"));

                CorporationTitles.Add(title);
            }
        }

        [DataMember]
        public string CharacterID { get; set; }

        [DataMember]
        public string CharacterName { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public string Race { get; set; }

        [DataMember]
        public string BloodLine { get; set; }

        [DataMember]
        public string Ancestry { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string CorporationID { get; set; }

        [DataMember]
        public string CorporationName { get; set; }

        [DataMember]
        public string AllianceName { get; set; }

        [DataMember]
        public string AllianceID { get; set; }

        [DataMember]
        public string CloneName { get; set; }

        [DataMember]
        public int CloneSkillPoints { get; set; }

        [DataMember]
        public decimal Balance { get; set; }

        [DataMember]
        public IList<AttributeEnhancer> AttributeEnhancers { get; set; }

        [DataMember]
        public IList<CharacterAttribute> Attributes { get; set; }

        [DataMember]
        public IList<CharacterSkill> Skills { get; set; }

        [DataMember]
        public IList<Certificate> Certificates { get; private set; }

        [DataMember]
        public IList<CorporationRole> CorporationRoles { get; private set; }

        [DataMember]
        public IList<CorporationTitle> CorporationTitles { get; private set; }
    }
}
