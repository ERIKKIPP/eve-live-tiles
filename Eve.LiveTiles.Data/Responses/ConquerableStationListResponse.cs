﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    public sealed class ConquerableStationListResponse : Response
    {
        public ConquerableStationListResponse()
        {

        }

        public ConquerableStationListResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            ConquerableStationList = new List<ConquerableStation>();
            var rows = (from c in doc.Descendants("result").Elements("rowset")
                        where string.Equals(c.Attribute("name").Value, "outposts")
                        select c);

            foreach (var parent in rows.Elements())
            {
                ConquerableStation station = new ConquerableStation
                {
                    CorporationName = GetValue<string>(parent.Attribute("corporationName")),
                    StationName = GetValue<string>(parent.Attribute("stationName"))
                };
                
                station.CorporationID = GetValue<int>(parent.Attribute("corporationID"));
                station.StationID = GetValue<long>(parent.Attribute("stationID"));
                station.StationTypeID = GetValue<int>(parent.Attribute("stationTypeID"));
                station.SolarSystemID = GetValue<int>(parent.Attribute("solarSystemID"));

                ConquerableStationList.Add(station);
            }
        }

        public IList<ConquerableStation> ConquerableStationList { get; private set; }
    }
}
