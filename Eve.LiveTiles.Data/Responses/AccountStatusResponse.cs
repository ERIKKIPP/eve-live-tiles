﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class AccountStatusResponse : Response
    {
        public AccountStatusResponse()
        {

        }

        public AccountStatusResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            PaidUntil = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("paidUntil"));
            CreateDate = GetValue<DateTime>(doc.Element("eveapi").Element("result").Element("createDate"));
            LogonCount = GetValue<long>(doc.Element("eveapi").Element("result").Element("logonCount"));
            LogonMinutes = GetValue<long>(doc.Element("eveapi").Element("result").Element("logonMinutes"));

        }

        [DataMember]
        public DateTime PaidUntil { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public long LogonCount { get; set; }

        [DataMember]
        public long LogonMinutes { get; set; }
    }
}
