﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    public sealed class ServerStatusResponse : Response
    {
        public ServerStatusResponse()
        {

        }

        public ServerStatusResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            XElement ele = doc.Element("eveapi").Element("result").Element("serverOpen");
            ServerOpen = GetValue<bool>(ele);

            ele = doc.Element("eveapi").Element("result").Element("onlinePlayers");
            OnlinePlayers = GetValue<int>(ele);
        }

        public bool ServerOpen { get; set; }

        public int OnlinePlayers { get; set; }
    }
}
