﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CharacterMarketOrdersResponse : Response
    {
        public CharacterMarketOrdersResponse()
        {

        }

        public CharacterMarketOrdersResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            MarketOrders = new List<MarketOrder>();
            var rows = from c in doc.Descendants("result").Elements("rowset")
                       select c;

            foreach (var parent in rows.Elements())
            {
                MarketOrder order = new MarketOrder
                {
                    AccountKey = GetValue<string>(parent.Attribute("accountKey")),
                    Bid = GetValue<bool>(parent.Attribute("bid")),
                    CharID = GetValue<string>(parent.Attribute("charID")),
                    Duration = GetValue<int>(parent.Attribute("duration")),
                    Escrow = GetValue<decimal>(parent.Attribute("escrow")),
                    Issued = GetValue<DateTime>(parent.Attribute("issued")),
                    MinVolume = GetValue<int>(parent.Attribute("minVolume")),
                    OrderID = GetValue<int>(parent.Attribute("orderID")),
                    OrderState = (OrderStateType)GetValue<int>(parent.Attribute("orderState")),
                    Price = GetValue<decimal>(parent.Attribute("price")),
                    Range = GetValue<int>(parent.Attribute("range")),
                    StationID = GetValue<int>(parent.Attribute("stationID")),
                    TypeID = GetValue<int>(parent.Attribute("typeID")),
                    VolEntered = GetValue<int>(parent.Attribute("volEntered")),
                    VolRemaining = GetValue<int>(parent.Attribute("volRemaining"))
                };

                MarketOrders.Add(order);
            }

        }

        [DataMember]
        public IList<MarketOrder> MarketOrders { get; set; }


    }
}
