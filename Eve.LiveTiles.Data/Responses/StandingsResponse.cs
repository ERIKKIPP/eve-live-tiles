﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class StandingsResponse : Response
    {
        public StandingsResponse()
        {

        }

        public StandingsResponse(XDocument doc)
            : base(doc)
        {
            if (!IsError)
            {
                LoadXml(doc);
            }
        }

        protected override void LoadXml(XDocument doc)
        {
            StandingsGroups = new List<StandingsGroup>();

            var rows = (from c in doc.Descendants("result").Elements("characterNPCStandings").Elements("rowset")
                        where string.Equals(c.Attribute("name").Value, "agents")
                        select c);

            StandingsGroup stnd = new StandingsGroup();
            stnd.Name = "Agents";
            StandingsGroups.Add(stnd);

            foreach (var parent in rows.Elements())
            {
                Standings standing = new Standings();

                standing.Standing = GetValue<decimal>(parent.Attribute("standing"));
                standing.FromName = GetValue<string>(parent.Attribute("fromName"));

                stnd.StandingsList.Add(standing);
            }

            rows = (from c in doc.Descendants("result").Elements("characterNPCStandings").Elements("rowset")
                    where string.Equals(c.Attribute("name").Value, "NPCCorporations")
                    select c);

            stnd = new StandingsGroup();
            stnd.Name = "NPC Corporations";
            StandingsGroups.Add(stnd);

            foreach (var parent in rows.Elements())
            {
                Standings standing = new Standings();

                standing.Standing = GetValue<decimal>(parent.Attribute("standing"));
                standing.FromName = GetValue<string>(parent.Attribute("fromName"));

                stnd.StandingsList.Add(standing);
            }

            rows = (from c in doc.Descendants("result").Elements("characterNPCStandings").Elements("rowset")
                    where string.Equals(c.Attribute("name").Value, "factions")
                    select c);

            stnd = new StandingsGroup();
            stnd.Name = "Factions";
            StandingsGroups.Add(stnd);

            foreach (var parent in rows.Elements())
            {
                Standings standing = new Standings();

                standing.Standing = GetValue<decimal>(parent.Attribute("standing"));
                standing.FromName = GetValue<string>(parent.Attribute("fromName"));

                stnd.StandingsList.Add(standing);
            }
        }

        [DataMember]
        public IList<StandingsGroup> StandingsGroups { get; set; }
    }
}
