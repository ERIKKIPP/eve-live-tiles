﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Eve.LiveTiles.Data
{
    public interface IEveDataService
    {
        Task<Response> GetKeyInfo(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetAccountStatus(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterAccountBalance(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterAssetList(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterCalendarEventAttendees(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterSheet(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterInfo(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterContactList(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterContactNotifications(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterFactionalWarfareStatics(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterIndustryJobs(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterKillLog(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterMailingLists(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterMailBodies(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterMailMessages(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterMarketOrders(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterMedals(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterNotificationTexts(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterNotifications(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterNPCStandings(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterResearch(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterSkillInTraining(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterSkillQueue(ApiKey accountInfo, string id, bool forceUpdate);

        Task<Response> GetCharacterUpcomingCalendarEvents(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterWalletJournal(ApiKey accountInfo, bool forceUpdate);

        Task<Response> GetCharacterWalletTransactions(ApiKey accountInfo, bool forceUpdate);
        
        Task<Response> GetSkillTree(bool forceUpdate);

        Uri GetPortrait(MiscMethodType imageType, string id);

        Task<Response> GetServerStatus(bool forceUpdate);

        Task<Response> GetConquerableStationList(bool forceUpdate);

    }
}
