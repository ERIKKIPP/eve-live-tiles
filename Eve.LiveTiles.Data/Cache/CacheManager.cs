﻿using Eve.LiveTiles.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Eve.LiveTiles.Data
{
    public static class CacheManager
    {
        #region Private Members
        private const string CACHE_FOLDER = "_cache";
        #endregion

        #region CTOR

        #endregion

        #region Methods
        /// <summary>
        ///  Retrieve cached item
        /// </summary>
        /// <param name="key"></param>
        /// <returns>cached item or null if item is expired</returns>
        public async static Task<string> Get(string key)
        {
            Storage<CacheObject<string>> storage = new Storage<CacheObject<string>>();

            //Get cache value
            var value = await storage.OpenAsync(key, StorageLocation.Local, CACHE_FOLDER).ConfigureAwait(false);

            if (value == null)
            {
                return null;
            }
            else if (value.IsValid)
            {
                return value.File;
            }
            else
            {
                //Delete old value
                await Clear(key).ConfigureAwait(false);

                return null;
            }
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        /// <param name="absoluteExpiration"></param>
        /// <returns></returns>
        public async static Task Add(string objectToCache, string key, DateTime absoluteExpiration)
        {
            Storage<CacheObject<string>> storage = new Storage<CacheObject<string>>();
            CacheObject<string> cacheFile = new CacheObject<string>() { File = objectToCache, ExpireDateTime = absoluteExpiration };

            await storage.SaveAsync(cacheFile, key, StorageLocation.Local, CACHE_FOLDER).ConfigureAwait(false);
        }

        /// <summary>
        /// Remove item from cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public async static Task Clear(string key)
        {
            Storage<object> storage = new Storage<object>();
            await storage.DeleteAsync(key, StorageLocation.Local, CACHE_FOLDER).ConfigureAwait(false);
        }


        #endregion


    }
}
