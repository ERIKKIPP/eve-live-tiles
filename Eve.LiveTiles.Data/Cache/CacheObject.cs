﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [DataContract]
    public sealed class CacheObject<T>
    {
        /// <summary>
        /// Expire date of cached file
        /// </summary>
        [DataMember]
        public DateTime? ExpireDateTime { get; set; }

        /// <summary>
        /// Actual file being stored
        /// </summary>
        [DataMember]
        public T File { get; set; }

        /// <summary>
        /// Is the cache file valid?
        /// </summary>
        [IgnoreDataMember]
        public bool IsValid
        {
            get
            {
                return (ExpireDateTime == null || ExpireDateTime.Value > DateTime.Now);
            }
        }
    }
}
