﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.LiveTiles.Data
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public sealed class APIAttribute : Attribute
    {
        public APIAttribute()
        {

        }
        
        public string BaseUrl { get; set; }

        public string PostData { get; set; }

        public CallGroupType CallGroupId { get; set; }
    }
}
